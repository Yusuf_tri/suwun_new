package id.co.popay.suwunapps.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInfo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RedeemPulsaActivity extends SessionManager {

    @BindView(R.id.edtNomerHP)
    EditText edtNomerHP;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.ivGambarRedeem)
    ImageView ivGambarRedeem;
    @BindView(R.id.tvJudulRedeem)
    TextView tvJudulRedeem;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;

    private String mMSISDN;
    private String mItemGambar, mItemId, mItemTipe, mItemJudul, mItemDeskripsi, mItemPoin, mNominal;
    private TextWatcher txtWatcherMSISDN = null;
    private String mTimestamp, mSignature;
    private final int REQUEST_CODE=99;

    private String strTelcoSelected;

    TextView txtSaldo;
    TextView txtInfo, txtStatus, txtHeaderStatus;
    ImageView imgStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_pulsa);
        ButterKnife.bind(this);

    /*
        intent.putExtra("itemId", itemId);
        intent.putExtra("itemTipe", itemTipe);
        intent.putExtra("itemJudul", itemJudul);
        intent.putExtra("itemDeskripsi", itemDeskripsi);
        intent.putExtra("itemPoin",itemPoin);
        intent.putExtra("itemGambar", itemGambar);

    * */
        Intent intent = getIntent();
        mItemGambar = intent.getStringExtra("itemGambar");
        mItemId = intent.getStringExtra("itemId");
        mItemTipe = intent.getStringExtra("itemTipe");
        mItemJudul = intent.getStringExtra("itemJudul");
        mItemDeskripsi = intent.getStringExtra("itemDeskripsi");
        mItemPoin = intent.getStringExtra("itemPoin");
        mNominal = intent.getStringExtra("itemNominal");
        Picasso.with(this).load(mItemGambar).error(R.drawable.noimage).into(ivGambarRedeem);
        tvJudulRedeem.setText(mItemJudul + " " + "(" + mItemPoin + " poin)");

        txtWatcherMSISDN = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 10) {
                    btnBeli.setEnabled(false);
                    btnBeli.setBackgroundResource(R.color.colorGrid);
                } else {
                    if (getTelco(s.toString()) != -1) {
                        btnBeli.setEnabled(true);
                        btnBeli.setBackgroundResource(R.drawable.button_bg);
                    } else {
                        btnBeli.setEnabled(false);
                        btnBeli.setBackgroundResource(R.color.colorGrid);
                    }

                }

            }
        };
        edtNomerHP.addTextChangedListener(txtWatcherMSISDN);
    }

    @OnClick(R.id.btn_beli)
    public void onViewClicked() {
        // hash('sha1',  $arrHeader['timestamp']. $arrHeader['appkey']. $merchant->secret_key . $uid.$tipe.$poin.$amount.$dest) )
        mMSISDN = edtNomerHP.getText().toString();
        String data = mItemTipe + "-" + mItemPoin + "-" + mItemId + "-" + mMSISDN + "-" + mNominal + "-" + mItemJudul;

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getUID() +
                mItemTipe + mItemPoin + mNominal + mMSISDN);
        Log.d("RedeemPulsa", data);
        doRedeemPulsa();
        //Toast.makeText(this, data, Toast.LENGTH_LONG).show();
    }

    private void doRedeemPulsa() {
        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstances();
        Call<ResponseInfo> redeemPulsa = myAPI.redeemPulsa(mTimestamp, mItemTipe, mItemPoin, mItemId, mItemJudul, mNominal, mMSISDN, sessionManager.getUID(), mSignature);
        redeemPulsa.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                AlertDialog.Builder alert = new AlertDialog.Builder(RedeemPulsaActivity.this).setCancelable(false);
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_transaksi_pulsa, null);

                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);

                txtHeaderStatus.setText("Status redeem");
                Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                btnOk.setVisibility(View.VISIBLE);

                if (result_code.equals("0000")) {
                    txtStatus.setText(result_desc);
                    txtInfo.setText(response.body().getInfo());
                    //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                    imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                    //update saldo di sessionManager
                    sessionManager.setPoin(response.body().getBalance());

                } else if (result_code.equals("0168") || result_code.equals("2068")) {
                    if (result_desc.equals("PENDING")) {
                        txtStatus.setText("SEDANG DIPROSES");
                    } else {
                        txtStatus.setText(result_desc);
                    }
                    imgStatus.setImageResource(R.mipmap.ic_status_pending);
                    sessionManager.setPoin(response.body().getBalance());

                    if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                        //txtInfo.setText(result_desc);
                        if (result_desc.equals("PENDING")) {
                            txtStatus.setText("SEDANG DIPROSES");
                        } else {
                            txtStatus.setText(result_desc);
                        }

                    } else {
                        txtInfo.setText(response.body().getInfo());
                    }

                } else {
                    imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                    if (result_code.equals("1003")) {
                        //pin salah.
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc);
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        });
                    } else {
                        txtStatus.setText("GAGAL");
                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            txtInfo.setText(result_desc);
                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }
                    }
                    //txtInfo.setBackgroundResource(R.drawable.button_gagal);
                }
                alert.setView(v);
                alert.show();
            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
                //String pesan = t.getMessage();
                //Toast.makeText(RedeemPulsaActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                if (t.getMessage() != null) {
                    Toast.makeText(getApplicationContext(), "API redeemP Failed, " + t.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API redeemP Failed, TIMEOUT. Cek daftar riwayat redeem.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (REQUEST_CODE) :
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c= getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        if (Integer.valueOf(hasNumber)==1) {
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                num = formatPhoneNumber(num);
                                //Toast.makeText(PulsaRegulerActivity.this, "Number="+num, Toast.LENGTH_LONG).show();

                            }
                            edtNomerHP.setText(num);
                            if (getTelco(num) == -1) {
                                btnBeli.setEnabled(false);
                                btnBeli.setBackgroundResource(R.color.colorGrid);
                            } else {
                                btnBeli.setEnabled(true);
                                btnBeli.setBackgroundResource(R.drawable.button_bg);
                            }
                        }
                    }
                }
                break;
        }
    }

    public void displayHome(View view) {
        startActivity(new Intent(RedeemPulsaActivity.this, SuwunPoinActivity.class));
    }

    private int getTelco(String s) {
        int retValue = 0;

        switch (s.substring(0, 4)) {

            case "0811": //simpati
            case "0812": //simpati
            case "0813": //simpati
            case "0821": //simpati
            case "0822": //simpati
            case "0823": //as
            case "0851": //as
            case "0852": //as
            case "0853": //as
                retValue = 1;
                strTelcoSelected = "TELKOMSEL";
                break;

            case "0814": //mentari
            case "0815": //mentari
            case "0816": //mentari
            case "0855": //mentari
            case "0856": //im3
            case "0857": //im3
            case "0858": //mentari
                retValue = 2;
                strTelcoSelected = "INDOSAT";
                break;

            case "0817":
            case "0818":
            case "0819":
            case "0859":
            case "0877":
            case "0878":
            case "0879":
                retValue = 3;
                strTelcoSelected = "XL";
                break;

            case "0881":
            case "0882":
            case "0883":
            case "0884":
            case "0885":
            case "0886":
            case "0887":
            case "0888":
            case "0889":
                retValue = 4;
                strTelcoSelected = "SMARTFREN";
                break;

            case "0894":
            case "0895":
            case "0896":
            case "0897":
            case "0898":
            case "0899":
                retValue = 5;
                strTelcoSelected = "TRI";
                break;

            case "0831":
            case "0832":
            case "0833":
            case "0838":
                retValue = 6;
                strTelcoSelected = "AXIS";
                break;

            default:
                retValue = -1;
                strTelcoSelected = "";
                Toast.makeText(this, "Operator tidak dikenal (cek nomor)", Toast.LENGTH_SHORT).show();

        }
        return retValue;
    }


    public void displayPhoneBook(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE);
    }

    private String formatPhoneNumber(String num) {
        String temp;
        String pattern1 = "[-*#]|[a-zA-Z]|\\s";
        String pattern2 = "^62|^\\+62";

        temp = num.replaceAll(pattern1,"");
        temp = temp.replaceAll(pattern2,"0");
        return temp;
    }
}
