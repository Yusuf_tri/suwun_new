package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.ResultRedeemtrxItem;

/**
 * Created by ara on 02/04/18.
 */

public class RedeemHistoryAdapter extends RecyclerView.Adapter<RedeemHistoryAdapter.MyViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<ResultRedeemtrxItem> mTransaksiList;

    public RedeemHistoryAdapter(Context context, List<ResultRedeemtrxItem> transaksiList) {
        mContext = context;
        mTransaksiList = transaksiList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RedeemHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = mLayoutInflater.inflate(R.layout.item_daftar_redeem, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RedeemHistoryAdapter.MyViewHolder holder, int position) {
        ResultRedeemtrxItem redeemItem = mTransaksiList.get(position);
        String kodeproduk = redeemItem.getKodeproduk();
        if (! redeemItem.getTargetNumber().isEmpty()) {
            holder.txtItem.setText(redeemItem.getJudulItem() + ", ke " + redeemItem.getTargetNumber());
        } else {
            holder.txtItem.setText(redeemItem.getJudulItem());
        }
        holder.txtHarga.setText(String.format("%,d poin", Integer.parseInt(redeemItem.getPoinRedeem().replace(',','.'))));
        if (redeemItem.getStatusRedeem().equals("SUKSES")) {
            holder.txtStatus.setText(redeemItem.getStatusRedeem() +", " + redeemItem.getInfoRedeem());
        } else {
            if (redeemItem.getStatusRedeem().equals("PENDING")) {
                holder.txtStatus.setText("SEDANG DIPROSES");
            } else {
                holder.txtStatus.setText(redeemItem.getStatusRedeem() );
            }
        }
        holder.txtTanggal.setText(redeemItem.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return mTransaksiList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtItem, txtHarga, txtStatus, txtTanggal;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtItem = (TextView) itemView.findViewById(R.id.txtItem);
            txtHarga= (TextView) itemView.findViewById(R.id.txtHarga);
            txtStatus= (TextView) itemView.findViewById(R.id.txtStatus);
            txtTanggal = (TextView) itemView.findViewById(R.id.txtTanggal);
            cardView = (CardView) itemView.findViewById(R.id.container);
        }
    }


}
