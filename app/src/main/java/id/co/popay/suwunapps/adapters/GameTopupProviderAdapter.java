package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.GameProviderItem;

/**
 * Created by ara on 13/03/19.
 */

public class GameTopupProviderAdapter extends RecyclerView.Adapter<GameTopupProviderAdapter.MyViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<GameProviderItem> mProviderList;

    private OnItemClickListener mListener;

    public void setListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public GameTopupProviderAdapter(Context context, List<GameProviderItem> providerList) {
        mContext = context;
        mProviderList = providerList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_gametopup, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final GameProviderItem itemProvider = mProviderList.get(position);
        holder.txtGameProvider.setText(itemProvider.getProvider());
        Picasso.with(mContext)
                .load(itemProvider.getGambar().toString())
                .placeholder(R.mipmap.noimage1)
                .into(holder.imgGameProvider);

        if (mListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.OnItemClickListener(itemProvider.getProvider());
                }
            });

            holder.btnTopup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.OnItemClickListener(itemProvider.getProvider());
                }
            });

            holder.btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(mContext, itemProvder.getPlaystoreLink().toString(), Toast.LENGTH_SHORT).show();
                    String market_uri = itemProvider.getPlaystoreLink().toString();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(market_uri));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                            .FLAG_ACTIVITY_NO_ANIMATION);
                    mContext.startActivity(intent);

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return mProviderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtGameProvider;
        TextView txtGameKeterangan;
        ImageView imgGameProvider;
        CardView cardView;
        Button btnDownload, btnTopup;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtGameProvider = (TextView) itemView.findViewById(R.id.txtGameProvider);
            txtGameKeterangan = (TextView) itemView.findViewById(R.id.txtKeterangan);
            imgGameProvider = (ImageView) itemView.findViewById(R.id.imgGameProvider);
            cardView = (CardView) itemView.findViewById(R.id.container);
            btnDownload = (Button) itemView.findViewById(R.id.btnDownload);
            btnTopup = (Button) itemView.findViewById(R.id.btnTopup);
        }
    }

    public interface OnItemClickListener {
        void OnItemClickListener (String itemId);
    }
}
