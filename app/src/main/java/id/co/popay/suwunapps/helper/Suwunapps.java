package id.co.popay.suwunapps.helper;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.APP_KEY;

/**
 * Created by ara on 10/02/18.
 */

public class Suwunapps {

    public static String getSHA256(String s) {
        String sres = "";
        try {
            //MessageDigest md = MessageDigest.getInstance("MD5");
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(s.getBytes("UTF-8"));
            byte[] digest = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < digest.length; i++)
                hexString.append(String.format("%02x", digest[i]));
            sres = hexString.toString();
        } catch (Exception e) {
            Log.d("Suwunapps", "Exception getSHA256. Reason: " + e.toString());
        }
        return sres;
    }

    public static String getUnixTimeStamp() {
        int tm = (int) (System.currentTimeMillis() % Integer.MAX_VALUE);
        return Integer.toString(tm);
    }


    public static String getMd5(String s) {
        String sres = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(s.getBytes("UTF-8"));
            byte[] digest = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < digest.length; i++)
                hexString.append(String.format("%02x", digest[i]));
            sres = hexString.toString();
        } catch (Exception e) {
            Log.d("Suwunapps","Exception getMD5. Reason: " + e.toString());
        }
        return sres;
    }


    public static String getSHA1(String s) {
        String strResult="";
        try {
            MessageDigest mdSHA1 = MessageDigest.getInstance("SHA-1");
            mdSHA1 .reset();
            mdSHA1 .update(s.getBytes());
            byte[] output = mdSHA1.digest();
            StringBuffer hexString1 = new StringBuffer();
            for (int i=0; i<output.length; i++) {
                //hexString1.append(Integer.toHexString(0xFF & output[i]));
                hexString1.append(String.format("%02x",output[i]));
            }
            strResult = hexString1.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return strResult;
    }

    public static String getEncPin(String strPin, String ts) {
        String strResult = "";
        String a = MyConstants.APP_KEY;
        String b = MyConstants.APP_ID;
        String c = MyConstants.APP_SALT;

        strResult = getMd5(MyConstants.APP_SECRET + strPin + ts);
        return strResult ;
    }

    public static String[] getSaldoFromServer(String strUID) {
        String mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        String mSignature = Suwunapps.getSHA1(mTimestamp + APP_KEY + MyConstants.APP_SECRET + strUID);
        final String[] saldo = {""};

        APIServices myAPI = InitNetLibrary.getInstances();
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, strUID, mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    //Toast.makeText(MainActivity.this, "berhasil cek saldo: " + result_desc, Toast.LENGTH_SHORT).show();
                    //mtxtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    saldo[0] = result_desc;
                    saldo[1] = result_poin;
                } else {
                    saldo[0] = result_desc;
                    saldo[1] = "0";
                }
                //Log.d("darigetSaldo", saldo[0] );
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                saldo[0] = "-";
            }
        });
        Log.d("darigetSaldonya", saldo[0]);
        return saldo;
    }

    public static String formatPhoneNumber(String num) {
        String temp;
        String pattern1 = "[-*#]|[a-zA-Z]|\\s";
        String pattern2 = "^62|^\\+62";

        temp = num.replaceAll(pattern1,"");
        temp = temp.replaceAll(pattern2,"0");

        return temp;
    }

    public static int getTelco(String s) {
        int retValue = 0;

        switch (s.substring(0, 4)) {

            case "0811": //simpati
            case "0812": //simpati
            case "0813": //simpati
            case "0821": //simpati
            case "0822": //simpati
            case "0823": //as
            case "0851": //as
            case "0852": //as
            case "0853": //as
                retValue = 1;
                //strTelcoSelected = "TELKOMSEL";
                break;

            case "0814": //mentari
            case "0815": //mentari
            case "0816": //mentari
            case "0855": //mentari
            case "0856": //im3
            case "0857": //im3
            case "0858": //mentari
                retValue = 2;
                //strTelcoSelected = "INDOSAT";
                break;

            case "0817":
            case "0818":
            case "0819":
            case "0859":
            case "0877":
            case "0878":
            case "0879":
                retValue = 3;
                //strTelcoSelected = "XL";
                break;

            case "0881":
            case "0882":
            case "0883":
            case "0884":
            case "0885":
            case "0886":
            case "0887":
            case "0888":
            case "0889":
                retValue = 4;
                //strTelcoSelected = "SMARTFREN";
                break;

            case "0894":
            case "0895":
            case "0896":
            case "0897":
            case "0898":
            case "0899":
                retValue = 5;
                //strTelcoSelected = "TRI";
                break;

            case "0831":
            case "0832":
            case "0833":
            case "0838":
                retValue = 6;
                //strTelcoSelected = "AXIS";
                break;

            default:
                retValue = -1;
                //Toast.makeText(this, "Operator tidak dikenal (cek nomor)", Toast.LENGTH_SHORT).show();
        }
        return retValue;

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ENCRYPTION & DECRYPTION

    private IvParameterSpec generateIv() {
        SecureRandom random = new SecureRandom();
        byte[] biv = new byte[16];
        random.nextBytes(biv);
        IvParameterSpec iv = new IvParameterSpec(biv);
        return iv;
        // to get byte[] => iv.getIV()
    }

    private SecretKey getKey(byte[] b) throws Exception {
        SecretKey key = new SecretKeySpec(b, "AES");
        return key;
    }

    private String cbcEncrypt(String skeyval, String sdata, IvParameterSpec ivps) throws Exception {
        SecretKey key = getKey(skeyval.getBytes("UTF-8"));
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, key, ivps);
        byte[] bencval = c.doFinal(sdata.getBytes("UTF-8"));
        String sencval = new String(Base64Coder.encode(bencval));
        return sencval;
    }

    private String cbcDecrypt(String skeyval, String sencdata, IvParameterSpec ivps) throws Exception {
        SecretKey key = getKey(skeyval.getBytes("UTF-8"));
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.DECRYPT_MODE, key, ivps);
        byte[] bdecval = c.doFinal(Base64Coder.decode(sencdata));
        //String sdecval = new String(Base64Coder.encode(bdecval));
        String sdecval = new String(bdecval);
        return sdecval;
    }
}
