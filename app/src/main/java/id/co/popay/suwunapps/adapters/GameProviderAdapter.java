package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;

/**
 * Created by ara on 10/02/18.
 */

public class GameProviderAdapter extends RecyclerView.Adapter<GameProviderAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<String> mProviderList;

    private OnItemClickListener mListener;

    public void setListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public GameProviderAdapter(Context context, List<String> providerList) {
        mContext = context;
        mProviderList = providerList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_game_provider, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GameProviderAdapter.MyViewHolder holder, final int position) {
        holder.txtGameProvider.setText(mProviderList.get(position));
        holder.imgGameProvider.setImageResource(R.drawable.ic_nogamefilled);
        if (mListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.OnItemClickListener(mProviderList.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mProviderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtGameProvider;
        ImageView imgGameProvider;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtGameProvider = (TextView) itemView.findViewById(R.id.txtGameProvider);
            imgGameProvider = (ImageView) itemView.findViewById(R.id.imgGameProvider);
            cardView = (CardView) itemView.findViewById(R.id.container);
        }
    }

    public interface OnItemClickListener {
        void OnItemClickListener (String itemId);
    }
}
