package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class CitiesItem{

	@SerializedName("kota")
	private String kota;

	@SerializedName("_id")
	private String id;

	@SerializedName("product_code")
	private String productCode;

	public void setKota(String kota){
		this.kota = kota;
	}

	public String getKota(){
		return kota;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setProductCode(String productCode){
		this.productCode = productCode;
	}

	public String getProductCode(){
		return productCode;
	}

	@Override
 	public String toString(){
		return 
			"CitiesItem{" + 
			"kota = '" + kota + '\'' + 
			",_id = '" + id + '\'' + 
			",product_code = '" + productCode + '\'' + 
			"}";
		}
}