package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;


public class ResponseInfo {

	@SerializedName("balance")
	private String balance;

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	@SerializedName("info")
	private String info;

	public void setBalance(String balance){
		this.balance = balance;
	}

	public String getBalance(){
		return balance;
	}

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	public void setInfo(String info){
		this.info = info;
	}

	public String getInfo(){
		return info;
	}

	@Override
 	public String toString(){
		return 
			"ResponseInfo{" + 
			"balance = '" + balance + '\'' + 
			",result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			",info = '" + info + '\'' + 
			"}";
		}
}