package id.co.popay.suwunapps.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInfo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class RedeemBarangActivity extends SessionManager {

    @BindView(R.id.ivGambarRedeem)
    ImageView ivGambarRedeem;
    @BindView(R.id.tvJudulRedeem)
    TextView tvJudulRedeem;
    @BindView(R.id.edtNama)
    EditText edtNama;
    @BindView(R.id.edtAlamat)
    EditText edtAlamat;
    @BindView(R.id.edtKota)
    EditText edtKota;
    @BindView(R.id.edtPropinsi)
    EditText edtPropinsi;
    @BindView(R.id.edtKodepos)
    EditText edtKodepos;
    @BindView(R.id.edtCatatan)
    EditText edtCatatan;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.imageCardView)
    CardView imageCardView;
    @BindView(R.id.edtNohp)
    EditText edtNohp;


    private String mItemGambar;
    private String mItemId, mItemTipe, mItemJudul, mItemDeskripsi, mItemPoin;
    private String mTimestamp, mSignature;
    TextView txtSaldo;
    TextView txtInfo, txtStatus, txtHeaderStatus;
    ImageView imgStatus;
    String strNama ;
    String strMsisdn;
    String strAlamat;
    String strKota;
    String strPropinsi;
    String strKodepos;
    String strCatatan;
    String alamatLengkap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_barang);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mItemGambar = intent.getStringExtra("itemGambar");
        mItemId = intent.getStringExtra("itemId");
        mItemTipe = intent.getStringExtra("itemTipe");
        mItemJudul = intent.getStringExtra("itemJudul");
        mItemDeskripsi = intent.getStringExtra("itemDeskripsi");
        mItemPoin = intent.getStringExtra("itemPoin");

        Picasso.with(this).load(mItemGambar).error(R.drawable.noimage).into(ivGambarRedeem);
        tvJudulRedeem.setText(mItemJudul + " " + "(" + mItemPoin + " poin)");

    }

    @OnClick(R.id.btn_beli)
    public void onViewClicked() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());

        strNama = edtNama.getText().toString();
        strMsisdn = edtNohp.getText().toString();
        strAlamat = edtAlamat.getText().toString();
        strKota = edtKota.getText().toString();
        strPropinsi = edtPropinsi.getText().toString();
        strKodepos = edtKodepos.getText().toString();
        strCatatan = edtCatatan.getText().toString();

        if (TextUtils.isEmpty(strNama)) {
            edtNama.setError("Nama penerima masih kosong");
        } else if (TextUtils.isEmpty(strMsisdn)) {
            edtNohp.setError("Nomer HP masih kosong");
        } else if (TextUtils.isEmpty(strAlamat)) {
            edtAlamat.setError("Alama harus diisi");
        } else if (TextUtils.isEmpty(strKota)) {
            edtKota.setError("Kota harus diisi");
        } else if (TextUtils.isEmpty(strPropinsi)) {
            edtPropinsi.setError("Propinsi harus diisi");
        } else if (TextUtils.isEmpty(strKodepos)) {
            edtKodepos.setError("Kode pos masih kosong");
        } else {
            //call API simpan data barang reedem
            alamatLengkap = strNama + "\n" + strAlamat + "\n" + strKota + "\n" + strPropinsi + "\n" + strKodepos + "\nNomer HP: " + strMsisdn;
            if (!TextUtils.isEmpty(strCatatan)) {
                alamatLengkap = alamatLengkap + "\r\n" + strCatatan;
            }
            String data = mItemTipe + "-" + mItemPoin + "-" + mItemId + "-" +  mItemJudul + "-" + alamatLengkap;
            Log.d("RedeemBarang", data);

            //hash('sha1',  $arrHeader['timestamp']. $arrHeader['appkey']. $merchant->secret_key . $uid.$tipe.$poin.$dest
            mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET +
                    sessionManager.getUID() + mItemTipe + mItemPoin + strMsisdn);
            doRedeemBarang();
        }
    }

    private void doRedeemBarang() {
        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstances();
        Call<ResponseInfo> redeemBarang = myAPI.redeemBarang(mTimestamp, mItemTipe, mItemPoin, mItemId, mItemJudul, strMsisdn, alamatLengkap,
                strCatatan, sessionManager.getUID(), mSignature );
        redeemBarang.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_desc= response.body().getResultDesc();
                String result_code= response.body().getResultCode();
                AlertDialog.Builder alert = new AlertDialog.Builder(RedeemBarangActivity.this).setCancelable(false);

                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_transaksi_pulsa, null);

                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);

                txtHeaderStatus.setText("Status redeem");
                Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                btnOk.setVisibility(View.VISIBLE);

                if (result_code.equals("0000")) {
                    txtStatus.setText(result_desc);
                    txtInfo.setText(response.body().getInfo() );
                    //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                    imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                    //update saldo di sessionManager
                    sessionManager.setPoin(response.body().getBalance());

                } else if (result_code.equals("0168") || result_code.equals("2068")) {
                    if (result_desc.equals("PENDING")) {
                        txtStatus.setText("SEDANG DIPROSES");
                    } else {
                        txtStatus.setText(result_desc);
                    }
                    imgStatus.setImageResource(R.mipmap.ic_status_pending);
                    sessionManager.setPoin(response.body().getBalance());

                    if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                        //txtInfo.setText(result_desc);
                        if (result_desc.equals("PENDING")) {
                            txtStatus.setText("SEDANG DIPROSES");
                        } else {
                            txtStatus.setText(result_desc);
                        }

                    } else {
                        txtInfo.setText(response.body().getInfo() );
                    }

                } else {
                    imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                    if (result_code.equals("1003")) {
                        //pin salah.
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        });
                    } else {
                        txtStatus.setText("GAGAL");
                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            txtInfo.setText(result_desc );
                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }
                    }
                    //txtInfo.setBackgroundResource(R.drawable.button_gagal);
                }
                alert.setView(v);
                alert.show();
            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
//                String pesan = t.getMessage();
//                Toast.makeText(RedeemBarangActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                //Log.d("RedeemBarang", pesan, t);
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API redeemB Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API redeemB Failed, TIMEOUT. Cek daftar riwayat redeem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void displayHome(View view) {
        startActivity(new Intent(RedeemBarangActivity.this, SuwunPoinActivity.class));
    }

}
