package id.co.popay.suwunapps.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.adapters.TransaksiAdapter;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.model.ResponseTransaksi;
import id.co.popay.suwunapps.model.ResultTrxItem;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import id.co.popay.suwunapps.utils.ConnectivityUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrxHistoryFragment extends Fragment {

    RecyclerView recyclerView;
    List<ResultTrxItem> resultTrx;
    TransaksiAdapter mAdapter;
    TextView txtJudulDeposit;

    String mUserId, mId;
    ProgressDialog mProgressDialog;
    private String mTimestamp, mSignature;
    private String mSaldo, mPoin;
    private ImageView btn_refresh;

    public TrxHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trx_history, container, false);
        txtJudulDeposit = (TextView) view.findViewById(R.id.txtJudulDeposit);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        resultTrx = new ArrayList<>();
        mAdapter = new TransaksiAdapter(getActivity(), resultTrx);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
        //mAdapter.setListener();

        if (ConnectivityUtil.isConnected(getContext())) {
            loadListTrx();
        } else {
            Toast.makeText(getContext(), "Tidak ada koneksi internet", Toast.LENGTH_SHORT).show();

        }


        btn_refresh = (ImageView) view.findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityUtil.isConnected(getContext())) {
                    getSaldoFromServer();
                    loadListTrx();
                } else {
                    Toast.makeText(getContext(), "Tidak ada koneksi internet", Toast.LENGTH_SHORT).show();

                }
            }
        });


        return view;
    }

//    @UiThread
//    protected void dataSetChanged() {
//        mAdapter.notifyDataSetChanged();
//    }

    private void loadListTrx() {
        //showProgressDialog("Loading data.... ");
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            //mProgressDialog.setTitle("");
            mProgressDialog.setMessage("Loading transaction data..");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();

        APIServices myAPI = InitNetLibrary.getInstanceWc(getContext());

        mUserId = getArguments().getString("userId");
        mId = getArguments().getString("id");
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mId);

        final Call<ResponseTransaksi> dataTransaksi = myAPI.getTrxList(mTimestamp, mId, mSignature);

        dataTransaksi.enqueue(new Callback<ResponseTransaksi>() {
            @Override
            public void onResponse(Call<ResponseTransaksi> call, Response<ResponseTransaksi> response) {
                //hideProgressDialog();
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    List<ResultTrxItem> itemTrx = response.body().getResultTrx();
                    if (resultTrx.size() != 0) {
                        resultTrx.clear();
                    }
                    resultTrx.addAll(itemTrx);
                    mAdapter.notifyDataSetChanged();

//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mAdapter.notifyDataSetChanged();
//                        }
//                    });

                } else {
                    //txtJudulDeposit.setText(result_desc);
                }
            }

            @Override
            public void onFailure(Call<ResponseTransaksi> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSaldo = getArguments().getString("saldo");
        mPoin  = getArguments().getString("poin");
//        btn_refresh = (ImageView) getView().findViewById(R.id.btn_refresh);
//        btn_refresh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getSaldoFromServer();
//                loadListTrx();
//            }
//        });

    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mId);

        APIServices myAPI = InitNetLibrary.getInstanceWc(getContext());
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, mId, mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                TextView tvSaldo = getActivity().findViewById(R.id.tvSaldo);
                TextView tvPoin = getActivity().findViewById(R.id.tvPoin);
                if (result_code.equals("0000")) {
                    //sessionManager.setSaldo(result_desc);
                    //txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    mSaldo = result_desc;
                    mPoin = result_poin;
                    tvSaldo.setText(String.format("Rp. %,d", Integer.parseInt(mSaldo)).replace(',', '.'));
                    tvPoin.setText(String.format("%,d", Integer.parseInt(mPoin)).replace(',', '.'));
                } else if (result_code.equals("0103")){
                    Toast.makeText(getContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    Log.d("MainTab", result_desc + " " + result_poin);
                    tvSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    tvPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                } else {
                    Toast.makeText(getContext(),"Gagal ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                Toast.makeText(getActivity(), "API cs Fail, " + t.getMessage().replace(BASE_IP,"API_HOST") , Toast.LENGTH_SHORT).show();
            }
        });

    }

}
