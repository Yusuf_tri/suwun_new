package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResultInfoPln{

	@SerializedName("provider")
	private String provider;

	@SerializedName("kodeproduk")
	private String kodeproduk;

	@SerializedName("adminsuwun")
	private String adminsuwun;

	@SerializedName("tagihan")
	private String tagihan;

	@SerializedName("nama")
	private String nama;

	@SerializedName("biaya")
	private String biaya;

	@SerializedName("inquiryid")
	private String inquiryid;

	@SerializedName("systrace")
	private String systrace;

	@SerializedName("billid")
	private String billid;

	@SerializedName("idpel")
	private String idpel;

	@SerializedName("periode")
	private String periode;

	public void setProvider(String provider){
		this.provider= provider;
	}

	public String getProvider(){
		return provider;
	}

	public void setKodeproduk(String kodeproduk){ this.kodeproduk= kodeproduk; }

	public String getKodeproduk(){
		return kodeproduk;
	}

	public void setAdminsuwun(String adminsuwun){
		this.adminsuwun = adminsuwun;
	}

	public String getAdminsuwun(){
		return adminsuwun;
	}

	public void setTagihan(String tagihan){
		this.tagihan = tagihan;
	}

	public String getTagihan(){
		return tagihan;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setBiaya(String biaya){
		this.biaya = biaya;
	}

	public String getBiaya(){
		return biaya;
	}

	public void setInquiryid(String inquiryid){
		this.inquiryid = inquiryid;
	}

	public String getInquiryid(){
		return inquiryid;
	}

	public void setSystrace(String systrace){
		this.systrace = systrace;
	}

	public String getSystrace(){
		return systrace;
	}

	public void setBillid(String billid){
		this.billid = billid;
	}

	public String getBillid(){
		return billid;
	}

	public void setIdpel(String idpel){
		this.idpel = idpel;
	}

	public String getIdpel(){
		return idpel;
	}

	public void setPeriode(String periode){
		this.periode = periode;
	}

	public String getPeriode(){
		return periode;
	}

	@Override
 	public String toString(){
		return 
			"ResultInfoPln{" + 
			"adminsuwun = '" + adminsuwun + '\'' + 
			",tagihan = '" + tagihan + '\'' + 
			",nama = '" + nama + '\'' + 
			",biaya = '" + biaya + '\'' + 
			",inquiryid = '" + inquiryid + '\'' + 
			",systrace = '" + systrace + '\'' + 
			",billid = '" + billid + '\'' + 
			",idpel = '" + idpel + '\'' + 
			",periode = '" + periode + '\'' + 
			"}";
		}
}