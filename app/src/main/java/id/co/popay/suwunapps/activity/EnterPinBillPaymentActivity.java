package id.co.popay.suwunapps.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInfo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class EnterPinBillPaymentActivity extends SessionManager {

    @BindView(R.id.edt_pin)
    EditText edtPin;
    @BindView(R.id.mainCard)
    CardView mainCard;
    @BindView(R.id.btn_bayartagihan)
    Button btnBayartagihan;
    @BindView(R.id.btn_batalbayartagihan)
    Button btnBatalbayartagihan;

    private String mTimestamp;
    private String mSignature;

    private String mIdpel, mNamaPelanggan, mPeriode;
    private String mKodeproduk;
    private String mUid;
    private String mTipe;
    private String mProvider;
    private String mAmount;
    private int  mAdminCA;
    private String mInquiryid, mSystrace, mBillid;
    private int mDebitSaldo;
    private String myData;

    TextView txtSaldo;
    TextView txtInfo, txtStatus, txtHeaderStatus;
    ImageView imgStatus;

    private String mNohp;
    public static final String MYTAG = "EnterPinBillPayActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_pin_bill_payment);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mIdpel = intent.getStringExtra("idpel");
        mNamaPelanggan = intent.getStringExtra("namapelanggan");
        mPeriode = intent.getStringExtra("periode");
        mAmount = intent.getStringExtra("amount");
        mInquiryid = intent.getStringExtra("inquiryid");
        mSystrace  = intent.getStringExtra("systrace");
        mBillid    = intent.getStringExtra("billid");
        mAdminCA = intent.getIntExtra("adminca",0);
        mDebitSaldo = intent.getIntExtra("debitsaldo",0);
        mTipe = intent.getStringExtra("tipe");
        mProvider = intent.getStringExtra("provider");
        mKodeproduk = intent.getStringExtra("kodeproduk");
        if (mKodeproduk.equals("BILLBPJS")) {
            mNohp = intent.getStringExtra("nohp");
        }

        mUid = sessionManager.getId();

        myData = mKodeproduk+ " " +mIdpel+" " +mNamaPelanggan+" "+mPeriode+" "+mAmount+" "+mAdminCA+" "+mInquiryid+" "+mSystrace+" "+mBillid+" "+mDebitSaldo+
           " "+mTipe+" "+mProvider;
        Log.d(MYTAG , myData);
    }


    @OnClick(R.id.btn_batalbayartagihan)
    public void onBtnBatalbayartagihanClicked() {
        startActivity(new Intent(EnterPinBillPaymentActivity.this, MainTabActivity.class));
    }

    public void displayHome(View view) {
        startActivity(new Intent(EnterPinBillPaymentActivity.this, MainTabActivity.class));
    }

    public void goToLupaPin(View view) {
        startActivity(new Intent(EnterPinBillPaymentActivity.this, LupaPinActivity.class));
    }

    @OnClick(R.id.btn_bayartagihan)
    public void onBtnBayartagihanClicked() {
        String strPin = edtPin.getText().toString();

        if (strPin.equals("")) {
            Toast.makeText(this, "PIN masih kosong", Toast.LENGTH_SHORT).show();
        } else {
            showProgressDialog("");
            switch (mKodeproduk) {
                case "BILLPLN"  : doPaymentPLN(strPin);
                            break;

                case "BILLBPJS" : doPaymentBPJS(strPin);
                            break;

                case "BILLTELKOM" :
                case "BILLINDIHOME" : doPaymentPSTN(strPin);
                                  break;

                case "BILLPDAM": doPaymentPDAM(strPin);
                                 break;
            }

        }
    }

    private void doPaymentPDAM(String strPin) {

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        String sencPin = Suwunapps.getEncPin(strPin, mTimestamp);
        //$arrHeader['timestamp']. $arrHeader['appkey']. $merchant->secret_key . $idpel . $amount . $inquiryid . $systrace . $billid . $debitSaldo.$uid.$pin)
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mIdpel + mAmount + mSystrace + mDebitSaldo +
                mUid + sencPin );


        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseInfo> trxBillPayment = myAPI.paymentBillPDAM(mTimestamp, mIdpel, mAmount, mSystrace, mDebitSaldo, mUid, sencPin, mSignature, mProvider, mKodeproduk);

        trxBillPayment.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                AlertDialog.Builder alert= new AlertDialog.Builder(EnterPinBillPaymentActivity.this).setCancelable(false);
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_transaksi_bayar_tagihan, null);

                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);
                Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                btnOk.setVisibility(View.VISIBLE);

                if (result_code.equals("0000")) {
                    txtStatus.setText("SUKSES");
                    txtInfo.setText(response.body().getInfo() );
                    //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                    imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                    //update saldo di sessionManager
                    sessionManager.setSaldo(response.body().getBalance());

                } else if (result_code.equals("0168") || result_code.equals("2068")) {
                    txtStatus.setText("PENDING");
                    imgStatus.setImageResource(R.mipmap.ic_status_pending);
                    sessionManager.setSaldo(response.body().getBalance());

                    if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                        txtInfo.setText(result_desc);
                    } else {
                        txtInfo.setText(response.body().getInfo() );
                    }

                } else {
                    imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                    if (result_code.equals("1003")) {
                        //pin salah.
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        });
                    } else if (result_code.equals("3777")) {
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(market_uri));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                        .FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                        });

                    } else {
                        txtStatus.setText("GAGAL");
                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            txtInfo.setText(result_desc );
                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }
                    }
                }
                alert.setView(v);
                alert.show();
            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.\nCek daftar transaksi.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void doPaymentBPJS(String strPin) {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        String sencPin = Suwunapps.getEncPin(strPin, mTimestamp);
        //$arrHeader['timestamp']. $arrHeader['appkey']. $merchant->secret_key . $idpel . $amount . $inquiryid . $systrace . $billid . $debitSaldo.$uid.$pin)
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mIdpel + mAmount + mInquiryid + mSystrace + mBillid + mDebitSaldo +
                mUid + sencPin );


        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseInfo> trxBillPayment = myAPI.paymentBillBPJSKes(mTimestamp, mIdpel,mAmount,mInquiryid, mSystrace, mBillid, mDebitSaldo, mUid, sencPin, mSignature, mProvider, mKodeproduk, mNohp,mPeriode);

        trxBillPayment.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                AlertDialog.Builder alert= new AlertDialog.Builder(EnterPinBillPaymentActivity.this).setCancelable(false);
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_transaksi_bayar_tagihan, null);

                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);
                Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                btnOk.setVisibility(View.VISIBLE);

                if (result_code.equals("0000")) {
                    txtStatus.setText("SUKSES");
                    txtInfo.setText(response.body().getInfo() );
                    //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                    imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                    //update saldo di sessionManager
                    sessionManager.setSaldo(response.body().getBalance());

                } else if (result_code.equals("0168") || result_code.equals("2068")) {
                    txtStatus.setText("PENDING");
                    imgStatus.setImageResource(R.mipmap.ic_status_pending);
                    sessionManager.setSaldo(response.body().getBalance());

                    if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                        txtInfo.setText(result_desc);
                    } else {
                        txtInfo.setText(response.body().getInfo() );
                    }

                } else {
                    imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                    if (result_code.equals("1003")) {
                        //pin salah.
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        });
                    } else if (result_code.equals("3777")) {
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(market_uri));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                        .FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                        });

                    } else {
                        txtStatus.setText("GAGAL");
                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            txtInfo.setText(result_desc );
                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }
                    }
                }
                alert.setView(v);
                alert.show();
            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
//                String pesan = t.getMessage();
//                Toast.makeText(EnterPinBillPaymentActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
//                Log.d("EnterPinBillPayment", pesan, t);
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.\nCek daftar transaksi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Toast.makeText(this, ""+myData, Toast.LENGTH_LONG).show();
    }

    private void doPaymentPLN(String strPin) {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        String sencPin = Suwunapps.getEncPin(strPin, mTimestamp);
        //$arrHeader['timestamp']. $arrHeader['appkey']. $merchant->secret_key . $idpel . $amount . $inquiryid . $systrace . $billid . $debitSaldo.$uid.$pin)
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mIdpel + mAmount + mInquiryid + mSystrace + mBillid + mDebitSaldo +
                mUid + sencPin );


        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseInfo> trxBillPayment = myAPI.paymentBillPLN(mTimestamp, mIdpel,mAmount,mInquiryid, mSystrace, mBillid, mDebitSaldo, mUid, sencPin, mSignature, mProvider, mKodeproduk);

        trxBillPayment.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                AlertDialog.Builder alert= new AlertDialog.Builder(EnterPinBillPaymentActivity.this).setCancelable(false);
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_transaksi_bayar_tagihan, null);

                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);
                Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                btnOk.setVisibility(View.VISIBLE);

                if (result_code.equals("0000")) {
                    txtStatus.setText("SUKSES");
                    txtInfo.setText(response.body().getInfo() );
                    //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                    imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                    //update saldo di sessionManager
                    sessionManager.setSaldo(response.body().getBalance());

                } else if (result_code.equals("0168") || result_code.equals("2068")) {
                    txtStatus.setText("PENDING");
                    imgStatus.setImageResource(R.mipmap.ic_status_pending);
                    sessionManager.setSaldo(response.body().getBalance());

                    if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                        txtInfo.setText(result_desc);
                    } else {
                        txtInfo.setText(response.body().getInfo() );
                    }

                } else {
                    imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                    if (result_code.equals("1003")) {
                        //pin salah.
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        });

                    } else if (result_code.equals("3777")) {
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(market_uri));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                        .FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                        });

                    } else {
                        txtStatus.setText("GAGAL");
                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            txtInfo.setText(result_desc );
                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }
                    }
                }
                alert.setView(v);
                alert.show();
            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
//                String pesan = t.getMessage();
//                Toast.makeText(EnterPinBillPaymentActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
//                Log.d("EnterPinBillPayment", pesan, t);
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.\nCek daftar transaksi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Toast.makeText(this, ""+myData, Toast.LENGTH_LONG).show();

    }

    private void doPaymentPSTN(String strPin) {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        String sencPin = Suwunapps.getEncPin(strPin, mTimestamp);
        //$arrHeader['timestamp']. $arrHeader['appkey']. $merchant->secret_key . $idpel . $amount . $inquiryid . $systrace . $billid . $debitSaldo.$uid.$pin)
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mIdpel + mAmount + mInquiryid + mSystrace + mBillid + mDebitSaldo +
                mUid + sencPin );


        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseInfo> trxBillPayment = myAPI.paymentBillPstn(mTimestamp, mIdpel,mAmount,mInquiryid, mSystrace, mBillid, mDebitSaldo, mUid, sencPin, mSignature, mProvider, mKodeproduk);

        trxBillPayment.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                AlertDialog.Builder alert= new AlertDialog.Builder(EnterPinBillPaymentActivity.this).setCancelable(false);
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_transaksi_bayar_tagihan, null);

                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);
                Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                btnOk.setVisibility(View.VISIBLE);

                if (result_code.equals("0000")) {
                    txtStatus.setText("SUKSES");
                    txtInfo.setText(response.body().getInfo() );
                    //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                    imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                    //update saldo di sessionManager
                    sessionManager.setSaldo(response.body().getBalance());

                } else if (result_code.equals("0168") || result_code.equals("2068")) {
                    txtStatus.setText("PENDING");
                    imgStatus.setImageResource(R.mipmap.ic_status_pending);
                    sessionManager.setSaldo(response.body().getBalance());

                    if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                        txtInfo.setText(result_desc);
                    } else {
                        txtInfo.setText(response.body().getInfo() );
                    }

                } else {
                    imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                    if (result_code.equals("1003")) {
                        //pin salah.
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        });
                    } else if (result_code.equals("3777")) {
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(market_uri));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                        .FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                        });

                    } else {
                        txtStatus.setText("GAGAL");
                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            txtInfo.setText(result_desc );
                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }
                    }
                }
                alert.setView(v);
                alert.show();
            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
                //String pesan = t.getMessage();
                //Toast.makeText(EnterPinBillPaymentActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                //Log.d("EnterPinBillPayment", pesan, t);
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.\nCek daftar transaksi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Toast.makeText(this, ""+myData, Toast.LENGTH_LONG).show();

    }

}
