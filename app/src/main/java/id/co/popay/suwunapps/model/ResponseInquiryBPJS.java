package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResponseInquiryBPJS{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_info_bpjs")
	private ResultInfoBpjs resultInfoBpjs;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultInfoBpjs(ResultInfoBpjs resultInfoBpjs){
		this.resultInfoBpjs = resultInfoBpjs;
	}

	public ResultInfoBpjs getResultInfoBpjs(){
		return resultInfoBpjs;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseInquiryBPJS{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_info_bpjs = '" + resultInfoBpjs + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}