package id.co.popay.suwunapps.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class GantiPasswordActivity extends SessionManager {

    @BindView(R.id.edtOldPassword)
    EditText edtOldPassword;
    @BindView(R.id.edtNewPassword)
    EditText edtNewPassword;
    @BindView(R.id.edtConfirmPassword)
    EditText edtConfirmPassword;
    @BindView(R.id.gantiPasswordCardView)
    CardView gantiPasswordCardView;
    @BindView(R.id.btnUpdatePassword)
    Button btnUpdatePassword;

    private String mTimestamp, mSignature;
    private String mOldPassword, mNewPassword, mConfPassword;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_password);
        ButterKnife.bind(this);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btnUpdatePassword)
    public void onViewClicked() {
        if (edtOldPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Password lama harus diisi.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (edtNewPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Password baru harus diisi.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (edtConfirmPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Konfirmasi Password harus diisi.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (! edtNewPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            Toast.makeText(this, "Konfirmasi Password tidak sama.", Toast.LENGTH_SHORT).show();
            return;
        }

        //Toast.makeText(this, "Anda mau ganti password?", Toast.LENGTH_SHORT).show();
        gantiPassword();

    }

    private void gantiPassword() {
        showProgressDialog("");
        mOldPassword = edtOldPassword.getText().toString();
        mNewPassword = edtNewPassword.getText().toString();
        mConfPassword= edtConfirmPassword.getText().toString();

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mOldPassword + mNewPassword + sessionManager.getId());

        APIServices myApi = InitNetLibrary.getInstanceWc(this);
        Call<ResponseO> changePasswd = myApi.gantiPassword(mTimestamp,mOldPassword,mNewPassword,mConfPassword,sessionManager.getId(),mSignature);
        changePasswd.enqueue(new Callback<ResponseO>() {
            @Override
            public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    Toast.makeText(GantiPasswordActivity.this, "Password baru telah diset.", Toast.LENGTH_SHORT).show();
                    //startActivity(new Intent(GantiPinActivity.this, MainTabActivity.class));
                    onBackPressed();
                } else {
                    Toast.makeText(GantiPasswordActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseO> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(GantiPasswordActivity.this, "API failure gp\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_LONG).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
