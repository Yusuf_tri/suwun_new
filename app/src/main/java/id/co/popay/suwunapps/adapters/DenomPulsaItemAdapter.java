package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.DenomPulsaItem;

/**
 * Created by ara on 06/02/18.
 */

public class DenomPulsaItemAdapter extends RecyclerView.Adapter<DenomPulsaItemAdapter.MyViewHolder>  {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<DenomPulsaItem> mDenomList;

    private OnItemClickListener mListener;

    public void setListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public DenomPulsaItemAdapter(Context context, List<DenomPulsaItem> denomList) {
        mContext = context;
        mDenomList = denomList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public DenomPulsaItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_denom_pulsa, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DenomPulsaItemAdapter.MyViewHolder holder, int position) {
        final DenomPulsaItem denomPulsaItem = mDenomList.get(position);
        holder.denomTextView.setText(String.format("%,d" , Integer.parseInt(String.valueOf(denomPulsaItem.getNominal()))).replace(',','.'));

        if (denomPulsaItem.getInfo() != null) {
            //holder.keteranganTextView.setText(denomPulsaItem.getKeterangan()+ "\r\n" + denomPulsaItem.getInfo());
            holder.keteranganTextView.setText(denomPulsaItem.getKeterangan());
            holder.infoTextView.setText(denomPulsaItem.getInfo());
        } else {
            holder.keteranganTextView.setText(denomPulsaItem.getKeterangan());
            holder.infoTextView.setText("-");
        }

        if (denomPulsaItem.getIspromo().equals("1")) {
            holder.promoTextView.setText("PROMO!");
            holder.promoTextView.setVisibility(View.VISIBLE);
            holder.promoTextView.setTextColor(Color.RED);
        } else if (denomPulsaItem.getIspromo().equals("2")) {
            holder.promoTextView.setText("GANGGUAN");
            holder.promoTextView.setVisibility(View.VISIBLE);
            holder.promoTextView.setTextColor(Color.BLUE);
        } else if (denomPulsaItem.getIspromo().equals("0")) {
            holder.promoTextView.setVisibility(View.GONE);
        }

        holder.hargaTextView.setText(String.format("Rp %,d" , Integer.parseInt(String.valueOf(denomPulsaItem.getHarga()))).replace(',','.'));

        if (mListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.OnItemClickListener(denomPulsaItem.getKodeproduk(),
                            denomPulsaItem.getKeterangan(), denomPulsaItem.getNominal(),
                            denomPulsaItem.getHarga(),
                            denomPulsaItem.getInfo());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mDenomList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView denomTextView, keteranganTextView, hargaTextView, infoTextView, promoTextView;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            denomTextView = (TextView) itemView.findViewById(R.id.txtDenom);
            keteranganTextView = (TextView) itemView.findViewById(R.id.txtKeterangan);
            hargaTextView = (TextView) itemView.findViewById(R.id.txtHarga);
            infoTextView =(TextView) itemView.findViewById(R.id.txtTambahan);
            promoTextView = (TextView) itemView.findViewById(R.id.txtPromo);
            cardView = (CardView) itemView.findViewById(R.id.container);
        }
    }

    public interface OnItemClickListener {
        void OnItemClickListener(String productId, String productDesc, String productNominal, String productPrice, String productInfo);
    }

}
