package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.ResultTrxItem;

/**
 * Created by ara on 04/04/18.
 */

public class TransaksiAdapter extends RecyclerView.Adapter<TransaksiAdapter.MyViewHolder>{
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<ResultTrxItem> mTransaksiList;

    private OnItemClickListener mListener;

    public void setListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public TransaksiAdapter(Context context, List<ResultTrxItem> transaksiList) {
        mContext = context;
        mTransaksiList = transaksiList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public TransaksiAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = mLayoutInflater.inflate(R.layout.item_daftar_transaksi, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TransaksiAdapter.MyViewHolder holder, int position) {

        ResultTrxItem trxItem = mTransaksiList.get(position);
        String kodeproduk = trxItem.getProductid();
        holder.txtItem.setText(trxItem.getKeterangan());
        if (kodeproduk.length()>=4) {
            if (kodeproduk.substring(0,4).equals("BILL")) {
                holder.txtHarga.setText("Bayar " + String.format("Rp. %,d", Integer.parseInt(trxItem.getPrice())).replace(',', '.') + ", IDPel: " + trxItem.getTargetNumber());
            } else {
                holder.txtHarga.setText("Harga " + String.format("Rp. %,d", Integer.parseInt(trxItem.getPrice())).replace(',', '.') + ", ke " + trxItem.getTargetNumber());
            }
        } else {
            holder.txtHarga.setText("Harga " + String.format("Rp. %,d", Integer.parseInt(trxItem.getPrice())).replace(',', '.') + ", ke " + trxItem.getTargetNumber());
        }
        holder.txtTanggal.setText(trxItem.getCreatedAt());

        if (trxItem.getStatus().equals("SUKSES")) {
            holder.txtStatus.setText(trxItem.getStatus() +", " + trxItem.getInfo());
        } else {
            if (trxItem.getStatus().equals("PENDING")) {
                holder.txtStatus.setText("SEDANG DIPROSES");
            } else {
                holder.txtStatus.setText(trxItem.getStatus() );
            }

        }

        switch (trxItem.getOperator()) {
                case "TELKOMSEL" : holder.imgOperator.setImageResource(R.drawable.logo_telkomsel); break;
                case "INDOSAT"   : holder.imgOperator.setImageResource(R.drawable.logo_indosat); break;
                case "XL"        : holder.imgOperator.setImageResource(R.drawable.logo_xl); break;
                case "SMARTFREN" : holder.imgOperator.setImageResource(R.drawable.logo_smartfren); break;
                case "AXIS"      : holder.imgOperator.setImageResource(R.drawable.logo_axis); break;
                case "TRI"       : holder.imgOperator.setImageResource(R.drawable.logo_three); break;
                case "GOJEK"     : holder.imgOperator.setImageResource(R.drawable.logo_gojek);break;
                case "PLN"       : holder.imgOperator.setImageResource(R.drawable.ic_logo_pln);break;
            case "TELKOM"    : holder.imgOperator.setImageResource(R.mipmap.ic_logotelkomtangan);break;
            case "BPJS"      : holder.imgOperator.setImageResource(R.drawable.ic_bpjskesehatan);break;
            case "PDAM"      : holder.imgOperator.setImageResource(R.drawable.ic_pdam); break;
            case "EMONEY"    : holder.imgOperator.setImageResource(R.drawable.logo_emoney); break;
            case "OVO"       : holder.imgOperator.setImageResource(R.drawable.logo_ovo); break;
            case "TCASH"     : holder.imgOperator.setImageResource(R.drawable.logo_tcash); break;
            case "DANA"      : holder.imgOperator.setImageResource(R.drawable.logo_dana); break;
                default : holder.imgOperator.setImageResource(R.drawable.ic_nogame);
        }

    }

    @Override
    public int getItemCount() {
        return mTransaksiList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtItem, txtHarga, txtStatus, txtTanggal;
        ImageView imgOperator;
        CardView cardView;

        public MyViewHolder(View itemview) {
            super(itemview);
            //setting semua widget di dalem view layout_list_transaksi
            txtItem = (TextView) itemview.findViewById(R.id.txtItem);
            txtHarga= (TextView) itemview.findViewById(R.id.txtHarga);
            txtStatus= (TextView) itemview.findViewById(R.id.txtStatus);
            txtTanggal = (TextView) itemview.findViewById(R.id.txtTanggal);
            imgOperator = (ImageView) itemview.findViewById(R.id.imgOperator);
            cardView = (CardView) itemView.findViewById(R.id.container);
        }
    }

    public interface OnItemClickListener {
        void OnItemClickListener(String trxItem, String trxHarga, String trxStatus, String trxTanggal, String trxOperator);
    }
}
