package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import id.co.popay.suwunapps.R;

/**
 * Created by ara on 02/01/18.
 */

public class BlokMenuItemAdapter extends RecyclerView.Adapter<BlokMenuItemAdapter.MyViewHolder>{

    Integer gambarIcon[] = {
                            R.mipmap.ic_blokmenu_selular, R.mipmap.ic_blokmenu_internet, R.mipmap.ic_blokmenu_gamev,
                            R.mipmap.ic_blokmenu_plntoken, R.mipmap.ic_blokmenu_gopay, R.mipmap.ic_blokmenu_bpjskesehatan,
                            R.mipmap.ic_blokmenu_telepon, R.mipmap.ic_blokmenu_plntagihan, R.mipmap.ic_blokmenu_pdam ,
                            R.mipmap.ic_blokmenu_emoney, R.mipmap.ic_blokmenu_ovo,R.mipmap.ic_blokmenu_tcash,
                            R.mipmap.ic_blokmenu_dana, R.mipmap.ic_blokmenu_redeem };

    String textIcon[] = { "Pulsa HP","Paket Data", "Voucher Game",
                         "Token PLN","Gopay Customer","BPJS Kesehatan",
                         "Tagihan Telepon Rumah", "Tagihan PLN", "PDAM",
                         "Saldo e-money" , "Saldo Ovo", "Saldo Tcash" ,
                         "Saldo Dana", "Tukar Poin"};

    boolean enabledFlag[] = {true,true, true, true, true, true, true, true, true, true, true, true, true, true, true};

    /*
    Integer gambarIcon[] = {R.mipmap.ic_blokmenu_selular, R.mipmap.ic_blokmenu_internet, R.mipmap.ic_blokmenu_gamev,
            R.mipmap.ic_blokmenu_plntoken, R.mipmap.ic_blokmenu_gopay, R.mipmap.ic_blokmenu_plntagihan_disabled
            };
    String textIcon[] = {"Pulsa HP","Paket Data", "Voucher Game",
            "Token PLN","Gopay Customer","Tagihan PLN"};

    boolean enabledFlag[] = {true,true, true, true, true,false};
*/

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private OnItemClickListener mListener;

    public void setListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public BlokMenuItemAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_blokmenu, parent,false );
        return new MyViewHolder(view) ;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.imageView.setImageResource(gambarIcon[position]);
        holder.nameTextView.setText(textIcon[position].replace(',','\n'));

        if (!enabledFlag[position]) {
            //holder.cardView.setCardBackgroundColor(Color.LTGRAY);
            //holder.nameTextView.setTextColor(Color.WHITE);
        }

        if (mListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //mListener.OnItemClickListener(textIcon[position]);
                    mListener.OnItemClickListener(gambarIcon[position]);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return gambarIcon.length;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameTextView;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            nameTextView = (TextView) itemView.findViewById(R.id.tv_name);
            cardView = (CardView) itemView.findViewById(R.id.container);

        }
    }

    public interface OnItemClickListener {
        void OnItemClickListener(int itemId);
    }
}
