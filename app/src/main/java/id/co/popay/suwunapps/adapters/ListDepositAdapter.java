package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.ResultDepositItem;

/**
 * Created by ara on 10/04/18.
 */

public class ListDepositAdapter extends RecyclerView.Adapter<ListDepositAdapter.MyViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<ResultDepositItem> mDataDeposit;

    public ListDepositAdapter(Context context, List<ResultDepositItem> dataDeposit) {
        mContext = context;
        mDataDeposit = dataDeposit;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ListDepositAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_daftar_deposit, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListDepositAdapter.MyViewHolder holder, int position) {
        //setting semua widget di dalem view layout_list_transaksi
        ResultDepositItem depositItem = mDataDeposit.get(position);
        String namaBank = depositItem.getNamabank();
        String strItem = "";
        if (namaBank.equals("ALFAMART")) {
            strItem = namaBank;
        } else {
            strItem = "Bank " + namaBank;
        }
        String strJumlahTransfer= String.format("Jumlah Transfer: Rp. %,d", Integer.parseInt(depositItem.getJumlahtransfer())).replace(',', '.') ;
        String strStatus = depositItem.getStatus();

        holder.txtItem.setText(strItem);
        holder.txtJumlahTransfer.setText(strJumlahTransfer);

        holder.txtTanggal.setText(depositItem.getCreatedAt());

        switch (strStatus) {
            case "1" :  holder.txtStatus.setText("SEDANG DIPROSES");
                        holder.txtStatus.setTextColor(Color.BLUE);
                        break;

            case "2" :  holder.txtStatus.setText("SUDAH DIPROSES");
                        holder.txtStatus.setTextColor(Color.DKGRAY);
                        holder.txtStatus.setBackgroundResource(R.drawable.button_sukses);
                        break;

            case "0" :  holder.txtStatus.setText("EXPIRED");
                        holder.txtStatus.setTextColor(Color.RED);
                        break;

            default   : holder.txtStatus.setText("SEDANG DIPROSES");
        }


    }

    @Override
    public int getItemCount() {
        return mDataDeposit.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtItem ;
        TextView txtJumlahTransfer;
        TextView txtStatus;
        TextView txtTanggal;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtItem = (TextView) itemView.findViewById(R.id.txtItem);
            txtJumlahTransfer= (TextView) itemView.findViewById(R.id.txtJumlahTransfer);
            txtStatus= (TextView) itemView.findViewById(R.id.txtStatus);
            txtTanggal = (TextView) itemView.findViewById(R.id.txtTanggal);
            cardView = (CardView) itemView.findViewById(R.id.container);
        }
    }
}
