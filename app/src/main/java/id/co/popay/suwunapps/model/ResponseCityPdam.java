package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseCityPdam{

	@SerializedName("cities")
	private List<CitiesItem> cities;

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	public void setCities(List<CitiesItem> cities){
		this.cities = cities;
	}

	public List<CitiesItem> getCities(){
		return cities;
	}

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseCityPdam{" + 
			"cities = '" + cities + '\'' + 
			",result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}