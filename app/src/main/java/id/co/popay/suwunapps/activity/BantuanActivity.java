package id.co.popay.suwunapps.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.BuildConfig;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseKonfigurasi;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BantuanActivity extends AppCompatActivity {

    @BindView(R.id.imgInfo)
    ImageView imgInfo;
    @BindView(R.id.txtIntro)
    TextView txtIntro;
    @BindView(R.id.imgWA)
    ImageView imgWA;
    @BindView(R.id.txtBantuanWA)
    TextView txtBantuanWA;
    @BindView(R.id.imgEmailAddress)
    ImageView imgEmailAddress;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.imgIG)
    ImageView imgIG;
    @BindView(R.id.txtInstagram)
    TextView txtInstagram;
    @BindView(R.id.cardview_IG)
    CardView cardviewIG;
    @BindView(R.id.imgFB)
    ImageView imgFB;
    @BindView(R.id.txtFacebook)
    TextView txtFacebook;
    @BindView(R.id.cardview_FB)
    CardView cardviewFB;
    @BindView(R.id.versionName)
    TextView versionName;

    String IMEI_Number_Holder = "";
    TelephonyManager telephonyManager;
    private final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;
    String _versionName = BuildConfig.VERSION_NAME;
    int _versionCode = BuildConfig.VERSION_CODE;

    @RequiresApi(api = 26)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            IMEI_Number_Holder = telephonyManager.getDeviceId();
        } else {
            ActivityCompat.requestPermissions(BantuanActivity.this,
                        new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);

        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bantuan);
        ButterKnife.bind(this);
        Log.d("BantuanActivity", _versionName + "-" + _versionCode);
        if (!IMEI_Number_Holder.equals("")) {
            versionName.setText("Version " + _versionName + "." + _versionCode + "\n" + IMEI_Number_Holder);
        } else {
            versionName.setText("Version " + _versionName + "." + _versionCode );
        }




//        String dbVersionName = Suwunapps.checkCurrentAppVersion();

//        //cek APP_VERSiON terkini dari database server
//        if (! _versionName.equals(dbVersionName)) {
//            AlertDialog.Builder dialog = new AlertDialog.Builder(BantuanActivity.this);
//            dialog.setTitle("Upgrade Aplikasi");
//            dialog.setMessage("Versi terbaru sudah tersedia di PLAYSTORE.\nSilakan lakukan upgrade aplikasi.");
//            dialog.setPositiveButton("Upgrade", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse(market_uri));
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
//                            .FLAG_ACTIVITY_NO_ANIMATION);
//                    startActivity(intent);
//                }
//            });
//
//            dialog.setNegativeButton("Nanti saja", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    dialogInterface.dismiss();
//                }
//            });
//
//            dialog.show();
//
//        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    IMEI_Number_Holder = telephonyManager.getDeviceId();
                    versionName.setText("Version " + _versionName + "." + _versionCode + "\n" + IMEI_Number_Holder);
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void goToInstagram(View view) {
        Uri uriUrl = Uri.parse("http://www.instagram.com/suwunindonesia");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void goToFacebook(View view) {
        Uri uriFBUrl = Uri.parse("https://www.facebook.com/Suwun-Indonesia-333382990507614/");
        Intent launchBrowserFB = new Intent(Intent.ACTION_VIEW, uriFBUrl);
        startActivity(launchBrowserFB);
    }

    @OnClick(R.id.versionName)
    public void onViewClicked() {
        String mTimestamp, mSignature;
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET );

        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(BantuanActivity.this);
        Call<ResponseKonfigurasi> cekConfig = myAPI.getKonfig(mTimestamp,  mSignature);
        cekConfig.enqueue(new Callback<ResponseKonfigurasi>() {
            @Override
            public void onResponse(Call<ResponseKonfigurasi> call, Response<ResponseKonfigurasi> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    String vn = id.co.popay.suwunapps.BuildConfig.VERSION_NAME;
                    String vc = String.valueOf(BuildConfig.VERSION_CODE);

                    List<String> listKonfig = response.body().getResultInfokonfig();
                    Log.d("splashscreen", result_desc);
                    if (! vc.equals(result_desc) ) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(BantuanActivity.this);
                        dialog.setTitle("Upgrade Aplikasi");
                        dialog.setCancelable(false);
                        dialog.setMessage(
                                "Versi terbaru sudah tersedia di PLAYSTORE.\nSilakan lakukan upgrade aplikasi.");
                        dialog.setPositiveButton("UPGRADE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(market_uri));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                        .FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                        });

                        dialog.setNegativeButton("NANTI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });

                        dialog.show();

                    } else {
                        Toast.makeText(BantuanActivity.this, "Versi terpasang sudah paling update.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(BantuanActivity.this, "Failed to check app version", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseKonfigurasi> call, Throwable t) {
                Toast.makeText(BantuanActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
