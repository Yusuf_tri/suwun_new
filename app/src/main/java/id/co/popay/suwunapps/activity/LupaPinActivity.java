package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class LupaPinActivity extends SessionManager {

    @BindView(R.id.btn_kirimkode)
    Button btnKirimkode;
    @BindView(R.id.edt_kodereset)
    TextInputEditText edtKodereset;
    @BindView(R.id.edt_passwordbaru)
    TextInputEditText edtPasswordbaru;
    @BindView(R.id.btn_resetpin)
    Button btnResetpin;

    String mTimestamp, mSignature;
    String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_pin);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_kirimkode)
    public void onBtnKirimkodeClicked() {
        kirimKodeResetPin();
    }

    private void kirimKodeResetPin() {
            showProgressDialog("");
            APIServices myApi = InitNetLibrary.getInstances();
            mEmail = sessionManager.getEmail();

            mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
            mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mEmail );

            Call<ResponseO> kirimKode = myApi.sendCode(mTimestamp, mEmail, mSignature );
            kirimKode.enqueue(new Callback<ResponseO>() {
                @Override
                public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                    hideProgressDialog();
                    String result_code = response.body().getResultCode();
                    String result_desc = response.body().getResultDesc();
                    if (result_code.equals("0000")) {
                        Toast.makeText(LupaPinActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LupaPinActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseO> call, Throwable t) {
                    hideProgressDialog();
                    //Toast.makeText(LupaPinActivity.this, "API failure kirimKode\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_LONG).show();
                    if (t.getMessage() != null ) {
                        Toast.makeText(getApplicationContext(), "API kirimKode Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "API kirimKode Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
    }

    @OnClick(R.id.btn_resetpin)
    public void onBtnResetpinClicked() {
        if (TextUtils.isEmpty(edtKodereset.getText().toString())) {
            Toast.makeText(this, "Kode reset tidak boleh kosong. Cek email.", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(edtPasswordbaru.getText().toString())) {
            Toast.makeText(this, "PIN tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        } else {
            resetPin();
        }

    }

    private void resetPin() {
        showProgressDialog("");
        APIServices myApi = InitNetLibrary.getInstances();
        mEmail = sessionManager.getEmail();
        String strPin = edtPasswordbaru.getText().toString();
        String strKodeReset = edtKodereset.getText().toString();
        String strUid = sessionManager.getUID();

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + strKodeReset + strPin + mEmail + strUid  );

        Call<ResponseO> resetPin = myApi.updatePin(mTimestamp, strKodeReset, strPin, mEmail, strUid, mSignature);
        resetPin.enqueue(new Callback<ResponseO>() {
            @Override
            public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    Toast.makeText(LupaPinActivity.this, "PIN baru telah diset.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LupaPinActivity.this, MainTabActivity.class));
                } else {
                    Toast.makeText(LupaPinActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseO> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(LupaPinActivity.this, "API failure updatePin\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_LONG).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API updatePIN Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API updatePIN Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
