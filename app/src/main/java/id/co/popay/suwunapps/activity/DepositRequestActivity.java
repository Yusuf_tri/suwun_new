package id.co.popay.suwunapps.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.NumberTextWatcher;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class DepositRequestActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.spn_bank)
    Spinner spnBank;
    @BindView(R.id.edt_requestDesposit)
    EditText edtRequestDesposit;
    @BindView(R.id.btnRequestDeposit)
    Button btnRequestDeposit;
    @BindView(R.id.btn_refresh)
    ImageView btnRefresh;


    WebView webPanduanDeposit;

    String signature = "131231231";

    //String[] namaBank = {"BCA (Norek: 6050-47-5555)", "BRI (Norek: 0509-01-001043-30-4)", "MANDIRI (Norek: 164-0001-525791)"};
    //String[] namaBank = {"BCA", "BRI", "MANDIRI", "ALFAMART"};
    String[] namaBank = {"BCA", "BRI", "MANDIRI"};
    String bankSelected = "";
    Intent intent;

    TextView txtStatus, txtInfo, txtJumlahTransfer, txtPanduanDeposit,  txtFooter;
    LinearLayout container_infotransfer;
    @BindView(R.id.txtTransferVA)
    TextView txtTransferVA;
    @BindView(R.id.layout_transfer_va)
    LinearLayout layoutTransferVa;
    @BindView(R.id.txtTransferRekening)
    TextView txtTransferRekening;
    @BindView(R.id.layout_transferRek)
    LinearLayout layoutTransferRek;
    @BindView(R.id.img_onlinesupport)
    ImageView imgOnlinesupport;
    @BindView(R.id.txt_wanumber)
    TextView txtWanumber;
    @BindView(R.id.img_logowa)
    ImageView imgLogowa;
    @BindView(R.id.txtInfoAlfamart)
    TextView txtInfoAlfamart;
    private String mSaldo = "0";
    private String mTimestamp, mSignature;


    LinearLayout layoutTransfer , layoutVA ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit_request);
        ButterKnife.bind(this);

        layoutTransfer = (LinearLayout) findViewById(R.id.layout_transferRek);
        layoutVA = (LinearLayout) findViewById(R.id.layout_transfer_va);


        //setting text txtSaldo dengan data saldo
        getSaldoFromServer();
        txtSaldo = (TextView) findViewById(R.id.txtSaldo);
        txtInfoAlfamart = (TextView) findViewById(R.id.txtInfoAlfamart);
        txtFooter = (TextView) findViewById(R.id.txt_footer);
        txtInfoAlfamart.setText("SUWUN. Dengan nomer pelanggan 32123" + sessionManager.getMsisdn());
        txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(sessionManager.getSaldo())).replace(',', '.'));

        //populate spinner
        ArrayAdapter<String> adapterBank = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, namaBank);
        adapterBank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnBank.setAdapter(adapterBank);


        edtRequestDesposit.addTextChangedListener(new NumberTextWatcher(edtRequestDesposit));

        //listener on spinner
        spnBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankSelected = spnBank.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //setting buat panduan deposit pake html
        InputStream stream;
        String text;
        try {
            stream = getAssets().open("depositguide.html");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            text = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
            text = "No data available";
        }

//        txtPanduanDeposit = (TextView) findViewById(R.id.txtPanduanDeposit);
//        txtPanduanDeposit.setText(Html.fromHtml(text,null,new MyTagHandler()));
        //webPanduanDeposit = (WebView) findViewById(R.id.webPanduanDeposit);
        //setting webview
//        webPanduanDeposit.getSettings().setJavaScriptEnabled(true);
        //webPanduanDeposit.getSettings().setAppCacheEnabled(true);

//        webPanduanDeposit.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//                return super.shouldOverrideUrlLoading(view, request);
//            }
//        });
//

        //webPanduanDeposit.loadDataWithBaseURL(null, text, "text/html", "utf-8", null);
        //webPanduanDeposit.loadUrl("http://www.popay.co.id/suwun/depositguide");


    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_deposit_request, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_history) {
            //return true;
            startActivity(new Intent(DepositRequestActivity.this, ListDepositActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.btnRequestDeposit)
    public void onViewClicked() {
        final String jumlahTransfer;
        jumlahTransfer = edtRequestDesposit.getText().toString().replace(",", "").replace(".", "");

        //Toast.makeText(this, "Bank: " + bankSelected +"\nJumlah Transfer: " + jumlahTransfer , Toast.LENGTH_LONG).show();
        intent = new Intent(DepositRequestActivity.this, MainTabActivity.class);

        if (jumlahTransfer.equals("")) {
            Toast.makeText(this, "Jumlah transfer belum diisi", Toast.LENGTH_SHORT).show();
        } else {
            if (Integer.parseInt(jumlahTransfer) < 10000) {
                Toast.makeText(this, "Jumlah transfer minimal Rp 10.000", Toast.LENGTH_SHORT).show();
            } else {

                AlertDialog.Builder dialog = new AlertDialog.Builder(DepositRequestActivity.this);
                dialog.setTitle("Konfirmasi Transfer");
                String pesan = "";
                if (bankSelected.equals("ALFAMART")) {
                    pesan = "Anda akan melakukan permintaan topup saldo sebesar Rp. " + edtRequestDesposit.getText().toString().replace(",", ".") +
                            "\n" + "lewat " + bankSelected + ".\n\n";

                } else {
                    pesan = "Anda akan melakukan permintaan transfer sebesar Rp. " + edtRequestDesposit.getText().toString().replace(",", ".") +
                            "\n" + "ke Bank " + bankSelected + ".\n\n";
                }
                dialog.setMessage(pesan);

                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(DepositRequest.this, "Anda Setuju ya kalo gitu...", Toast.LENGTH_SHORT).show();
                        makeDeposit(bankSelected, jumlahTransfer);
                    }
                });

                dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        }

    }

    private void makeDeposit(final String bankSelected, String jumlahTransfer) {
        //call API deposit

        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(DepositRequestActivity.this);
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        Call<ResponseO> reqDepo = myAPI.requestDeposit(mTimestamp, sessionManager.getId(), bankSelected, jumlahTransfer, signature);

        reqDepo.enqueue(new Callback<ResponseO>() {
            @Override
            public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                //kluarin custom alert
                AlertDialog.Builder alert = new AlertDialog.Builder(DepositRequestActivity.this);
                alert.setCancelable(false);

                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_request_deposit, null);
                //akses widget yg ada di layout custom
                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                txtJumlahTransfer = (TextView) v.findViewById(R.id.txtJumlahTransfer);
                txtFooter = (TextView) v.findViewById(R.id.txt_footer);

                container_infotransfer = (LinearLayout) v.findViewById(R.id.container_infotransfer);

                if (result_code.equals("0000")) {
                    container_infotransfer.setVisibility(View.VISIBLE);
                    txtStatus.setText("SUKSES");

                    if (bankSelected.equals("BCA")) {
                        txtInfo.setText("Lakukan transfer ke Bank\n" + bankSelected + "\n" +
                                        "no. rekening 6050-47-55555\n"+
                                        "a.n. PT Redision Teknologi Indonesia\nsejumlah tepat " );
                        txtJumlahTransfer.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                        txtFooter.setVisibility(View.VISIBLE);
                        txtFooter.setText("\n\n"+"Atau dengan transfer ke BCA Virtual Account.\n" +
                                "Nomor Virtual Account Anda: 10434" + sessionManager.getMsisdn());

                    } else {
                        String norek = "";
                        if (bankSelected.equals("BRI")) {
                            norek = "0509-01-001043-30-4";
                        } else {
                            norek = "164-0001-525791";
                        }
                        txtInfo.setText("Lakukan transfer ke Bank\n" + bankSelected + "\n" +
                                "no. rekening " + norek + "\n" +
                                "a.n. PT Redision Teknologi Indonesia\nsejumlah tepat ");
                        txtJumlahTransfer.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                        txtFooter.setVisibility(View.VISIBLE);
                    }


                } else {
                    txtStatus.setText("GAGAL");
                    txtInfo.setText(result_desc);
                    container_infotransfer.setVisibility(View.GONE);
                }
                alert.setView(v);
                alert.show();
            }

            @Override
            public void onFailure(Call<ResponseO> call, Throwable t) {
                if (t.getMessage() == null) {
                    Toast.makeText(getApplicationContext(), "API md TIMEOUT.\nCek daftar history deposit.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API md FAILED, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void displayHome(View view) {
        //startActivity(new Intent(DepositRequestActivity.this, MainTabActivity.class));
        startActivity(new Intent(DepositRequestActivity.this, ListDepositActivity.class));
    }

    private void getSaldoFromServer() {
        showProgressDialog("Cek Saldo");
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());
        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(DepositRequestActivity.this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    sessionManager.setPoin(result_poin);
                    mSaldo = result_desc;
                    Log.d("DepositRequestActivity", mSaldo);
                } else if (result_code.equals("0103")) {
                    Toast.makeText(getApplicationContext(), "Session Anda habis. Silakan login ulang (E" + result_code + "). ", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(DepositRequestActivity.this, LoginActivity.class));

                } else  {
                    Toast.makeText(getApplicationContext(), "Gagal ambil profile (E" + result_code + "). ", Toast.LENGTH_LONG).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    Log.d("MainTab", result_desc + " " + result_poin);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0")).replace(',', '.'));
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() == null) {
                    Toast.makeText(getApplicationContext(), "API cs Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void displayListDeposit(View view) {
        startActivity(new Intent(DepositRequestActivity.this, ListDepositActivity.class));
    }


    public void doRefreshSaldo(View view) {
        getSaldoFromServer();
    }

    @OnClick(R.id.txtTransferVA)
    public void onTxtTransferVAClicked() {
        if (layoutVA.getVisibility() == View.VISIBLE) {
            layoutVA.setVisibility(View.GONE);
            txtTransferVA.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chevron_right_black24,0);
        } else {
            layoutVA.setVisibility(View.VISIBLE);
            txtTransferVA.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chevron_down_black24,0);
        }
    }

    @OnClick(R.id.txtTransferRekening)
    public void onTxtTransferRekeningClicked() {
        if (layoutTransfer.getVisibility() == View.VISIBLE) {
            layoutTransfer.setVisibility(View.GONE);
            txtTransferRekening.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chevron_right_black24,0);
        } else {
            layoutTransfer.setVisibility(View.VISIBLE);
            txtTransferRekening.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_chevron_down_black24,0);
        }
    }
}