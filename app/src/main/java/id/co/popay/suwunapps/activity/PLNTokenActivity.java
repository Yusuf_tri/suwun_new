package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.DenomPulsaItem;
import id.co.popay.suwunapps.model.ResponseDenomPulsa;
import id.co.popay.suwunapps.model.ResponseInquiryPLN;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class PLNTokenActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;
    @BindView(R.id.saldoBar)
    LinearLayout saldoBar;
    @BindView(R.id.edtIdPel)
    EditText edtIdPel;
    @BindView(R.id.idPelangganCardView)
    CardView idPelangganCardView;
    @BindView(R.id.txtLabel20K)
    Button txtLabel20K;
    @BindView(R.id.txtLabel50K)
    Button txtLabel50K;
    @BindView(R.id.txtLabel100K)
    Button txtLabel100K;
    @BindView(R.id.txtLabel200K)
    Button txtLabel200K;
    @BindView(R.id.txtLabel500K)
    Button txtLabel500K;
    @BindView(R.id.txtLabel1000K)
    Button txtLabel1000K;
    @BindView(R.id.denomCard)
    CardView denomCard;
    @BindView(R.id.txtLabelNomerMeter)
    TextView txtLabelNomerMeter;
    @BindView(R.id.txtNomerMeter)
    TextView txtNomerMeter;
    @BindView(R.id.txtLabelNama)
    TextView txtLabelNama;
    @BindView(R.id.txtNamaPelanggan)
    TextView txtNamaPelanggan;
    @BindView(R.id.txtLabelDaya)
    TextView txtLabelDaya;
    @BindView(R.id.txtDaya)
    TextView txtDaya;
    @BindView(R.id.txtLabelNominal)
    TextView txtLabelNominal;
    @BindView(R.id.txtNominal)
    TextView txtNominal;
    @BindView(R.id.txtLabelAdminCA)
    TextView txtLabelAdminCA;
    @BindView(R.id.txtAdminCA)
    TextView txtAdminCA;
    @BindView(R.id.txtLabelTotal)
    TextView txtLabelTotal;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.LinearLayout)
    LinearLayout mLinearLayout;
    @BindView(R.id.btnCekPelanggan)
    Button btnCekPelanggan;

    private String mTimestamp, mSignature;
    private String mNominal;
    private String mIdPel;
    private TextWatcher txtWatcherIdPel = null;

    private String mNamaPelanggan;
    private String mMeter;
    private String mDaya;
    private String mInquiryId;
    private String mSystrace;
    private int mBillid;
    private int mAdminCA = 0;
    private int mTotal ;
    private String mKodeProduk;
    List<DenomPulsaItem> resultDenomPulsa = new ArrayList<>();
    private String mHarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plntoken);
        ButterKnife.bind(this);
        denomCard.setVisibility(View.INVISIBLE);
        purchaseDetail.setVisibility(View.INVISIBLE);
        btnBeli.setVisibility(View.INVISIBLE);

        txtWatcherIdPel = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 11) {
                    btnCekPelanggan.setEnabled(true);
                    btnCekPelanggan.setBackgroundResource(R.drawable.button_bg);
                } else {
                    btnCekPelanggan.setEnabled(false);
                    btnCekPelanggan.setBackgroundResource(R.color.colorGrid);
                    denomCard.setVisibility(View.INVISIBLE);
                    purchaseDetail.setVisibility(View.INVISIBLE);
                    btnBeli.setVisibility(View.INVISIBLE);
                    btnBeli.setEnabled(false);
                    btnBeli.setBackgroundResource(R.color.colorGrid);
                    clearDenom();
                }
            }
        };

        edtIdPel.addTextChangedListener(txtWatcherIdPel);
        getSaldoFromServer();
        loadListDenom("PLN");
    }


    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(PLNTokenActivity.this, DepositRequestActivity.class));
    }

    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        //Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.txtLabel20K)
    public void onTxtLabel20KClicked() {
        changeDenom("20");
    }


    @OnClick(R.id.txtLabel50K)
    public void onTxtLabel50KClicked() {
        changeDenom("50");
    }

    @OnClick(R.id.txtLabel100K)
    public void onTxtLabel100KClicked() {
        changeDenom("100");
    }

    @OnClick(R.id.txtLabel200K)
    public void onTxtLabel200KClicked() {
        changeDenom("200");
    }

    @OnClick(R.id.txtLabel500K)
    public void onTxtLabel500KClicked() {
        changeDenom("500");
    }

    @OnClick(R.id.txtLabel1000K)
    public void onTxtLabel1000KClicked() {
        changeDenom("1000");
    }

    @OnClick(R.id.btnCekPelanggan)
    public void onBtnCekPelangganClicked() {
        showProgressDialog("");
        clearDenom();
        btnBeli.setEnabled(false);
        purchaseDetail.setVisibility(View.INVISIBLE);
        denomCard.setVisibility(View.INVISIBLE);
        btnBeli.setVisibility(View.INVISIBLE);
        btnBeli.setBackgroundResource(R.color.colorGrid);

        mIdPel = edtIdPel.getText().toString();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + "TOKEN_PLN" + "PLN" + mIdPel + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseInquiryPLN> inqToken = myAPI.inquiryPLN(mTimestamp, "TOKEN_PLN", "PLN", mIdPel, sessionManager.getId(), mSignature);
        inqToken.enqueue(new Callback<ResponseInquiryPLN>() {
            @Override
            public void onResponse(Call<ResponseInquiryPLN> call, Response<ResponseInquiryPLN> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    mNamaPelanggan = response.body().getResultInfo().getNama();
                    mMeter = response.body().getResultInfo().getMeter();
                    mIdPel = response.body().getResultInfo().getIdpel();
                    mDaya = response.body().getResultInfo().getDaya();
                    mInquiryId = response.body().getResultInfo().getInquiryid();
                    mSystrace = response.body().getResultInfo().getSystrace();
                    mBillid = response.body().getResultInfo().getBillid();

                    txtNamaPelanggan.setText(mNamaPelanggan);
                    txtNomerMeter.setText(mMeter);
                    txtDaya.setText(mDaya);
                    txtNominal.setText(String.format("Rp. %,d", Integer.parseInt(mNominal)).replace(',', '.'));
                    txtAdminCA.setText(String.format("Rp. %,d", mAdminCA).replace(',', '.'));
                    txtTotal.setText(String.format("Rp. %,d", Integer.parseInt(mNominal + mAdminCA)).replace(',', '.'));
                    purchaseDetail.setVisibility(View.VISIBLE);
                    denomCard.setVisibility(View.VISIBLE);
                    btnBeli.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(PLNTokenActivity.this, "Inquiry gagal.\n" + result_desc, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseInquiryPLN> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage()== null) {
                    Toast.makeText(getApplicationContext(), "Cek data pelanggan, gagal. TIMEOUT.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Inquiry Failed, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @OnClick(R.id.btn_beli)
    public void onBtnBeliClicked() {
        Intent intent = new Intent(PLNTokenActivity.this, EnterPinActivity.class);
        intent.putExtra("msisdn",mMeter);
        intent.putExtra("kodeproduk", mKodeProduk);
        intent.putExtra("harga", String.valueOf(mTotal));
        intent.putExtra("uid", sessionManager.getId());
        intent.putExtra("tipe","TOKEN_PLN");
        intent.putExtra("provider","PLN");
        intent.putExtra("amount", mNominal);

        startActivity(intent);
    }

    private void loadListDenom(String mOperator) {
        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET);

        final Call<ResponseDenomPulsa> dataDenom = myAPI.getDenomPulsa( mTimestamp, mOperator ,"TOKEN_PLN", mSignature);

        dataDenom.enqueue(new Callback<ResponseDenomPulsa>() {
            @Override
            public void onResponse(Call<ResponseDenomPulsa> call, Response<ResponseDenomPulsa> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                Log.d("PLNTokenActivity", result_desc);
                if(result_code.equals("0000")) {
                    List<DenomPulsaItem> items = response.body().getDenomPulsa();
                    resultDenomPulsa.addAll(items);
                }
            }

            @Override
            public void onFailure(Call<ResponseDenomPulsa> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(PLNTokenActivity.this, "loadListDenom failed, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API loadDenom Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API loadDenom Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    private void changeDenom(String denom) {
        switch (denom) {
            case "20":
                clearDenom();
                txtLabel20K.setBackgroundResource(R.drawable.button_bg2);
                txtLabel20K.setTextColor(Color.WHITE);
                mNominal = "20000";
                break;
            case "50":
                clearDenom();
                txtLabel50K.setBackgroundResource(R.drawable.button_bg2);
                txtLabel50K.setTextColor(Color.WHITE);
                mNominal = "50000";
                break;
            case "100":
                clearDenom();
                txtLabel100K.setBackgroundResource(R.drawable.button_bg2);
                txtLabel100K.setTextColor(Color.WHITE);
                mNominal = "100000";
                break;
            case "200":
                clearDenom();
                txtLabel200K.setBackgroundResource(R.drawable.button_bg2);
                txtLabel200K.setTextColor(Color.WHITE);
                mNominal = "200000";
                break;
            case "500":
                clearDenom();
                txtLabel500K.setBackgroundResource(R.drawable.button_bg2);
                txtLabel500K.setTextColor(Color.WHITE);
                mNominal = "500000";
                break;
            case "1000":
                clearDenom();
                txtLabel1000K.setBackgroundResource(R.drawable.button_bg2);
                txtLabel1000K.setTextColor(Color.WHITE);
                mNominal = "1000000";
                break;
        }
        setKodeProduk(mNominal);
        mTotal = Integer.parseInt(mNominal) + mAdminCA;
        txtNominal.setText(String.format("Rp. %,d", Integer.parseInt(mNominal)).replace(',', '.'));
        txtAdminCA.setText(String.format("Rp. %,d", mAdminCA).replace(',', '.'));
        txtTotal.setText(String.format("Rp. %,d", mTotal).replace(',', '.'));
        toogleBtnBeli();

        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollview);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
        //txtLabelTotal.requestFocus();
    }


    private void setKodeProduk(String nominal) {
        DenomPulsaItem denomTokenItem ;
        for (int i =0 ; i<resultDenomPulsa.size(); i++) {
            denomTokenItem = resultDenomPulsa.get(i);
             if (denomTokenItem.getNominal().equals(nominal)) {
                 mKodeProduk = denomTokenItem.getKodeproduk();
                 mHarga      = denomTokenItem.getHarga();
                 mAdminCA    = Integer.parseInt(mHarga )- Integer.parseInt(nominal);
             }
        }
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    sessionManager.setPoin(result_poin);
                    //txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else if (result_code.equals("0103")) {
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API cs Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean isEnoughBalance() {
        return (Integer.parseInt(sessionManager.getSaldo()) >= (mAdminCA + Integer.parseInt(mNominal)));
    }

    private void toogleBtnBeli() {
        if (!isEnoughBalance()) {
            btnBeli.setEnabled(false);
            btnBeli.setBackgroundResource(R.color.colorGrid);
            Snackbar.make(mLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                    .setAction("Top Up", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(PLNTokenActivity.this, DepositRequestActivity.class));
                        }
                    }).show();
        } else {
            btnBeli.setEnabled(true);
            btnBeli.setBackgroundResource(R.drawable.button_bg);
        }
    }

    private void clearDenom() {
        txtLabel20K.setBackgroundResource(R.drawable.button_bgwhite);
        txtLabel20K.setTextColor(Color.BLACK);
        txtLabel50K.setBackgroundResource(R.drawable.button_bgwhite);
        txtLabel50K.setTextColor(Color.BLACK);
        txtLabel100K.setBackgroundResource(R.drawable.button_bgwhite);
        txtLabel100K.setTextColor(Color.BLACK);
        txtLabel200K.setBackgroundResource(R.drawable.button_bgwhite);
        txtLabel200K.setTextColor(Color.BLACK);
        txtLabel500K.setBackgroundResource(R.drawable.button_bgwhite);
        txtLabel500K.setTextColor(Color.BLACK);
        txtLabel1000K.setBackgroundResource(R.drawable.button_bgwhite);
        txtLabel1000K.setTextColor(Color.BLACK);
        mNominal = "0";
    }


}
