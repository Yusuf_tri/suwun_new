package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponseDenomPulsa{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	@SerializedName("denom_pulsa")
	private List<DenomPulsaItem> denomPulsa;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	public void setDenomPulsa(List<DenomPulsaItem> denomPulsa){
		this.denomPulsa = denomPulsa;
	}

	public List<DenomPulsaItem> getDenomPulsa(){
		return denomPulsa;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDenomPulsa{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			",denom_pulsa = '" + denomPulsa + '\'' + 
			"}";
		}
}