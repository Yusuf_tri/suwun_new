package id.co.popay.suwunapps.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.adapters.ListDepositAdapter;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseDeposit;
import id.co.popay.suwunapps.model.ResultDepositItem;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class ListDepositActivity extends SessionManager {
    List<ResultDepositItem> resultDeposit;
    ListDepositAdapter mAdapter;
    RecyclerView recyclerView;

    String mUserId;
    private String mTimestamp, mSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_deposit);

        recyclerView = (RecyclerView) findViewById(R.id.list_deposit_container);

        APIServices myAPI = InitNetLibrary.getInstances();
        showProgressDialog("");
        mUserId = sessionManager.getUID();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mUserId);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        resultDeposit = new ArrayList<>();
        mAdapter= new ListDepositAdapter(ListDepositActivity.this, resultDeposit);
        recyclerView.setAdapter(mAdapter);


        final Call<ResponseDeposit> dataDeposit = myAPI.getDepList(mTimestamp, mUserId, mSignature);

        dataDeposit.enqueue(new Callback<ResponseDeposit>() {
            @Override
            public void onResponse(Call<ResponseDeposit> call, Response<ResponseDeposit> response) {
                hideProgressDialog();

                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    List<ResultDepositItem> items = response.body().getResultDeposit();
                    resultDeposit.addAll(items);

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseDeposit> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(ListDepositActivity.this, "call gd Failed, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API getdeposit Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
