package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInfo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class RedeemVoucherActivity extends SessionManager {

    @BindView(R.id.ivGambarRedeem)
    ImageView ivGambarRedeem;
    @BindView(R.id.tvJudulRedeem)
    TextView tvJudulRedeem;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;
    @BindView(R.id.btnGoHome)
    Button btnGoHome;

    private String mItemGambar;
    private String mItemId, mItemTipe, mItemJudul, mItemDeskripsi, mItemPoin, mItemNominal, mMSISDN;
    private String mTimestamp, mSignature;
    
    TextView txtSaldo;
    TextView txtKodeVoucher;
    ImageView imgStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_voucher);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mItemGambar = intent.getStringExtra("itemGambar");
        mItemId = intent.getStringExtra("itemId");
        mItemTipe = intent.getStringExtra("itemTipe");
        mItemJudul = intent.getStringExtra("itemJudul");
        mItemDeskripsi = intent.getStringExtra("itemDeskripsi");
        mItemPoin = intent.getStringExtra("itemPoin");
        mItemNominal = intent.getStringExtra("itemNominal");
        mMSISDN = sessionManager.getMsisdn();

        Picasso.with(this).load(mItemGambar).error(R.drawable.noimage).into(ivGambarRedeem);
        tvJudulRedeem.setText(mItemJudul + " " + "(" + mItemPoin + " poin)");

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getUID() +
                mItemTipe + mItemPoin + mItemNominal + mMSISDN);

        doRedeemVoucher();
    }

    private void doRedeemVoucher() {
        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstances();
        Call<ResponseInfo> redeemV = myAPI.redeemVoucher(mTimestamp,mItemTipe, mItemPoin, mItemId, mItemJudul,
                mItemNominal,sessionManager.getUID(),mMSISDN, mSignature);
        Log.d("RedeemVoucher", mItemId + "-" + mItemJudul + "-" + mItemNominal + "-" +
                sessionManager.getUID()+"-"+mMSISDN+"-"+mSignature);
        redeemV.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_desc= response.body().getResultDesc();
                String result_code= response.body().getResultCode();
                String result_info= response.body().getInfo();
                txtKodeVoucher = (TextView) findViewById(R.id.tvKodeVoucher);

                if (result_code.equals("0000")) {
                    txtKodeVoucher.setText(result_info);
                    sessionManager.setPoin(response.body().getBalance());
                } else {
                    //GAGAL
                    txtKodeVoucher.setTextSize(15);
                    txtKodeVoucher.setTextColor(Color.RED);
                    txtKodeVoucher.setText(result_desc + ", " + result_info);
                }
            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
                String pesan = t.getMessage();
//                Toast.makeText(RedeemGameActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                Log.d("RedeemVoucher", pesan, t);
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API redeemV Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API redeemV Failed, TIMEOUT. Cek daftar riwayat redeem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @OnClick(R.id.btnGoHome)
    public void onViewClicked() {
        startActivity(new Intent(this, SuwunPoinActivity.class));
    }
}
