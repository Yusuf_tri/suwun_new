package id.co.popay.suwunapps.network;

import id.co.popay.suwunapps.model.ResponseCityPdam;
import id.co.popay.suwunapps.model.ResponseDenomPulsa;
import id.co.popay.suwunapps.model.ResponseDeposit;
import id.co.popay.suwunapps.model.ResponseGameProvider;
import id.co.popay.suwunapps.model.ResponseGameTopup;
import id.co.popay.suwunapps.model.ResponseGameUser;
import id.co.popay.suwunapps.model.ResponseInfo;
import id.co.popay.suwunapps.model.ResponseInquiryBPJS;
import id.co.popay.suwunapps.model.ResponseInquiryBillPLN;
import id.co.popay.suwunapps.model.ResponseInquiryPDAM;
import id.co.popay.suwunapps.model.ResponseInquiryPLN;
import id.co.popay.suwunapps.model.ResponseInquiryTelkomPSTN;
import id.co.popay.suwunapps.model.ResponseKonfigurasi;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.model.ResponseRedeem;
import id.co.popay.suwunapps.model.ResponseRedeemHistory;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.model.ResponseTransaksi;
import id.co.popay.suwunapps.model.ResponseUser;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ara on 01/02/18.
 */

public interface APIServices {

    @FormUrlEncoded
    @POST("logout")
    Call<ResponseO> logout (
            @Header("timestamp") String strTimestamp,
            @Field("id") String strId,
            @Field("signature") String strSignature
            );

    @FormUrlEncoded
    @POST("reg")
    Call<ResponseO> registerUser(
            @Header("timestamp") String strTimestamp,
            @Field("username") String strUsername,
            @Field("email") String strEmail,
            @Field("msisdn") String strMsisdn,
            @Field("password") String strPassword,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("auth")
    Call<ResponseUser> loginUser(
            @Header("timestamp") String strTimestamp,
            @Field("id") String strEmailOrMSISDN,
            @Field("password") String strPassword,
            @Field("signature") String strSignature
    );

    @GET("getsaldo")
    Call<ResponseSaldo> getSaldo(
            @Header("timestamp") String strTimestamp,
            @Query("uid") String strUID,
            @Query("signature") String strSignature
    );

    @GET("trxlist")
    Call<ResponseTransaksi> getTrxList(
            @Header("timestamp") String strTimestamp,
            @Query("uid") String strUID,
            @Query("signature") String strSignature
    );

    @GET("denompulsa")
    Call<ResponseDenomPulsa> getDenomPulsa(
            @Header("timestamp") String strTimestamp,
            @Query("voperator") String strOperator,
            @Query("vtipe") String strTipe,
            @Query("signature") String strSignature

    );

    @FormUrlEncoded
    @POST("reqd")
    Call<ResponseO> requestDeposit(
            @Header("timestamp") String strTimestamp,
            @Field("uid") String strUID,
            @Field("bank") String strBank,
            @Field("jumlah") String strJumlah,
            @Field("signature") String strSignature

    );

    @FormUrlEncoded
    @POST("trxpurchase")
    Call<ResponseInfo> purchaseItem(
            @Header("timestamp") String strTimestamp,
            @Field("uid") String strUID,
            @Field("pin") String strPIN,
            @Field("tipe") String strTipe ,
            @Field("provider") String strOperator,
            @Field("amount") String strNominal,
            @Field("dest") String strMSISDN,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("trxpurchase1")
    Call<ResponseInfo> purchaseItem1(
            @Header("timestamp") String strTimestamp,
            @Field("kodeproduk") String strKodeProduk,
            @Field("uid") String strUID,
            @Field("pin") String strPIN,
            @Field("tipe") String strTipe ,
            @Field("provider") String strOperator,
            @Field("amount") String strNominal,
            @Field("dest") String strMSISDN,
            @Field("signature") String strSignature
    );

    @GET("gameprovider")
    Call<ResponseGameProvider> getGameProvider(
            @Header("timestamp") String strTimestamp,
            @Query("signature") String strSignature
    );

    @GET("gametopupprovider")
    Call<ResponseGameTopup> getGameTopupProvider(
            @Header("timestamp") String strTimestamp,
            @Query("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("gametopupcekuser")
    Call<ResponseGameUser> getGameUser(
            @Header("timestamp") String strTimestamp,
            @Field("accid") String strAccId,
            @Field("provider") String strProvider,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("gametopuptransfer")
    Call<ResponseInfo> doGameTopupTransfer(
            @Header("timestamp") String strTimestamp,
            @Field("kodeproduk") String strKodeProduk,
            @Field("uid") String strUID,
            @Field("pin") String strPIN,
            @Field("tipe") String strTipe ,
            @Field("provider") String strOperator,
            @Field("amount") String strNominal,
            @Field("dest") String strMSISDN,
            @Field("signature") String strSignature
    );


    @FormUrlEncoded
    @POST("inqtokenpln")
    Call<ResponseInquiryPLN> inquiryPLN(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe,
            @Field("provider") String strProvider,
            @Field("idpel") String strIdPelanggan,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("inqbpjskes")
    Call<ResponseInquiryBPJS> inquiryBPJS(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe,
            @Field("provider") String strProvider,
            @Field("idpel") String strIdPelanggan,
            @Field("uid") String strUID,
            @Field("no_hp") String strNoHp,
            @Field("periode") String strPeriode,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("paybpjskes")
    Call<ResponseInfo> paymentBillBPJSKes (
            @Header("timestamp") String strTimestamp,
            @Field("idpel") String strIdpel,
            @Field("amount") String strAmount,
            @Field("inquiryid") String strInquiryid,
            @Field("systrace") String strSystrace,
            @Field("billid") String strBillid,
            @Field("debitsaldo") int intDebitsaldo,
            @Field("uid") String strUID,
            @Field("pin") String strPin,
            @Field("signature") String strSignature,
            @Field("provider") String strProvider,
            @Field("kodeproduk") String strKodeproduk,
            @Field("no_hp") String strNohp,
            @Field("periode") String strPeriode
    );

    @FormUrlEncoded
    @POST("inqpln")
    Call<ResponseInquiryBillPLN> inquiryBillPLN(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe,
            @Field("provider") String strProvider,
            @Field("idpel") String strIdPelanggan,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("paypln")
    Call<ResponseInfo> paymentBillPLN (
            @Header("timestamp") String strTimestamp,
            @Field("idpel") String strIdpel,
            @Field("amount") String strAmount,
            @Field("inquiryid") String strInquiryid,
            @Field("systrace") String strSystrace,
            @Field("billid") String strBillid,
            @Field("debitsaldo") int intDebitsaldo,
            @Field("uid") String strUID,
            @Field("pin") String strPin,
            @Field("signature") String strSignature,
            @Field("provider") String strProvider,
            @Field("kodeproduk") String strKodeproduk
    );

    @FormUrlEncoded
    @POST("inqpstn")
    Call<ResponseInquiryTelkomPSTN> inquiryBillPSTN(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe,
            @Field("provider") String strProvider,
            @Field("idpel") String strIdPelanggan,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("paypstn")
    Call<ResponseInfo> paymentBillPstn (
            @Header("timestamp") String strTimestamp,
            @Field("idpel") String strIdpel,
            @Field("amount") String strAmount,
            @Field("inquiryid") String strInquiryid,
            @Field("systrace") String strSystrace,
            @Field("billid") String strBillid,
            @Field("debitsaldo") int intDebitsaldo,
            @Field("uid") String strUID,
            @Field("pin") String strPin,
            @Field("signature") String strSignature,
            @Field("provider") String strProvider,
            @Field("kodeproduk") String strKodeproduk
    );


    @GET("deplist")
    Call<ResponseDeposit> getDepList(
            @Header("timestamp") String strTimestamp,
            @Query("uid") String strUID,
            @Query("signature") String strSignature
    );


    @GET("konfigurasi")
    Call<ResponseKonfigurasi> getKonfig (
            @Header("timestamp") String timestamp,
            @Query("signature") String signature
    );

    @FormUrlEncoded
    @POST("sendkodereset")
    Call<ResponseO> sendCode (
         @Header("timestamp") String timestamp,
         @Field("email") String strEmail,
         @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("updatepassword")
    Call<ResponseO> updatePassword (
            @Header("timestamp") String timestamp,
            @Field("kodereset") String strKodereset,
            @Field("password") String strPassword,
            @Field("email") String strEmail,
            @Field("signature") String strSignature

    ) ;

    @FormUrlEncoded
    @POST("updatepin")
    Call<ResponseO> updatePin (
            @Header("timestamp") String timestamp,
            @Field("kodereset") String strKodereset,
            @Field("pin") String strPin,
            @Field("email") String strEmail,
            @Field("uid") String strUID,
            @Field("signature") String strSignature

    ) ;

    @FormUrlEncoded
    @POST("gantipassword")
    Call<ResponseO> gantiPassword (
            @Header("timestamp") String timestamp,
            @Field("oldpasswd") String strOldPassword,
            @Field("newpasswd") String strNewPassword,
            @Field("confpasswd") String strConfPassword,
            @Field("uid") String strUID,
            @Field("signature") String strSignature

    ) ;

    @FormUrlEncoded
    @POST("gantipin")
    Call<ResponseO> gantiPin (
            @Header("timestamp") String timestamp,
            @Field("oldpin") String strOldPin,
            @Field("newpin") String strNewPin,
            @Field("confpin") String strConfPin,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    ) ;

    @GET("getredeemitem")
    Call<ResponseRedeem> getredeemitem (
        @Header("timestamp") String timestamp,
        @Query("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("redeempulsa")
    Call<ResponseInfo> redeemPulsa(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe ,
            @Field("poin") String strPoin,
            @Field("itemid") String strItemid,
            @Field("itemjudul") String strItemJudul,
            @Field("amount") String strNominal,
            @Field("dest") String strMSISDN,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("redeemgame")
    Call<ResponseInfo> redeemGame(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe ,
            @Field("poin") String strPoin,
            @Field("itemid") String strItemid,
            @Field("itemjudul") String strItemJudul,
            @Field("kodeproduk") String strKodeproduk,
            @Field("dest") String strMSISDN,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("redeemgopay")
    Call<ResponseInfo> redeemGopay(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe ,
            @Field("poin") String strPoin,
            @Field("itemid") String strItemid,
            @Field("itemjudul") String strItemJudul,
            @Field("kodeproduk") String strKodeproduk,
            @Field("dest") String strMSISDN,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("redeemtoken")
    Call<ResponseInfo> redeemToken(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe ,
            @Field("poin") String strPoin,
            @Field("itemid") String strItemid,
            @Field("itemjudul") String strItemJudul,
            @Field("kodeproduk") String strKodeproduk,
            @Field("dest") String strMSISDN,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );


    @FormUrlEncoded
    @POST("redeembarang")
    Call<ResponseInfo> redeemBarang(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe ,
            @Field("poin") String strPoin,
            @Field("itemid") String strItemid,
            @Field("itemjudul") String strItemJudul,
            @Field("dest") String strPenerima,
            @Field("alamat") String strAlamat,
            @Field("remark") String strCatatan,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("redeemvoucher")
    Call<ResponseInfo> redeemVoucher(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe ,
            @Field("poin") String strPoin,
            @Field("itemid") String strItemid,
            @Field("itemjudul") String strItemJudul,
            @Field("amount") String strNominal,
            @Field("uid") String strUID,
            @Field("dest") String strMSISDN,
            @Field("signature") String strSignature
    );

    @GET("getredeemhistory")
    Call<ResponseRedeemHistory> getRedeemHistory(
            @Header("timestamp") String strTimestamp,
            @Query("uid") String strUID,
            @Query("signature") String strSignature
    );

    @GET("getcitypdam")
    Call<ResponseCityPdam> getCityPdam(
            @Header("timestamp") String strTimestamp,
            @Query("uid") String strUID,
            @Query("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("inqpdam")
    Call<ResponseInquiryPDAM> inquiryBillPDAM(
            @Header("timestamp") String strTimestamp,
            @Field("tipe") String strTipe,
            @Field("provider") String strProvider,
            @Field("idpel") String strIdPelanggan,
            @Field("citycode") String strCityCode,
            @Field("uid") String strUID,
            @Field("signature") String strSignature
    );

    @FormUrlEncoded
    @POST("paypdam")
    Call<ResponseInfo> paymentBillPDAM (
            @Header("timestamp") String strTimestamp,
            @Field("idpel") String strIdpel,
            @Field("amount") String strAmount,
            @Field("systrace") String strSystrace,
            @Field("debitsaldo") int intDebitsaldo,
            @Field("uid") String strUID,
            @Field("pin") String strPin,
            @Field("signature") String strSignature,
            @Field("provider") String strProvider,
            @Field("kodeproduk") String strKodeproduk
    );


    @GET("zdummy")
    Call<ResponseInfo> zdummy (
         @Header("timestamp") String timestamp,
         @Query("signature") String signature
    );
}
