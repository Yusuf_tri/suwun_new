package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponseDeposit{

	@SerializedName("result_deposit")
	private List<ResultDepositItem> resultDeposit;

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDeposit(List<ResultDepositItem> resultDeposit){
		this.resultDeposit = resultDeposit;
	}

	public List<ResultDepositItem> getResultDeposit(){
		return resultDeposit;
	}

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDeposit{" + 
			"result_deposit = '" + resultDeposit + '\'' + 
			",result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}