package id.co.popay.suwunapps.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.co.popay.suwunapps.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment {

    private String mNameProfile;
    private String mSaldo;
    private String mMsisdn;
    private String mEmail;
    private String mDateJoin;
    private String mPoin;


    public UserProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        TextView txtNameProfile = (TextView) view.findViewById(R.id.txtNameProfile);
        TextView txtSaldo = (TextView) view.findViewById(R.id.txtSaldo);
        TextView txtPoin = (TextView) view.findViewById(R.id.txtPoin);
        TextView txtMsisdn = (TextView) view.findViewById(R.id.txtMsisdn);
        TextView txtEmail = (TextView) view.findViewById(R.id.txtEmail);
        TextView txtDateJoin = (TextView) view.findViewById(R.id.txtDateJoin);


//        argument.putString("saldo",sessionManager.getSaldo());
//        argument.putString("userId", sessionManager.getUID());
//        argument.putString("dateJoin", sessionManager.getDateJoin());
//        argument.putString("msisdn", sessionManager.getMsisdn());

        mNameProfile = getArguments().getString("username");
        mSaldo = getArguments().getString("saldo");
        mMsisdn = getArguments().getString("msisdn");
        mEmail = getArguments().getString("email");
        mDateJoin = getArguments().getString("dateJoin");
        mPoin = getArguments().getString("poin");

        txtNameProfile.setText(mNameProfile);
        txtSaldo.setText(String.format("Rp %,d", Integer.parseInt(mSaldo)).replace(',', '.'));
        txtPoin.setText(String.format("%,d poin", Integer.parseInt(mPoin)).replace(',', '.'));
        txtMsisdn.setText(mMsisdn);
        txtEmail.setText(mEmail);
        txtDateJoin.setText(mDateJoin);

        return view;
    }

}
