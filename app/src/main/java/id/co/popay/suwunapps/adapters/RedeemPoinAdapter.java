package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.RedeemItemItem;

/**
 * Created by ara on 30/03/18.
 */

public class RedeemPoinAdapter extends RecyclerView.Adapter<RedeemPoinAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<RedeemItemItem> mRedeemItemList;

    private OnItemClickListener mListener;

    public void setListener (OnItemClickListener listener) {
        mListener=listener;
    }

    public RedeemPoinAdapter(Context context, List<RedeemItemItem> itemList) {
        mContext = context;
        mRedeemItemList = itemList;
        mLayoutInflater = LayoutInflater.from(context);
    }


    @Override
    public RedeemPoinAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_redeem_poin, parent,false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RedeemPoinAdapter.MyViewHolder holder, int position) {
        final RedeemItemItem itemRedeem = mRedeemItemList.get(position);
        holder.tvNamaItem.setText(itemRedeem.getJudulItem());
        holder.tvJumlahPoin.setText(String.format("%,d",Integer.parseInt(String.valueOf(itemRedeem.getPoinRedeem()))).replace(',','.') + " poin");
        Picasso.with(mContext)
                .load(itemRedeem.getGambar().toString())
                .fit()
                .error(R.drawable.noimage)
                .into(holder.ivItemRedeem);

        if (mListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.OnItemClickListener(itemRedeem.getId(),
                            itemRedeem.getTipe(), itemRedeem.getJudulItem(), itemRedeem.getDeskripsiItem(),
                            itemRedeem.getPoinRedeem(), itemRedeem.getGambar(), itemRedeem.getNominal(),
                            itemRedeem.getKodeproduk());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mRedeemItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNamaItem, tvJumlahPoin;
        ImageView ivItemRedeem;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvNamaItem = (TextView) itemView.findViewById(R.id.tvNamaItem);
            tvJumlahPoin = (TextView) itemView.findViewById(R.id.tvJumlahPoin);
            cardView = (CardView) itemView.findViewById(R.id.container);
            ivItemRedeem = (ImageView) itemView.findViewById(R.id.ivItemRedeem);
        }
    }

    public interface OnItemClickListener {
        void OnItemClickListener(String itemId, String itemTipe, String itemJudul, String itemDeskripsi, String itemPoin, String itemGambar, String itemNominal, String itemKodeproduk);
    }
}