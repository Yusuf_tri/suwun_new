package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;


public class ResultInfoPdam{

	@SerializedName("kodeproduk")
	private String kodeproduk;

	@SerializedName("adminsuwun")
	private String adminsuwun;

	@SerializedName("totalbayar")
	private int totalbayar;

	@SerializedName("tagihan")
	private int tagihan;

	@SerializedName("nama")
	private String nama;

	@SerializedName("provider")
	private String provider;

	@SerializedName("systrace")
	private int systrace;

	@SerializedName("idpel")
	private String idpel;

	@SerializedName("denda")
	private int denda;

	@SerializedName("periode")
	private String periode;

	@SerializedName("cubic")
	private String cubic;

	@SerializedName("area")
	private String area;

	public void setArea(String area) {this.area= area; }

	public String getArea() {return area;}

	public void setCubic(String cubic) {this.cubic = cubic; }

	public String getCubic() {return cubic;}

	public void setKodeproduk(String kodeproduk){
		this.kodeproduk = kodeproduk;
	}

	public String getKodeproduk(){
		return kodeproduk;
	}

	public void setAdminsuwun(String adminsuwun){
		this.adminsuwun = adminsuwun;
	}

	public String getAdminsuwun(){
		return adminsuwun;
	}

	public void setTotalbayar(int totalbayar){
		this.totalbayar = totalbayar;
	}

	public int getTotalbayar(){
		return totalbayar;
	}

	public void setTagihan(int tagihan){
		this.tagihan = tagihan;
	}

	public int getTagihan(){
		return tagihan;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setProvider(String provider){
		this.provider = provider;
	}

	public String getProvider(){
		return provider;
	}

	public void setSystrace(int systrace){
		this.systrace = systrace;
	}

	public int getSystrace(){
		return systrace;
	}

	public void setIdpel(String idpel){
		this.idpel = idpel;
	}

	public String getIdpel(){
		return idpel;
	}

	public void setDenda(int denda){
		this.denda = denda;
	}

	public int getDenda(){
		return denda;
	}

	public void setPeriode(String periode){
		this.periode = periode;
	}

	public String getPeriode(){
		return periode;
	}

	@Override
 	public String toString(){
		return 
			"ResultInfoPdam{" + 
			"kodeproduk = '" + kodeproduk + '\'' + 
			",adminsuwun = '" + adminsuwun + '\'' + 
			",totalbayar = '" + totalbayar + '\'' + 
			",tagihan = '" + tagihan + '\'' + 
			",nama = '" + nama + '\'' + 
			",provider = '" + provider + '\'' + 
			",systrace = '" + systrace + '\'' + 
			",idpel = '" + idpel + '\'' + 
			",denda = '" + denda + '\'' +
			",cubic = '" + cubic + '\'' +
			",periode = '" + periode + '\'' +
					",area = '" + area + '\'' +
			"}";
		}
}