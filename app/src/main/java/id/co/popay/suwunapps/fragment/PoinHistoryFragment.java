package id.co.popay.suwunapps.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.adapters.RedeemHistoryAdapter;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseRedeemHistory;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.model.ResultRedeemtrxItem;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import id.co.popay.suwunapps.utils.ConnectivityUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

/**
 * A simple {@link Fragment} subclass.
 */
public class PoinHistoryFragment extends Fragment {

    RecyclerView recyclerView;
    List<ResultRedeemtrxItem> resultTrx;
    RedeemHistoryAdapter mAdapter;
    TextView txtJudulDeposit, txtTidakAdaData;

    String mUserId;
    ProgressDialog mProgressDialog;
    private String mTimestamp, mSignature;
    private String mSaldo, mPoin;
    private ImageView btn_refresh;

    public PoinHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_poin_history, container, false);
        txtJudulDeposit = (TextView) view.findViewById(R.id.txtJudulDeposit);
        txtTidakAdaData = (TextView) view.findViewById(R.id.txtTidakAdaData);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        resultTrx = new ArrayList<>();
        mAdapter = new RedeemHistoryAdapter(getActivity(), resultTrx);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
        //mAdapter.setListener();

        if (ConnectivityUtil.isConnected(getContext())) {
            loadListRedeemHistory();
        } else {
            Toast.makeText(getContext(), "Tidak ada koneksi internet", Toast.LENGTH_SHORT).show();

        }


        btn_refresh = (ImageView) view.findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityUtil.isConnected(getContext())) {
                    getSaldoFromServer();
                    loadListRedeemHistory();
                } else {
                    Toast.makeText(getContext(), "Tidak ada koneksi internet", Toast.LENGTH_SHORT).show();

                }
            }
        });


        return view;
    }

    private void loadListRedeemHistory() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            //mProgressDialog.setTitle("");
            mProgressDialog.setMessage("Loading transaction data..");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
        APIServices myAPI = InitNetLibrary.getInstances();
        mUserId = getArguments().getString("userId");
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mUserId);

        final Call<ResponseRedeemHistory> dataTransaksi = myAPI.getRedeemHistory(mTimestamp, mUserId, mSignature);
        dataTransaksi.enqueue(new Callback<ResponseRedeemHistory>() {
            @Override
            public void onResponse(Call<ResponseRedeemHistory> call, Response<ResponseRedeemHistory> response) {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    List<ResultRedeemtrxItem> itemTrx = response.body().getResultRedeemtrx();
                    if (itemTrx.size() == 0 ) {
                        txtTidakAdaData.setVisibility(View.VISIBLE);
                    } else {
                        txtTidakAdaData.setVisibility(View.GONE);
                    }

                    if (resultTrx.size() != 0) {
                        resultTrx.clear();
                    }
                    resultTrx.addAll(itemTrx);
                    mAdapter.notifyDataSetChanged();

                } else {
                    txtJudulDeposit.setText(result_desc);
                }
            }

            @Override
            public void onFailure(Call<ResponseRedeemHistory> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mUserId);

        APIServices myAPI = InitNetLibrary.getInstances();
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, mUserId, mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    mSaldo = result_desc;
                    mPoin = result_poin;
                    TextView tvSaldo = getActivity().findViewById(R.id.txtSaldo);
                    tvSaldo.setText(String.format("Rp. %,d", Integer.parseInt(mSaldo)).replace(',', '.'));
                    TextView tvPoin = getActivity().findViewById(R.id.txtPoin);
                    tvPoin.setText(String.format("%,d", Integer.parseInt(mPoin)).replace(',', '.'));
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                Toast.makeText(getActivity(), "API cs Fail, " + t.getMessage().replace(BASE_IP,"API_HOST") , Toast.LENGTH_SHORT).show();
            }
        });

    }

}
