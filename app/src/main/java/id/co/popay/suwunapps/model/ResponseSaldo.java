package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ara on 01/02/18.
 */

public class ResponseSaldo {
    @SerializedName("result_desc")
    private String resultDesc;

    @SerializedName("result_code")
    private String resultCode;

    @SerializedName("result_poin")
    private String resultPoin;

    public void setResultDesc(String resultDesc){
        this.resultDesc = resultDesc;
    }

    public String getResultDesc(){
        return resultDesc;
    }

    public void setResultCode(String resultCode){
        this.resultCode = resultCode;
    }

    public String getResultCode(){
        return resultCode;
    }

    public void setResultPoin(String resultPoin){
        this.resultPoin = resultPoin;
    }

    public String getResultPoin(){
        return resultPoin;
    }

    @Override
    public String toString(){
        return
                "ResponseO{" +
                        "result_desc = '" + resultDesc + '\'' +
                        ",result_code = '" + resultCode + '\'' +
                        ",result_poin = '" + resultPoin + '\'' +
                        "}";
    }

}
