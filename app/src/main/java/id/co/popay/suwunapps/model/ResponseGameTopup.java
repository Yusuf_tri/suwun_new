package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGameTopup{

	@SerializedName("game_provider")
	private List<GameProviderItem> gameProvider;

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	public void setGameProvider(List<GameProviderItem> gameProvider){
		this.gameProvider = gameProvider;
	}

	public List<GameProviderItem> getGameProvider(){
		return gameProvider;
	}

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseGameTopup{" + 
			"game_provider = '" + gameProvider + '\'' + 
			",result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}