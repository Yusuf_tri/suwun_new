package id.co.popay.suwunapps.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class GantiPinActivity extends SessionManager {

    @BindView(R.id.edtOldPin)
    EditText edtOldPin;
    @BindView(R.id.edtNewPin)
    EditText edtNewPin;
    @BindView(R.id.edtConfirmPin)
    EditText edtConfirmPin;
    @BindView(R.id.gantiPasswordCardView)
    CardView gantiPasswordCardView;
    @BindView(R.id.btnUpdatePin)
    Button btnUpdatePin;

    private String mTimestamp, mSignature;
    private String mOldPin, mNewPin, mConfPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_pin);
        ButterKnife.bind(this);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnUpdatePin)
    public void onViewClicked() {
        if (edtOldPin.getText().toString().isEmpty()) {
            Toast.makeText(this, "PIN lama harus diisi.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (edtNewPin.getText().toString().isEmpty()) {
            Toast.makeText(this, "PIN baru harus diisi.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (edtConfirmPin.getText().toString().isEmpty()) {
            Toast.makeText(this, "Konfirmasi PIN harus diisi.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (! edtNewPin.getText().toString().equals(edtConfirmPin.getText().toString())) {
            Toast.makeText(this, "Konfirmasi PIN tidak sama.", Toast.LENGTH_SHORT).show();
            return;
        }

        gantiPIN();
    }

    private void gantiPIN() {
        showProgressDialog("");
        mOldPin = edtOldPin.getText().toString();
        mNewPin = edtNewPin.getText().toString();
        mConfPin= edtConfirmPin.getText().toString();

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mOldPin + mNewPin + sessionManager.getId());

        APIServices myApi = InitNetLibrary.getInstanceWc(this);
        Call<ResponseO> changePIN = myApi.gantiPin(mTimestamp, mOldPin, mNewPin, mConfPin, sessionManager.getId(), mSignature);
        changePIN.enqueue(new Callback<ResponseO>() {
            @Override
            public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    Toast.makeText(GantiPinActivity.this, "PIN baru telah diset.", Toast.LENGTH_SHORT).show();
                    //startActivity(new Intent(GantiPinActivity.this, MainTabActivity.class));
                    onBackPressed();
                } else {
                    Toast.makeText(GantiPinActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseO> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(GantiPinActivity.this, "API failure gp\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_LONG).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
