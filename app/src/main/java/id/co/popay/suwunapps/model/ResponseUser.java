package id.co.popay.suwunapps.model;


import com.google.gson.annotations.SerializedName;


public class ResponseUser{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("dataUser")
	private DataUser dataUser;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setDataUser(DataUser dataUser){
		this.dataUser = dataUser;
	}

	public DataUser getDataUser(){
		return dataUser;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseUser{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",dataUser = '" + dataUser + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}