package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.CitiesItem;

/**
 * Created by ara on 06/07/18.
 */

public class PdamCityAdapter extends RecyclerView.Adapter<PdamCityAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<CitiesItem> mDataCity;

    private OnItemClickListener mListener;

    public void setListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public PdamCityAdapter (Context c, List<CitiesItem> listCities) {
        mContext = c;
        mDataCity = listCities;
        mLayoutInflater = LayoutInflater.from(c);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.layout_list_citypadam, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PdamCityAdapter.MyViewHolder holder, int position) {
        final CitiesItem cityItem = mDataCity.get(position);
        holder.txtCityName.setText(cityItem.getKota());

        if (mListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.OnItemClickLister(cityItem.getProductCode(), cityItem.getKota());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDataCity.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtCityName;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCityName = (TextView)itemView.findViewById(R.id.txtCityName);
            cardView = (CardView)itemView.findViewById(R.id.cardview);
        }
    }

    public interface OnItemClickListener {
        void OnItemClickLister(String cityId, String cityName);
    }
}
