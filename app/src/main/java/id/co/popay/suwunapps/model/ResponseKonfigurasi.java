package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseKonfigurasi{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_infokonfig")
	private List<String> resultInfokonfig;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultInfokonfig(List<String> resultInfokonfig){
		this.resultInfokonfig = resultInfokonfig;
	}

	public List<String> getResultInfokonfig(){
		return resultInfokonfig;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseKonfigurasi{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_infokonfig = '" + resultInfokonfig + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}