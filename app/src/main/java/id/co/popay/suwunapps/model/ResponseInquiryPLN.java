package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResponseInquiryPLN{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_info")
	private ResultInfo resultInfo;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultInfo(ResultInfo resultInfo){
		this.resultInfo = resultInfo;
	}

	public ResultInfo getResultInfo(){
		return resultInfo;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseInquiryPLN{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_info = '" + resultInfo + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}