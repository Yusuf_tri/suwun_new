package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInquiryBPJS;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class BPJSKesehatanActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;
    @BindView(R.id.saldoBar)
    LinearLayout saldoBar;
    @BindView(R.id.edtNomerVA)
    EditText edtNomerVA;
    @BindView(R.id.edtJumlahBulan)
    TextView edtJumlahBulan;
    @BindView(R.id.btnCekPelanggan)
    Button btnCekPelanggan;
    @BindView(R.id.nomervaCardView)
    CardView nomervaCardView;
    @BindView(R.id.txtLabelNomerMeter)
    TextView txtLabelNomerMeter;
    @BindView(R.id.txtNomerVA)
    TextView txtNomerVA;
    @BindView(R.id.txtLabelNama)
    TextView txtLabelNama;
    @BindView(R.id.txtNamaPelanggan)
    TextView txtNamaPelanggan;
    @BindView(R.id.txtLabelPeserta)
    TextView txtLabelPeserta;
    @BindView(R.id.txtJumlahPeserta)
    TextView txtJumlahPeserta;
    @BindView(R.id.txtLabelNominal)
    TextView txtLabelNominal;
    @BindView(R.id.txtTagihan)
    TextView txtTagihan;
    @BindView(R.id.txtLabelAdminCA)
    TextView txtLabelAdminCA;
    @BindView(R.id.txtAdminCA)
    TextView txtAdminCA;
    @BindView(R.id.txtLabelTotal)
    TextView txtLabelTotal;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.txtPeriode)
    TextView txtPeriode;
    @BindView(R.id.txtSaldoTidakCukup)
    TextView txtSaldoTidakCukup;
    @BindView(R.id.myTopLinearLayout)
    LinearLayout myTopLinearLayout;
    @BindView(R.id.txtLabelPeriode)
    TextView txtLabelPeriode;

    @BindView(R.id.spnPeriode)
    Spinner spnPeriode;


    private String mTimestamp, mSignature;
    private String mNoVirtualAccount;
    private TextWatcher txtWatcherNoVA = null;

    private String mNamaPelanggan;
    private String mIdPel;
    private String mJumlahPeserta;
    private int mPeriode;
    private String mStrPeriode;
    private int mTagihan;
    private int mTotalBayar;
    private String mTotalamount;
    private String mProvider;
    private String mKodeProduk;
    private String mInquiryId;
    private String mSystrace;
    private String mBillid;
    private int mAdminCA = 0;
    private int mTotal;
    private AlertDialog.Builder alert;

    private String strPeriode[] = {"1 Bulan", "2 Bulan", "3 Bulan", "4 Bulan", "5 Bulan", "6 Bulan"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bpjskesehatan);
        ButterKnife.bind(this);

        purchaseDetail.setVisibility(View.INVISIBLE);
        btnBeli.setVisibility(View.INVISIBLE);

        txtWatcherNoVA = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 13) {
                    btnCekPelanggan.setEnabled(true);
                    btnCekPelanggan.setBackgroundResource(R.drawable.button_bg);
                } else {
                    btnCekPelanggan.setEnabled(false);
                    btnCekPelanggan.setBackgroundResource(R.color.colorGrid);
                    purchaseDetail.setVisibility(View.INVISIBLE);
                    btnBeli.setVisibility(View.INVISIBLE);
                    btnBeli.setEnabled(false);
                    btnBeli.setBackgroundResource(R.color.colorGrid);
                    txtSaldoTidakCukup.setVisibility(View.GONE);
                }
            }
        };
        edtNomerVA.addTextChangedListener(txtWatcherNoVA);
        getSaldoFromServer();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strPeriode);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnPeriode.setAdapter(adapter);



        //listener on spinner
        spnPeriode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mStrPeriode= spnPeriode.getSelectedItem().toString();
                mPeriode=position+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(BPJSKesehatanActivity.this, DepositRequestActivity.class));
    }

    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        //Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(BPJSKesehatanActivity.this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.btnCekPelanggan)
    public void onBtnCekPelangganClicked() {
        showProgressDialog("");
        btnBeli.setEnabled(false);
        purchaseDetail.setVisibility(View.INVISIBLE);
        btnBeli.setVisibility(View.INVISIBLE);
        btnBeli.setBackgroundResource(R.color.colorGrid);

        mIdPel = edtNomerVA.getText().toString();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET +
                "BILL" + "BPJS" +
                mIdPel + sessionManager.getId() + sessionManager.getMsisdn() + mPeriode);

        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseInquiryBPJS> inquiryBPJSCall = myAPI.inquiryBPJS(mTimestamp, "BILL", "BPJS", mIdPel,
                sessionManager.getId(), sessionManager.getMsisdn(), String.valueOf(mPeriode), mSignature);

        inquiryBPJSCall.enqueue(new Callback<ResponseInquiryBPJS>() {
            @Override
            public void onResponse(Call<ResponseInquiryBPJS> call, Response<ResponseInquiryBPJS> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    mNamaPelanggan = response.body().getResultInfoBpjs().getNama();
                    mNoVirtualAccount = response.body().getResultInfoBpjs().getNova();
                    mTagihan = response.body().getResultInfoBpjs().getTagihan();
                    mJumlahPeserta = response.body().getResultInfoBpjs().getJumpeserta();
                    mInquiryId = response.body().getResultInfoBpjs().getInquiryid();
                    mSystrace = response.body().getResultInfoBpjs().getSystrace();
                    mBillid = response.body().getResultInfoBpjs().getBillid();
                    mTotalamount = response.body().getResultInfoBpjs().getTotalamount();
                    mStrPeriode = response.body().getResultInfoBpjs().getPeriode();
                    mAdminCA = Integer.parseInt(response.body().getResultInfoBpjs().getAdminsuwun());
                    mTotalBayar = response.body().getResultInfoBpjs().getTotalbayar();

                    mProvider = response.body().getResultInfoBpjs().getProvider();
                    mKodeProduk = response.body().getResultInfoBpjs().getKodeproduk();


                    txtNomerVA.setText(mNoVirtualAccount);
                    txtNamaPelanggan.setText(mNamaPelanggan);
                    txtPeriode.setText(mStrPeriode);
                    txtTagihan.setText(String.format("Rp. %,d", mTagihan).replace(',', '.'));
                    txtJumlahPeserta.setText(mJumlahPeserta + " orang");
                    txtAdminCA.setText(String.format("Rp. %,d", mAdminCA).replace(',', '.'));
                    mTotal = (mTagihan + mAdminCA);
                    txtTotal.setText(String.format("Rp. %,d", mTotal).replace(',', '.'));
                    purchaseDetail.setVisibility(View.VISIBLE);
                    btnBeli.setVisibility(View.VISIBLE);

                    toogleBtnBeli();

                    final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollview);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });

                } else {
                    Toast.makeText(BPJSKesehatanActivity.this, "Inquiry gagal.\n" + result_desc, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseInquiryBPJS> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() == null) {
                    Toast.makeText(getApplicationContext(), "API Inquiry Failed, TIMEOUT" , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Inquiry Failed, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();

                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    sessionManager.setPoin(result_poin);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else if (result_code.equals("0103")) {
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));

                } else {
                    Toast.makeText(getApplicationContext(),"GAGAL ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                if (t.getMessage() == null) {
                    Toast.makeText(getApplicationContext(), "API cs, TIMEOUT" , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean isEnoughBalance() {
        return (Integer.parseInt(sessionManager.getSaldo()) >= (mAdminCA + mTagihan));
    }

    private void toogleBtnBeli() {
        if (!isEnoughBalance()) {
            btnBeli.setEnabled(false);
            btnBeli.setBackgroundResource(R.color.colorGrid);
            txtSaldoTidakCukup.setVisibility(View.VISIBLE);
            Snackbar.make(myTopLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                    .setAction("Top Up", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(BPJSKesehatanActivity.this, DepositRequestActivity.class));
                        }
                    }).show();
        } else {
            txtSaldoTidakCukup.setVisibility(View.GONE);
            btnBeli.setEnabled(true);
            btnBeli.setBackgroundResource(R.drawable.button_bg);
        }
    }


    @OnClick(R.id.btn_beli)
    public void onBtnBeliClicked() {
        Intent intent = new Intent(BPJSKesehatanActivity.this, EnterPinBillPaymentActivity.class);
        intent.putExtra("idpel", mIdPel);
        intent.putExtra("namapelanggan", mNamaPelanggan);
        intent.putExtra("periode", mStrPeriode);
        intent.putExtra("amount", mTotalamount);
        intent.putExtra("adminca", mAdminCA);
        intent.putExtra("inquiryid", mInquiryId);
        intent.putExtra("systrace", mSystrace);
        intent.putExtra("billid", mBillid);
        intent.putExtra("debitsaldo", mTotal);
        intent.putExtra("tipe","BILLPAYMENT");
        intent.putExtra("provider",mProvider);
        intent.putExtra("kodeproduk", mKodeProduk);
        intent.putExtra("nohp", sessionManager.getMsisdn());
        intent.putExtra("periode", mPeriode);
        String myData = mProvider+ " " + mKodeProduk+ " " +mNoVirtualAccount+" " +mNamaPelanggan+" "+mStrPeriode+" "+mTagihan+" "+mAdminCA+" "+mTotalamount+" "+mInquiryId+" "+mSystrace+" "+mBillid+" "+mTotal;
        Log.d("BPJSKesehatan" , myData);

        startActivity(intent);

        //Toast.makeText(BPJSKesehatanActivity.this, "Under development", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(BPJSKesehatanActivity.this, MainTabActivity.class));
        return;
    }
}
