package id.co.popay.suwunapps.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.activity.BPJSKesehatanActivity;
import id.co.popay.suwunapps.activity.BantuanActivity;
import id.co.popay.suwunapps.activity.DanaActivity;
import id.co.popay.suwunapps.activity.EmoneyActivity;
import id.co.popay.suwunapps.activity.GopayCustomerActivity;
import id.co.popay.suwunapps.activity.OvoActivity;
import id.co.popay.suwunapps.activity.PDAMActivity;
import id.co.popay.suwunapps.activity.PLNTagihanActivity;
import id.co.popay.suwunapps.activity.PLNTokenActivity;
import id.co.popay.suwunapps.activity.PulsaDataActivity;
import id.co.popay.suwunapps.activity.PulsaRegulerActivity;
import id.co.popay.suwunapps.activity.SuwunPoinActivity;
import id.co.popay.suwunapps.activity.TcashActivity;
import id.co.popay.suwunapps.activity.TelkomTagihanActivity;
import id.co.popay.suwunapps.activity.VoucherGameActivity;
import id.co.popay.suwunapps.adapters.BlokMenuItemAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements BlokMenuItemAdapter.OnItemClickListener{

    private String mSaldo;
    private TextView txtSaldo;
    private TextView txtPoin;
    private BlokMenuItemAdapter mAdapter;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);

        mAdapter = new BlokMenuItemAdapter(getContext());
        mAdapter.setListener(this);
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSaldo = getArguments().getString("saldo");
//        txtSaldo = (TextView) getView().findViewById(R.id.txtSaldo);
//        txtPoin = (TextView) getView().findViewById(R.id.txtPoin);
//        txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(mSaldo)).replace(',', '.'));
//
//        txtSaldo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               startActivity(new Intent(getActivity(), DepositRequestActivity.class));
//            }
//        });
//
//        txtPoin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(getActivity(), SuwunPoinActivity.class));
//            }
//        });
    }

    @Override
    public void OnItemClickListener(int itemId) {

        if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_selular","mimpmap",getActivity().getPackageName())) {
            startActivity(new Intent(getActivity(), PulsaRegulerActivity.class));
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_internet","mimpmap",getActivity().getPackageName())) {
            startActivity(new Intent(getActivity(), PulsaDataActivity.class));
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_gamev","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), VoucherGameActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_plntoken","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), PLNTokenActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_gopay","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), GopayCustomerActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_bpjskesehatan","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), BPJSKesehatanActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_plntagihan","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), PLNTagihanActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_telepon","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), TelkomTagihanActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_pdam","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), PDAMActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_info","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), BantuanActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_redeem","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), SuwunPoinActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_emoney","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), EmoneyActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_ovo","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), OvoActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_tcash","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), TcashActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        } else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_dana","mimpmap",getActivity().getPackageName())) {
            Intent intent = new Intent(getActivity(), DanaActivity.class);
            intent.putExtra("saldo",mSaldo);
            startActivity(intent);
        }
//        else if (itemId == getResources().getIdentifier("@mipmap/ic_blokmenu_gametopup","mimpmap",getActivity().getPackageName())) {
//            Intent intent = new Intent(getActivity(), GameTopupActivity.class);
//            intent.putExtra("saldo",mSaldo);
//            startActivity(intent);
//        }
        else {
            Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_SHORT).show();
        }

    }
}
