package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResultRedeemtrxItem{

	@SerializedName("status_code")
	private String statusCode;

	@SerializedName("status_redeem")
	private String statusRedeem;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("remark")
	private String remark;

	@SerializedName("trxid")
	private String trxid;

	@SerializedName("kodeproduk")
	private String kodeproduk;

	@SerializedName("uid")
	private String uid;

	@SerializedName("endingpoin")
	private String endingpoin;

	@SerializedName("target_number")
	private String targetNumber;

	@SerializedName("info_redeem")
	private String infoRedeem;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("judul_item")
	private String judulItem;

	@SerializedName("poin_redeem")
	private String poinRedeem;

	@SerializedName("_id")
	private String id;

	@SerializedName("kode_item")
	private String kodeItem;

	public void setStatusCode(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode(){
		return statusCode;
	}

	public void setStatusRedeem(String statusRedeem){
		this.statusRedeem = statusRedeem;
	}

	public String getStatusRedeem(){
		return statusRedeem;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}

	public void setTrxid(String trxid){
		this.trxid = trxid;
	}

	public String getTrxid(){
		return trxid;
	}

	public void setKodeproduk(String kodeproduk){
		this.kodeproduk = kodeproduk;
	}

	public String getKodeproduk(){
		return kodeproduk;
	}

	public void setUid(String uid){
		this.uid = uid;
	}

	public String getUid(){
		return uid;
	}

	public void setEndingpoin(String endingpoin){
		this.endingpoin = endingpoin;
	}

	public String getEndingpoin(){
		return endingpoin;
	}

	public void setTargetNumber(String targetNumber){
		this.targetNumber = targetNumber;
	}

	public String getTargetNumber(){
		return targetNumber;
	}

	public void setInfoRedeem(String infoRedeem){
		this.infoRedeem = infoRedeem;
	}

	public String getInfoRedeem(){
		return infoRedeem;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setJudulItem(String judulItem){
		this.judulItem = judulItem;
	}

	public String getJudulItem(){
		return judulItem;
	}

	public void setPoinRedeem(String poinRedeem){
		this.poinRedeem = poinRedeem;
	}

	public String getPoinRedeem(){
		return poinRedeem;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setKodeItem(String kodeItem){
		this.kodeItem = kodeItem;
	}

	public String getKodeItem(){
		return kodeItem;
	}

	@Override
 	public String toString(){
		return 
			"ResultRedeemtrxItem{" + 
			"status_code = '" + statusCode + '\'' + 
			",status_redeem = '" + statusRedeem + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",remark = '" + remark + '\'' + 
			",trxid = '" + trxid + '\'' + 
			",kodeproduk = '" + kodeproduk + '\'' + 
			",uid = '" + uid + '\'' + 
			",endingpoin = '" + endingpoin + '\'' + 
			",target_number = '" + targetNumber + '\'' + 
			",info_redeem = '" + infoRedeem + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",judul_item = '" + judulItem + '\'' + 
			",poin_redeem = '" + poinRedeem + '\'' + 
			",_id = '" + id + '\'' + 
			",kode_item = '" + kodeItem + '\'' + 
			"}";
		}
}