package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResultTrxItem{

	@SerializedName("keterangan")
	private String keterangan;

	@SerializedName("status_code")
	private String statusCode;

	@SerializedName("productid")
	private String productid;

	@SerializedName("sup_price")
	private String supPrice;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("operator")
	private String operator;

	@SerializedName("rowid")
	private String rowid;

	@SerializedName("uid")
	private String uid;

	@SerializedName("nominal")
	private String nominal;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("price")
	private String price;

	@SerializedName("imei")
	private String imei;

	@SerializedName("msisdn")
	private String msisdn;

	@SerializedName("status")
	private String status;

	@SerializedName("info")
	private String info;

	@SerializedName("target_number")
	private String targetNumber;

	public void setKeterangan(String keterangan){
		this.keterangan= keterangan;
	}

	public String getKeterangan(){
		return keterangan;
	}

	public void setTargetNumber(String targetNumber){
		this.targetNumber = targetNumber;
	}

	public String getTargetNumber(){
		return targetNumber;
	}

	public void setStatusCode(String statusCode){
		this.statusCode = statusCode;
	}

	public String getStatusCode(){
		return statusCode;
	}

	public void setProductid(String productid){
		this.productid = productid;
	}

	public String getProductid(){
		return productid;
	}

	public void setSupPrice(String supPrice){
		this.supPrice = supPrice;
	}

	public String getSupPrice(){
		return supPrice;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setOperator(String operator){
		this.operator = operator;
	}

	public String getOperator(){
		return operator;
	}

	public void setRowid(String rowid){
		this.rowid = rowid;
	}

	public String getRowid(){
		return rowid;
	}

	public void setUid(String uid){
		this.uid = uid;
	}

	public String getUid(){
		return uid;
	}

	public void setNominal(String nominal){
		this.nominal = nominal;
	}

	public String getNominal(){
		return nominal;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setImei(String imei){
		this.imei = imei;
	}

	public String getImei(){
		return imei;
	}

	public void setMsisdn(String msisdn){
		this.msisdn = msisdn;
	}

	public String getMsisdn(){
		return msisdn;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setInfo(String info){
		this.info = info;
	}

	public String getInfo(){
		return info;
	}

	@Override
	public String toString(){
		return
				"ResultTrxItem{" +
						"status_code = '" + statusCode + '\'' +
						",productid = '" + productid + '\'' +
						",sup_price = '" + supPrice + '\'' +
						",created_at = '" + createdAt + '\'' +
						",operator = '" + operator + '\'' +
						",rowid = '" + rowid + '\'' +
						",uid = '" + uid + '\'' +
						",nominal = '" + nominal + '\'' +
						",updated_at = '" + updatedAt + '\'' +
						",price = '" + price + '\'' +
						",imei = '" + imei + '\'' +
						",msisdn = '" + msisdn + '\'' +
						",status = '" + status + '\'' +
						",info = '" + info + '\'' +
						"}";
	}
}