package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.BuildConfig;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseUser;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import id.co.popay.suwunapps.utils.ConnectivityUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class LoginActivity extends SessionManager {

    EditText edt_email, edt_password;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.ivLogoIg)
    ImageView ivLogoIg;
    @BindView(R.id.ivLogoFB)
    ImageView ivLogoFB;

    @BindView(R.id.txt_signup)
    TextView txtSignup;

    String mTimestamp, mSignature;
    @BindView(R.id.txtBelumPunya)
    TextView txtBelumPunya;
    @BindView(R.id.versionName)
    TextView versionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);

        versionName.setText("Version " + BuildConfig.VERSION_NAME);

    }

    public void displayRegister(View view) {
        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
    }

    @OnClick({R.id.btn_login, R.id.ivLogoIg, R.id.ivLogoFB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:                //cek apakah isian input sudah ada nlainya  semuanya
                if (TextUtils.isEmpty(edt_email.getText().toString())) {
                    edt_email.setError("username tidak boleh kosong");
                } else if (TextUtils.isEmpty(edt_password.getText().toString())) {
                    edt_password.setError("password tidak boleh kosong");
                } else {
                    if (ConnectivityUtil.isConnected(getApplicationContext())) {
                        loginuser();
                    } else {
                        Toast.makeText(this, "Tidak ada koneksi internet.", Toast.LENGTH_SHORT).show();
                    }

                }
                break;

            case R.id.ivLogoIg:
                Uri uriUrl = Uri.parse("http://www.instagram.com/suwunindonesia");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
                break;

            case R.id.ivLogoFB:
                Uri uriFBUrl = Uri.parse("https://www.facebook.com/Suwun-Indonesia-333382990507614/");
                Intent launchBrowserFB = new Intent(Intent.ACTION_VIEW, uriFBUrl);
                startActivity(launchBrowserFB);
                break;

        }

    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
//        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
//        alert.setMessage("Ingin keluar dari aplikasi?");
//        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                //moveTaskToBack(true);
//                //android.os.Process.killProcess(android.os.Process.myPid());
//                //System.exit(1);
//                finish();
//            }
//        });
//        alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//        alert.show();
        return;
    }

    private void loginuser() {
        showProgressDialog("Proses login.... ");
        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(LoginActivity.this);
        /*
        @Header("timestamp") String strTimestamp,
            @Field("id") String strEmailOrMSISDN,
            @Field("password") String strPassword,
            @Field("signature") String strSignature
         */
        String strEmail = edt_email.getText().toString();
        String strPassword = edt_password.getText().toString();

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + strEmail + strPassword);

        Call<ResponseUser> modelUserCall = myAPI.loginUser(mTimestamp, strEmail, strPassword, mSignature);
        modelUserCall.enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                if (result_code.equals("0000")) {
                    Log.d("LoginActivity", result_code + " " + result_desc);
                    String uid = response.body().getDataUser().getUid();
                    String namaUser = response.body().getDataUser().getUsername();
                    String emailUser = response.body().getDataUser().getEmail();
                    String saldoUser = response.body().getDataUser().getSaldo();
                    String poinUser = response.body().getDataUser().getPoin();
                    String Uid = response.body().getDataUser().getUid();
                    String dateJoin = response.body().getDataUser().getCreatedAt();
                    String msisdn = response.body().getDataUser().getUsermsisdn();
                    String id = response.body().getDataUser().getId();

                    //save to session
                    sessionManager.createSession(emailUser);
                    sessionManager.setPoin(poinUser);
                    sessionManager.setIdUser(namaUser);
                    sessionManager.setSaldo(saldoUser);
                    sessionManager.setUID(Uid);
                    sessionManager.setDateJoin(dateJoin);
                    sessionManager.setMsisdn(msisdn);
                    sessionManager.setEmail(emailUser);
                    sessionManager.setId(id);

                    Intent intent = new Intent(LoginActivity.this, MainTabActivity.class);
                    intent.putExtra("vEmailUser", emailUser);
                    intent.putExtra("vNamaUser", namaUser);
                    intent.putExtra("vSaldo", saldoUser);
                    intent.putExtra("vPoin", poinUser);
                    Toast.makeText(LoginActivity.this, "Selamat datang " + namaUser + "!", Toast.LENGTH_LONG).show();

                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, result_desc, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(LoginActivity.this, "API failure loginUser\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_LONG).show();
                if (t.getMessage() != null) {
                    Toast.makeText(getApplicationContext(), "Login gagal, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Login gagal, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void goToLupaPassword(View view) {
        startActivity(new Intent(this, LupaPasswordActivity.class));
    }

//    public void doLogin(View view) {
//        if (edt_email.getText().toString().isEmpty()) {
//            Toast.makeText(this, "Email harus diisi", Toast.LENGTH_SHORT).show();
//        } else if (edt_password.getText().toString().isEmpty()) {
//            Toast.makeText(this, "Password harus diisi", Toast.LENGTH_SHORT).show();
//        } else {
//            // tampilkan alert dialog untuk pindah transisi ke menu utama
//            AlertDialog.Builder alert = new AlertDialog.Builder(this);
//            LayoutInflater inflater = getLayoutInflater();
//            view = inflater.inflate(R.layout.login_progress, null);
//            alert.setView(view);
//            alert.setTitle("Proses login");
//            // alert.setMessage("Lagi login ......");
//            alert.show();
//
//            //simulasi login pake user password
//            Handler h = new Handler();
//            h.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
//                    finish();
//                }
//            }, 1000);
//        }
//    }


}
