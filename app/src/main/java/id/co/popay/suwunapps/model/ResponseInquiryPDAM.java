package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResponseInquiryPDAM{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	@SerializedName("result_info_pdam")
	private ResultInfoPdam resultInfoPdam;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	public void setResultInfoPdam(ResultInfoPdam resultInfoPdam){
		this.resultInfoPdam = resultInfoPdam;
	}

	public ResultInfoPdam getResultInfoPdam(){
		return resultInfoPdam;
	}

	@Override
 	public String toString(){
		return 
			"ResponseInquiryPDAM{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			",result_info_pdam = '" + resultInfoPdam + '\'' + 
			"}";
		}
}