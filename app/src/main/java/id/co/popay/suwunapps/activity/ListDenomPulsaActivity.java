package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.adapters.DenomPulsaItemAdapter;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.DenomPulsaItem;
import id.co.popay.suwunapps.model.ResponseDenomPulsa;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class ListDenomPulsaActivity extends SessionManager implements DenomPulsaItemAdapter.OnItemClickListener {
    private String mOperator,mTipe, mMSISDN;
    List<DenomPulsaItem> resultDenomPulsa;
    DenomPulsaItemAdapter mAdapter;
    RecyclerView recyclerView;
    private String mTimestamp, mSignature;
    CardView cvInfoZona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_denom_pulsa);

        cvInfoZona = (CardView) findViewById(R.id.cv_zonatelkomsel);
        cvInfoZona.setVisibility(View.GONE);
        Intent intent = getIntent();
        mOperator = intent.getStringExtra("operator");
        mTipe     = intent.getStringExtra("tipe");
        mMSISDN   = intent.getStringExtra("msisdn");
        //Toast.makeText(this, mMSISDN, Toast.LENGTH_LONG).show();

        recyclerView = (RecyclerView) findViewById(R.id.list_denom_container);
        if (mTipe.equals("PULSA")) {
            setTitle("Pulsa " + mOperator);
        } else if (mTipe.equals("DATA")) {
            setTitle("Paket Data " + mOperator);
            if (mOperator.equals("TELKOMSEL")) {
                cvInfoZona.setVisibility(View.VISIBLE);
            }
        } else if (mTipe.equals("GAME")) {
            setTitle("Voucher " + mOperator);
        } else if (mTipe.equals("GOPAY")) {
            setTitle("Saldo Gopay Customer");
        } else if (mTipe.equals("EMONEY")) {
            setTitle("Saldo E-money");
        } else if (mTipe.equals("OVO")) {
            setTitle("Saldo Ovo");
        } else if (mTipe.equals("TCASH")) {
            setTitle("Saldo T-Cash");
        } else if (mTipe.equals("DANA")) {
            setTitle("Saldo Dana");
        } else if (mTipe.equals("TOPUP_GAME")) {
            setTitle(mOperator);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        resultDenomPulsa = new ArrayList<>();
        mAdapter= new DenomPulsaItemAdapter(ListDenomPulsaActivity.this, resultDenomPulsa);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setListener(this);

        loadListDenom(mOperator);
    }

    private void loadListDenom(String mOperator) {
        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstances();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET);

        final Call<ResponseDenomPulsa> dataDenom = myAPI.getDenomPulsa( mTimestamp, mOperator ,mTipe, mSignature);

        dataDenom.enqueue(new Callback<ResponseDenomPulsa>() {
            @Override
            public void onResponse(Call<ResponseDenomPulsa> call, Response<ResponseDenomPulsa> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                if(result_code.equals("0000")) {
                    List<DenomPulsaItem> items = response.body().getDenomPulsa();
                    resultDenomPulsa.addAll(items);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseDenomPulsa> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void OnItemClickListener(String productId, String productDesc, String productNominal, String productPrice, String productInfo) {
        Intent intent = null;

        if (mTipe.equals("PULSA")) {
            intent = new Intent(ListDenomPulsaActivity.this, PulsaRegulerActivity.class);
        } else if (mTipe.equals("DATA")) {
            intent = new Intent(ListDenomPulsaActivity.this, PulsaDataActivity.class);
        } else if (mTipe.equals("GAME")) {
            intent = new Intent(ListDenomPulsaActivity.this, VoucherGameActivity.class);
        } else if (mTipe.equals("GOPAY")) {
            intent = new Intent(ListDenomPulsaActivity.this, GopayCustomerActivity.class);
        } else if (mTipe.equals("EMONEY")) {
            intent = new Intent(ListDenomPulsaActivity.this, EmoneyActivity.class);
        } else if (mTipe.equals("OVO")) {
            intent = new Intent(ListDenomPulsaActivity.this, OvoActivity.class);
        } else if (mTipe.equals("TCASH")) {
            intent = new Intent(ListDenomPulsaActivity.this, TcashActivity.class);
        } else if (mTipe.equals("DANA")) {
            intent = new Intent(ListDenomPulsaActivity.this, DanaActivity.class);
        } else if (mTipe.equals("TOPUP_GAME")) {
            intent = new Intent(ListDenomPulsaActivity.this, GameTopupActivity.class);
        }
            intent.putExtra("kodeproduk", productId);
            intent.putExtra("keterangan", productDesc);
            intent.putExtra("info", productInfo);
            intent.putExtra("nominal", productNominal);
            intent.putExtra("harga",productPrice);
            intent.putExtra("msisdn", mMSISDN);
            intent.putExtra("operator", mOperator);
            startActivity(intent);
    }

    public void goToBrowser(View view) {
        Uri uriUrl = Uri.parse("http://suwun.id/zona_telkomsel.php");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
