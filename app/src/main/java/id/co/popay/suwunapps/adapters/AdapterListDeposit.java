package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.ResultDepositItem;

/**
 * Created by ara on 21/02/18.
 */

public class AdapterListDeposit extends BaseAdapter {
    Context con;
    List<ResultDepositItem> dataDeposit;

    public AdapterListDeposit (Context c, List<ResultDepositItem> resultDeposit) {
        con =  c;
        dataDeposit = resultDeposit;
    }

    @Override
    public int getCount() {
        return dataDeposit.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)con.getSystemService(con.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.layout_list_deposit,null);

        //setting semua widget di dalem view layout_list_transaksi
        TextView txtItem = (TextView) view.findViewById(R.id.txtItem);
        TextView txtJumlahTransfer= (TextView) view.findViewById(R.id.txtJumlahTransfer);
        TextView txtStatus= (TextView) view.findViewById(R.id.txtStatus);
        TextView txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);

        String namaBank = dataDeposit.get(i).getNamabank();
        String strItem = "";
        if (namaBank.equals("ALFAMART")) {
            strItem = namaBank;
        } else {
            strItem = "Bank " + namaBank;
        }

        String strJumlahTransfer= String.format("Jumlah Transfer: Rp. %,d", Integer.parseInt(dataDeposit.get(i).getJumlahtransfer())).replace(',', '.') ;
        String strStatus = dataDeposit.get(i).getStatus();

        txtItem.setText(strItem);
        txtJumlahTransfer.setText(strJumlahTransfer);

        txtTanggal.setText(dataDeposit.get(i).getCreatedAt());

        switch (strStatus) {
            case "1" : txtStatus.setText("SEDANG DIPROSES"); txtStatus.setTextColor(Color.BLUE); break;
            case "2" : txtStatus.setText("SUDAH DIPROSES");
                       txtStatus.setTextColor(Color.DKGRAY);
                       txtStatus.setBackgroundResource(R.drawable.button_sukses); break;
            case "0" : txtStatus.setText("EXPIRED");  txtStatus.setTextColor(Color.RED); break;
            default : txtStatus.setText("SEDANG DIPROSES");
        }

        return view;
    }
}
