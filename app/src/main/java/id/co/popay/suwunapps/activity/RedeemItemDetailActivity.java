package id.co.popay.suwunapps.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.SessionManager;

public class RedeemItemDetailActivity extends SessionManager {
    @BindView(R.id.ivGambarRedeem)
    ImageView ivGambarRedeem;
    @BindView(R.id.tvJudulRedeem)
    TextView tvJudulRedeem;
    @BindView(R.id.tvDeksripsiRedeem)
    TextView tvDeksripsiRedeem;
    @BindView(R.id.btnTukarPoin)
    Button btnTukarPoin;
    @BindView(R.id.mTopLayout)
    LinearLayout mTopLayout;
    private String mItemGambar;
    private String mItemId, mItemTipe, mItemJudul, mItemDeskripsi, mItemPoin, mNominal, mKodeproduk;
    private String mMemberPoin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_redeem_item_detail);
        ButterKnife.bind(this);
        mMemberPoin = sessionManager.getPoin();


        Intent intent = getIntent();
        mItemGambar = intent.getStringExtra("itemGambar");
        mItemId = intent.getStringExtra("itemId");
        mItemTipe = intent.getStringExtra("itemTipe");
        mItemJudul = intent.getStringExtra("itemJudul");
        mItemDeskripsi = intent.getStringExtra("itemDeskripsi");
        mItemPoin = intent.getStringExtra("itemPoin");
        mNominal = intent.getStringExtra("itemNominal");
        mKodeproduk = intent.getStringExtra("itemKodeproduk");

        setTitle(mItemJudul);
        Picasso.with(this).load(mItemGambar).error(R.drawable.noimage).into(ivGambarRedeem);
        tvJudulRedeem.setText(mItemJudul + " " + "(" + mItemPoin + " poin)");
        tvDeksripsiRedeem.setText(mItemDeskripsi);

        if (Integer.parseInt(mMemberPoin) < Integer.parseInt(mItemPoin)) {
            btnTukarPoin.setEnabled(false);
            btnTukarPoin.setBackgroundResource(R.color.colorGrid);

            Snackbar.make(mTopLayout, "Poin Anda tidak cukup", Snackbar.LENGTH_LONG)
                    .show();
        } else {
            btnTukarPoin.setEnabled(true);
            btnTukarPoin.setBackgroundResource(R.drawable.button_bg);
        }
    }

    @OnClick(R.id.btnTukarPoin)
    public void onViewClicked() {
        Intent intent = null;

        if (mItemTipe.equals("PULSA")) {
            intent = new Intent(this, RedeemPulsaActivity.class);
        } else if (mItemTipe.equals("VOUCHER")) {
            displayDialog();
            return;
        } else if (mItemTipe.equals("BARANG")) {
            intent = new Intent(this, RedeemBarangActivity.class);
        } else if (mItemTipe.equals("GAME")) {
            intent = new Intent(this, RedeemGameActivity.class);
            intent.putExtra("itemKodeproduk", mKodeproduk);
        } else if (mItemTipe.equals("GOPAY")) {
            intent = new Intent(this, RedeemGopayActivity.class);
            intent.putExtra("itemKodeproduk", mKodeproduk);
        } else if (mItemTipe.equals("TOKEN_PLN")) {
            intent = new Intent(this, RedeemTokenPLNActivity.class);
            intent.putExtra("itemKodeproduk", mKodeproduk);
        }

        intent.putExtra("itemId", mItemId);
        intent.putExtra("itemTipe", mItemTipe);
        intent.putExtra("itemJudul", mItemJudul);
        intent.putExtra("itemDeskripsi", mItemDeskripsi);
        intent.putExtra("itemPoin", mItemPoin);
        intent.putExtra("itemGambar", mItemGambar);
        intent.putExtra("itemNominal", mNominal);
        startActivity(intent);
    }

    private void displayDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(RedeemItemDetailActivity.this);
        dialog.setTitle("Konfirmasi Tukar Poin");
        String pesan = "Anda akan menukarkan " + mItemPoin + " poin untuk " + mItemJudul + ".\n\nLanjut?";
        dialog.setMessage(pesan);

        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(RedeemItemDetailActivity.this, RedeemVoucherActivity.class);
                intent.putExtra("itemId", mItemId);
                intent.putExtra("itemTipe", mItemTipe);
                intent.putExtra("itemJudul", mItemJudul);
                intent.putExtra("itemDeskripsi", mItemDeskripsi);
                intent.putExtra("itemPoin", mItemPoin);
                intent.putExtra("itemGambar", mItemGambar);
                intent.putExtra("itemNominal", mNominal);
                startActivity(intent);
            }
        });

        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
