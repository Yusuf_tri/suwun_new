package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseGameUser;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class GameTopupActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;
    @BindView(R.id.saldoBar)
    LinearLayout saldoBar;
    @BindView(R.id.edtGameVoucher)
    EditText edtGameVoucher;
    @BindView(R.id.edtDenom)
    EditText edtDenom;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;
    @BindView(R.id.imgOperator)
    ImageView imgOperator;
    @BindView(R.id.txtOperator)
    TextView txtOperator;
    @BindView(R.id.logoBar)
    CardView logoBar;
    @BindView(R.id.txtProduk)
    TextView txtProduk;
    @BindView(R.id.txtHarga)
    TextView txtHarga;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.edtAccid)
    EditText edtAccid;
    @BindView(R.id.myTopLinearLayout)
    LinearLayout myTopLinearLayout;

    private String mMSISDN;
    private String mKodeProduk;
    private String mHarga;
    private String mProvider;
    private String mNominal;
    private String mDenomDescription, mInfo;
    private LinearLayout mLinearLayout;
    private String mTimestamp, mSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_topup);
        ButterKnife.bind(this);
        getSaldoFromServer();
        purchaseDetail.setVisibility(View.GONE);

        Intent intent = getIntent();
        mProvider = intent.getStringExtra("operator");
        mDenomDescription = intent.getStringExtra("keterangan");
        mKodeProduk = intent.getStringExtra("kodeproduk");
        mHarga = intent.getStringExtra("harga");
        mNominal = intent.getStringExtra("nominal");
        mInfo = intent.getStringExtra("info");

        edtGameVoucher.setText(mProvider);
        edtDenom.setText(mDenomDescription);

        if (intent.hasExtra("harga")) {
            purchaseDetail.setVisibility(View.VISIBLE);
            txtProduk.setText(mDenomDescription + "\n" + mInfo);
            txtHarga.setText(String.format("Harga Rp %,d", Integer.parseInt(mHarga)).replace(',', '.'));
            txtHarga.requestFocus();
        }

        if (mDenomDescription != null) {
            Log.d("GameTopupActivity", "Saldo " + sessionManager.getSaldo() + " harga " + mHarga);
            if (Integer.parseInt(sessionManager.getSaldo()) < Integer.parseInt(mHarga)) {
                //Toast.makeText(this, "Saldo Anda tidak cukup", Toast.LENGTH_LONG).show();
                mLinearLayout = (LinearLayout) findViewById(R.id.myTopLinearLayout);
                Snackbar.make(mLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                        .setAction("Top Up", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(GameTopupActivity.this, DepositRequestActivity.class));
                            }
                        }).show();
            } else {
                btnBeli.setEnabled(true);
                btnBeli.setBackgroundResource(R.drawable.button_bg);
            }
            //edtNomerHP.setText(mMSISDN);
        } else {
            mDenomDescription = "Denom voucher";
        }

    }

    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(GameTopupActivity.this, DepositRequestActivity.class));
    }

    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        startActivity(new Intent(GameTopupActivity.this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.edtGameVoucher)
    public void onEdtGameVoucherClicked() {
        startActivity(new Intent(GameTopupActivity.this, ListTopupGameActivity.class));
    }

    @OnClick(R.id.edtDenom)
    public void onEdtDenomClicked() {
        Log.d("GameTopupActivity", "Game Provider: " + mProvider);
        if (mProvider != null) {
            Intent intent = new Intent(GameTopupActivity.this, ListDenomPulsaActivity.class);
            intent.putExtra("operator", mProvider);
            intent.putExtra("tipe", "TOPUP_GAME");
            //intent.putExtra("msisdn", mMSISDN);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Pilih provider game terlebih dahulu", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.btn_beli)
    public void onBtnBeliClicked() {
        final String strAccid = edtAccid.getText().toString();
        String mUid = sessionManager.getId();

        if (strAccid.equals("")) {
            Toast.makeText(this, "Account ID masih kosong", Toast.LENGTH_SHORT).show();
        } else {
            showProgressDialog("");

            mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
            mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mUid + strAccid + mProvider );

            APIServices myAPI = InitNetLibrary.getInstanceWc(GameTopupActivity.this);
            String strParameter = "uid:" + mUid + "|";
            strParameter = strParameter + "provider:" + mProvider + "|";
            strParameter = strParameter + "timestamp:" + mTimestamp + "|";
            strParameter = strParameter + "signature:" + mSignature ;

            Log.d("GameTopupActivity", strParameter);
            Call<ResponseGameUser> gameUser = myAPI.getGameUser(mTimestamp, strAccid,  mProvider, mUid, mSignature);
            gameUser.enqueue(new Callback<ResponseGameUser>() {
                @Override
                public void onResponse(Call<ResponseGameUser> call, Response<ResponseGameUser> response) {
                    hideProgressDialog();
                    String result_code = response.body().getResultCode();
                    String result_desc = response.body().getResultDesc();

                    Log.d("GameTopupActivity", "Result: " + result_code + " " + result_desc   );

                    AlertDialog.Builder builder = new AlertDialog.Builder(GameTopupActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View v = inflater.inflate(R.layout.gametopupcekuser, null);
                    builder.setCancelable(false);
                    builder.setView(v);

                    final AlertDialog  dialog = builder.create();

                    TextView tvAkunid       = (TextView) v.findViewById(R.id.txtAkunid);
                    TextView tvNamaUser     = (TextView) v.findViewById(R.id.txtNamaUser);
                    TextView tvLevelUser    = (TextView) v.findViewById(R.id.txtLevelUser);
                    TextView tvBalance      = (TextView) v.findViewById(R.id.txtBalance);
                    Button btnOk            = (Button) v.findViewById(R.id.btn_OK);
                    Button btnBatal         = (Button) v.findViewById(R.id.btn_batal);
                    LinearLayout layoutStatusCheckUser = (LinearLayout) v.findViewById(R.id.statusCheckUser);
                    LinearLayout layoutDetailUser = (LinearLayout) v.findViewById(R.id.detailUser);
                    TextView tvStatusCheckUser = (TextView) v.findViewById(R.id.txtStatusCheckUser);


                    if (result_code.equals("0000") ) {
                        tvAkunid.setText(response.body().getResultGameuser().getTargetId());
                        tvNamaUser.setText(response.body().getResultGameuser().getTargetName());
                        tvLevelUser.setText(response.body().getResultGameuser().getTargetLevel());
                        tvBalance.setText(response.body().getResultGameuser().getTargetBalance());layoutStatusCheckUser.setVisibility(View.GONE);


                        btnOk.setVisibility(View.VISIBLE);
                        btnBatal.setText("BATAL");
                        btnBatal.setVisibility(View.VISIBLE);

                        layoutDetailUser.setVisibility(View.VISIBLE);
                        layoutStatusCheckUser.setVisibility(View.GONE);

                        btnOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                goEnterPIN(view);
                            }
                        });

                        btnBatal.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                    } else {
                        btnOk.setVisibility(View.GONE);
                        btnBatal.setVisibility(View.VISIBLE);
                        layoutStatusCheckUser.setVisibility(View.GONE);
                        btnBatal.setText("OK");

                        tvStatusCheckUser.setText("");
                        tvAkunid.setText(strAccid);
                        tvNamaUser.setText(response.body().getResultGameuser().getTargetName());
                        tvLevelUser.setText("-");
                        tvBalance.setText("0");

                        btnBatal.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                    }
                    dialog.show();
                }

                @Override
                public void onFailure(Call<ResponseGameUser> call, Throwable t) {
                    hideProgressDialog();
                    if (t.getMessage() != null) {
                        String msg = t.getMessage().replace(BASE_IP, "API_HOST");
                        msg = "Cek Daftar Transaksi (E-501)";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        Log.d("EnterPinActivity", t.toString() + t.getMessage());
                        startActivity(new Intent(GameTopupActivity.this, MainTabActivity.class));
                    } else {
                        Toast.makeText(getApplicationContext(), "TIMEOUT. Cek daftar transaksi.", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(GameTopupActivity.this, MainTabActivity.class));
                    }

                }
            });


        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        return;
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    sessionManager.setPoin(result_poin);
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    Log.d("MainTab", result_desc + " " + result_poin);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                if (t.getMessage() != null) {
                    Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public void goEnterPIN(View view) {
        Intent intent = new Intent(GameTopupActivity.this, EnterPinGameTopupActivity.class);
        intent.putExtra("msisdn", edtAccid.getText().toString());
        intent.putExtra("kodeproduk", mKodeProduk);
        intent.putExtra("harga", mHarga);
        intent.putExtra("uid", sessionManager.getId());
        intent.putExtra("tipe", "TOPUP_GAME");
        intent.putExtra("provider", mProvider);
        intent.putExtra("amount", mNominal);
        startActivity(intent);

    }
}
