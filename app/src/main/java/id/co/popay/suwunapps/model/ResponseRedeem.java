package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponseRedeem{

	@SerializedName("redeem_item")
	private List<RedeemItemItem> redeemItem;

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	public void setRedeemItem(List<RedeemItemItem> redeemItem){
		this.redeemItem = redeemItem;
	}

	public List<RedeemItemItem> getRedeemItem(){
		return redeemItem;
	}

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseRedeem{" + 
			"redeem_item = '" + redeemItem + '\'' + 
			",result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}