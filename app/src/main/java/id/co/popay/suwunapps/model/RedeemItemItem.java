package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class RedeemItemItem{

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("tipe")
	private String tipe;

	@SerializedName("panduan_item")
	private String panduanItem;

	@SerializedName("judul_item")
	private String judulItem;

	@SerializedName("poin_redeem")
	private String poinRedeem;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("_id")
	private String id;

	@SerializedName("deskripsi_item")
	private String deskripsiItem;

	@SerializedName("expired_at")
	private String expiredAt;

	@SerializedName("kode_item")
	private String kodeItem;

	@SerializedName("gambar")
	private String gambar;

	@SerializedName("nominal")
	private String nominal;

	@SerializedName("kodeproduk")
	private String kodeproduk;

	public void setKodeproduk(String kodeproduk){
		this.kodeproduk= kodeproduk;
	}

	public String getKodeproduk(){
		return kodeproduk;
	}

	public void setNominal(String nominal){
		this.nominal= nominal;
	}

	public String getNominal(){
		return nominal;
	}

	public void setTipe(String tipe){
		this.tipe= tipe;
	}

	public String getTipe(){
		return tipe;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setPanduanItem(String panduanItem){
		this.panduanItem = panduanItem;
	}

	public String getPanduanItem(){
		return panduanItem;
	}

	public void setJudulItem(String judulItem){
		this.judulItem = judulItem;
	}

	public String getJudulItem(){
		return judulItem;
	}

	public void setPoinRedeem(String poinRedeem){
		this.poinRedeem = poinRedeem;
	}

	public String getPoinRedeem(){
		return poinRedeem;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDeskripsiItem(String deskripsiItem){
		this.deskripsiItem = deskripsiItem;
	}

	public String getDeskripsiItem(){
		return deskripsiItem;
	}

	public void setExpiredAt(String expiredAt){
		this.expiredAt = expiredAt;
	}

	public String getExpiredAt(){
		return expiredAt;
	}

	public void setKodeItem(String kodeItem){
		this.kodeItem = kodeItem;
	}

	public String getKodeItem(){
		return kodeItem;
	}

	public void setGambar(String gambar){
		this.gambar = gambar;
	}

	public String getGambar(){
		return gambar;
	}

	@Override
 	public String toString(){
		return 
			"RedeemItemItem{" + 
			"updated_at = '" + updatedAt + '\'' + 
			",panduan_item = '" + panduanItem + '\'' + 
			",judul_item = '" + judulItem + '\'' + 
			",poin_redeem = '" + poinRedeem + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",_id = '" + id + '\'' + 
			",deskripsi_item = '" + deskripsiItem + '\'' + 
			",expired_at = '" + expiredAt + '\'' + 
			",kode_item = '" + kodeItem + '\'' + 
			",gambar = '" + gambar + '\'' + 
			"}";
		}
}