package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseRedeemHistory{

	@SerializedName("result_redeemtrx")
	private List<ResultRedeemtrxItem> resultRedeemtrx;

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultRedeemtrx(List<ResultRedeemtrxItem> resultRedeemtrx){
		this.resultRedeemtrx = resultRedeemtrx;
	}

	public List<ResultRedeemtrxItem> getResultRedeemtrx(){
		return resultRedeemtrx;
	}

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseRedeemHistory{" + 
			"result_redeemtrx = '" + resultRedeemtrx + '\'' + 
			",result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}