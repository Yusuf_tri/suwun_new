package id.co.popay.suwunapps.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class PulsaRegulerActivity extends SessionManager {



    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;

    @BindView(R.id.edtNomerHP)
    EditText edtNomerHP;
    @BindView(R.id.edtDenom)
    EditText edtDenom;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;
    @BindView(R.id.imgOperator)
    ImageView imgOperator;
    @BindView(R.id.txtOperator)
    TextView txtOperator;
    @BindView(R.id.logoBar)
    CardView logoBar;
    @BindView(R.id.txtProduk)
    TextView txtProduk;
    @BindView(R.id.txtHarga)
    TextView txtHarga;
    @BindView(R.id.LinearLayout)
    android.widget.LinearLayout LinearLayout;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.saldoBar)
    android.widget.LinearLayout saldoBar;
    @BindView(R.id.img_phonebook)
    ImageView imgPhonebook;
    @BindView(R.id.img_rightarrow)
    ImageView imgRightarrow;
    @BindView(R.id.scrollview)
    ScrollView scrollview;

    private TextWatcher txtWatcherMSISDN = null;
    private static final int TELKOMSEL = 1;
    private static final int INDOSAT = 2;
    private static final int XL = 3;
    private static final int SMARTFREN = 4;
    private static final int TRI = 5;
    private static final int AXIS = 6;
    private String strTelcoSelected = "";
    private String mDenomDescription;
    private String mKodeProduk;
    private String mMSISDN;
    private String mNominal;
    private String mHarga;
    private LinearLayout mLinearLayout;

    private String mTimestamp, mSignature;

    private final int REQUEST_CODE=99;
    private static final int REQUEST_CODE_APPS_SETTING = 98;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulsa_reguler);
        ButterKnife.bind(this);
        logoBar.setVisibility(View.GONE);
        purchaseDetail.setVisibility(View.GONE);

        txtWatcherMSISDN = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    int intCodeTelco = getTelco(s.toString());
                    //displayDenomSpinner(intCodeTelco );
                } else {
                    if (s.length() < 4) {
                        logoBar.setVisibility(View.GONE);
                        btnBeli.setEnabled(false);
                        btnBeli.setBackgroundResource(R.color.colorGrid);
                        edtDenom.setText("");
                        //edtNomerHP.setCompoundDrawablesWithIntrinsicBounds(null,null, null,null);
                    }
                }
            }
        };

        edtNomerHP.addTextChangedListener(txtWatcherMSISDN);
        getSaldoFromServer();
        //txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(sessionManager.getSaldo())).replace(',', '.'));

        Intent intent = getIntent();
        mDenomDescription = intent.getStringExtra("keterangan");
        mKodeProduk = intent.getStringExtra("kodeproduk");
        mMSISDN = intent.getStringExtra("msisdn");
        mNominal = intent.getStringExtra("nominal");
        mHarga = intent.getStringExtra("harga");

        if (intent.hasExtra("harga")) {
            purchaseDetail.setVisibility(View.VISIBLE);
            txtProduk.setText(String.format("Pulsa %,d", Integer.parseInt(mNominal)).replace(',', '.'));
            txtHarga.setText(String.format("Harga Rp %,d", Integer.parseInt(mHarga)).replace(',', '.'));

//            final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollview);
//            scrollView.post(new Runnable() {
//                @Override
//                public void run() {
//                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);
//                }
//            });
            txtHarga.requestFocus();
            //Log.d("PulsaRegulerActivity",intent.getStringExtra("harga"));
        }

        mLinearLayout = (LinearLayout) findViewById(R.id.LinearLayout);
        if (mDenomDescription != null && mMSISDN != null) {
            if (Integer.parseInt(sessionManager.getSaldo()) < Integer.parseInt(mHarga)) {
                Snackbar.make(mLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                        .setAction("Top Up", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(PulsaRegulerActivity.this, DepositRequestActivity.class));
                            }
                        }).show();
            } else {
                btnBeli.setEnabled(true);
                btnBeli.setBackgroundResource(R.drawable.button_bg);
            }
            edtNomerHP.setText(mMSISDN);
            getTelco(mMSISDN);
        } else {
            mDenomDescription = "Pilih Denom";
        }

        edtDenom.setText(mDenomDescription);

    }

    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(this, DepositRequestActivity.class));
        //startActivity(new Intent(this, DepositRequestRevActivity.class));
    }

    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        //Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.edtDenom)
    public void onEdtDenomClicked() {
        if ((edtNomerHP.getText().toString()).length() < 4) {
            Toast.makeText(PulsaRegulerActivity.this, "Operator tidak dikenal.\nCek kembali nomer HP.", Toast.LENGTH_SHORT).show();
        } else if ((edtNomerHP.getText().toString()).length() < 10) {
            Toast.makeText(PulsaRegulerActivity.this, "Nomer HP minimal 10 digit.", Toast.LENGTH_SHORT).show();
        } else {
            if (strTelcoSelected.equals("")) {
                int kode = getTelco(edtNomerHP.getText().toString());
                if (kode == -1) {
                    Toast.makeText(PulsaRegulerActivity.this, "Operator tidak dikenal", Toast.LENGTH_SHORT).show();
                } else {
                    String nomerHP = edtNomerHP.getText().toString();
                    Intent intent = new Intent(PulsaRegulerActivity.this, ListDenomPulsaActivity.class);
                    intent.putExtra("operator", strTelcoSelected);
                    intent.putExtra("tipe", "PULSA");
                    intent.putExtra("msisdn", nomerHP);
                    //Toast.makeText(this, "ini nomer HP:"+nomerHP, Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                }
            } else {
                String nomerHP = edtNomerHP.getText().toString();
                Intent intent = new Intent(PulsaRegulerActivity.this, ListDenomPulsaActivity.class);
                intent.putExtra("operator", strTelcoSelected);
                intent.putExtra("tipe", "PULSA");
                intent.putExtra("msisdn", nomerHP);
                //Toast.makeText(this, "ini nomer HP:"+nomerHP, Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        }
    }

    private int getTelco(String s) {
        int retValue = 0;
        logoBar.setVisibility(View.VISIBLE);

        switch (s.substring(0, 4)) {

            case "0811": //simpati
            case "0812": //simpati
            case "0813": //simpati
            case "0821": //simpati
            case "0822": //simpati
            case "0823": //as
            case "0851": //as
            case "0852": //as
            case "0853": //as
                retValue = 1;
                imgOperator.setImageResource(R.drawable.logo_telkomsel);
                strTelcoSelected = "TELKOMSEL";
                txtOperator.setText(strTelcoSelected);
                break;

            case "0814": //mentari
            case "0815": //mentari
            case "0816": //mentari
            case "0855": //mentari
            case "0856": //im3
            case "0857": //im3
            case "0858": //mentari
                retValue = 2;
                imgOperator.setImageResource(R.drawable.logo_indosat);
                strTelcoSelected = "INDOSAT";
                txtOperator.setText(strTelcoSelected);
                break;

            case "0817":
            case "0818":
            case "0819":
            case "0859":
            case "0877":
            case "0878":
            case "0879":
                retValue = 3;
                imgOperator.setImageResource(R.drawable.logo_xl);
                strTelcoSelected = "XL";
                txtOperator.setText(strTelcoSelected);
                break;

            case "0881":
            case "0882":
            case "0883":
            case "0884":
            case "0885":
            case "0886":
            case "0887":
            case "0888":
            case "0889":
                retValue = 4;
                imgOperator.setImageResource(R.drawable.logo_smartfren);
                strTelcoSelected = "SMARTFREN";
                txtOperator.setText(strTelcoSelected);
                break;

            case "0894":
            case "0895":
            case "0896":
            case "0897":
            case "0898":
            case "0899":
                retValue = 5;
                imgOperator.setImageResource(R.drawable.logo_three);
                strTelcoSelected = "TRI";
                txtOperator.setText(strTelcoSelected);
                break;

            case "0831":
            case "0832":
            case "0833":
            case "0838":
                retValue = 6;
                imgOperator.setImageResource(R.drawable.logo_axis);
                strTelcoSelected = "AXIS";
                txtOperator.setText(strTelcoSelected);
                break;

            default:
                retValue = -1;
                strTelcoSelected = "";
                //Toast.makeText(this, "Operator tidak dikenal (cek nomor)", Toast.LENGTH_SHORT).show();
                logoBar.setVisibility(View.GONE);
                btnBeli.setEnabled(false);
                btnBeli.setBackgroundResource(R.color.colorGrid);
        }
        return retValue;
    }


    public void displayListDenom(View view) {
        String nomerHP = edtNomerHP.getText().toString();
        Intent intent = new Intent(PulsaRegulerActivity.this, ListDenomPulsaActivity.class);
        intent.putExtra("operator", strTelcoSelected);
        intent.putExtra("tipe", "PULSA");
        intent.putExtra("msisdn", nomerHP);
        //Toast.makeText(this, "ini nomer HP:"+nomerHP, Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        return;
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());
        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    sessionManager.setPoin(result_poin);
                    //txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(sessionManager.getSaldo())).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP,"API_HOST") , Toast.LENGTH_SHORT).show();
                if (t.getMessage() != null) {
                    Toast.makeText(getApplicationContext(), "Ooops.. (E590) " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "TIMEOUT (E568)", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @OnClick(R.id.btn_beli)
    public void onViewClicked() {
        if ((edtNomerHP.getText().toString()).length() < 10) {
            Toast.makeText(PulsaRegulerActivity.this, "Nomer HP minimal 10 digit.", Toast.LENGTH_SHORT).show();
        } else if (strTelcoSelected.equals("")) {
            Toast.makeText(PulsaRegulerActivity.this, "Operator tidak dikenal.\nCek kembali nomer HP.", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(PulsaRegulerActivity.this, EnterPinActivity.class);
            intent.putExtra("msisdn", mMSISDN);
            intent.putExtra("kodeproduk", mKodeProduk);
            intent.putExtra("harga", mHarga);
            intent.putExtra("uid", sessionManager.getId());
            intent.putExtra("tipe", "PULSA");
            intent.putExtra("provider", strTelcoSelected);
            intent.putExtra("amount", mNominal);

            startActivity(intent);
        }
    }

    @OnClick(R.id.img_phonebook)
    public void onImgPhonebookClicked() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            showPhoneBook();
        } else {
            //requestPermissions(new String[] {Manifest.permission.READ_CONTACTS}, REQUEST_CODE);
            //tampilin Alert dialog untuk buka setting/pengaturan HP - Applikasi
            AlertDialog.Builder dialog = new AlertDialog.Builder(PulsaRegulerActivity.this);
            dialog.setTitle("Izin Aplikasi");
            dialog.setCancelable(false);
            dialog.setMessage("SUWUN belum mendapatkan izin untuk membuka daftar kontak. \nTap \"BERI IZIN\", pilih \"IZIN\" " +
                    "di tampilan selanjutnya. ");
            dialog.setPositiveButton("BERI IZIN", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    showActionSetting();
                }
            });

            dialog.setNegativeButton("NANTI SAJA", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
        }
    }

    private void showActionSetting() {
        //Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package",getPackageName(), null));
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",getPackageName(),null);
        intent.setData(uri);
        //tambahan opsional
        /*
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        */
        startActivity(intent);
    }

    private void showPhoneBook() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @OnClick(R.id.img_rightarrow)
    public void onImgRightarrowClicked() {
        onEdtDenomClicked();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (REQUEST_CODE) :
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c= getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        if (Integer.valueOf(hasNumber)==1) {
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                num = Suwunapps.formatPhoneNumber(num);
                                //Toast.makeText(PulsaRegulerActivity.this, "Number="+num, Toast.LENGTH_LONG).show();

                            }

                            int i = getTelco(num);
                            if (i == -1) {
                                Toast.makeText(this, "Operator tidak dikenal." , Toast.LENGTH_LONG).show();
                            }
                            edtNomerHP.setText(num);
                            edtDenom.setText("");
                            btnBeli.setEnabled(false);
                            btnBeli.setBackgroundResource(R.color.colorGrid);
                            purchaseDetail.setVisibility(View.GONE);
                        }
                    }
                }
                break;
        }
    }

}
