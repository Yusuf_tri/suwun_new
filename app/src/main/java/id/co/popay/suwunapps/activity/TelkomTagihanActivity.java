package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInquiryTelkomPSTN;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class TelkomTagihanActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;
    @BindView(R.id.edtIdPel)
    EditText edtIdPel;
    @BindView(R.id.btnCekPelanggan)
    Button btnCekPelanggan;
    @BindView(R.id.txtIdPel)
    TextView txtIdPel;
    @BindView(R.id.txtNamaPelanggan)
    TextView txtNamaPelanggan;
    @BindView(R.id.txtPeriode)
    TextView txtPeriode;
    @BindView(R.id.txtTagihan)
    TextView txtTagihan;
    @BindView(R.id.txtAdminCA)
    TextView txtAdminCA;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtSaldoTidakCukup)
    TextView txtSaldoTidakCukup;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.myTopLinearLayout)
    LinearLayout myTopLinearLayout;

    private String mTimestamp, mSignature;
    private String mIdPel;
    private TextWatcher txtWatcherIdPel = null;

    private String mNamaPelanggan;
    private int mPeriode;
    private String mStrPeriode;
    private int mTagihan;
    private int mTotalBayar;
    private String mInquiryId;
    private String mSystrace;
    private String mBillid;
    private int mAdminCA = 1000;
    private int mTotal;
    private String mStrTagihan;
    private String mProvider, mKodeproduk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telkom_tagihan);
        ButterKnife.bind(this);

        purchaseDetail.setVisibility(View.INVISIBLE);
        btnBeli.setVisibility(View.INVISIBLE);

        txtWatcherIdPel = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 8) {
                    btnCekPelanggan.setEnabled(true);
                    btnCekPelanggan.setBackgroundResource(R.drawable.button_bg);
                } else {
                    btnCekPelanggan.setEnabled(false);
                    btnCekPelanggan.setBackgroundResource(R.color.colorGrid);
                    purchaseDetail.setVisibility(View.INVISIBLE);
                    btnBeli.setVisibility(View.INVISIBLE);
                    btnBeli.setEnabled(false);
                    btnBeli.setBackgroundResource(R.color.colorGrid);
                    txtSaldoTidakCukup.setVisibility(View.GONE);
                }
            }
        };
        edtIdPel.addTextChangedListener(txtWatcherIdPel);
        getSaldoFromServer();

    }

    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(TelkomTagihanActivity.this, DepositRequestActivity.class));
    }


    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        //Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.btnCekPelanggan)
    public void onBtnCekPelangganClicked() {
        showProgressDialog("");
        btnBeli.setEnabled(false);
        purchaseDetail.setVisibility(View.INVISIBLE);
        btnBeli.setVisibility(View.INVISIBLE);
        btnBeli.setBackgroundResource(R.color.colorGrid);

        mIdPel = edtIdPel.getText().toString();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET +
                "BILL" + "TELKOM" +
                mIdPel + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseInquiryTelkomPSTN> inquiryBillPSTN= myAPI.inquiryBillPSTN(mTimestamp, "BILL", "TELKOM", mIdPel,
                sessionManager.getId(),  mSignature);

        inquiryBillPSTN.enqueue(new Callback<ResponseInquiryTelkomPSTN>() {
            @Override
            public void onResponse(Call<ResponseInquiryTelkomPSTN> call, Response<ResponseInquiryTelkomPSTN> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                Log.d("TelkomTagihanActivity", response.body().toString());
                if (result_code.equals("0000")) {
                    mNamaPelanggan = response.body().getResultInfoPstn().getNama();
                    mStrPeriode = response.body().getResultInfoPstn().getJumbulan();
                    mTagihan = (int)Float.parseFloat(response.body().getResultInfoPstn().getTotalamount());
                    mStrTagihan= response.body().getResultInfoPstn().getTotalamount();
                    mAdminCA = response.body().getResultInfoPstn().getAdminsuwun();
                    mInquiryId = response.body().getResultInfoPstn().getInquiryid();
                    mSystrace = response.body().getResultInfoPstn().getSystrace();
                    mBillid = response.body().getResultInfoPstn().getBillid();
                    mProvider = response.body().getResultInfoPstn().getProvider();
                    mKodeproduk = response.body().getResultInfoPstn().getKodeproduk();
                    String rincian = TextUtils.join("\n", response.body().getResultInfoPstn().getRincian());


                    txtIdPel.setText(edtIdPel.getText().toString());
                    txtNamaPelanggan.setText(mNamaPelanggan);
                    txtPeriode.setText(mStrPeriode + " bulan\n" + rincian);
                    txtTagihan.setText(String.format("Rp. %,d", mTagihan).replace(',', '.'));
                    txtAdminCA.setText(String.format("Rp. %,d", mAdminCA).replace(',', '.'));
                    mTotal = (mTagihan + mAdminCA);
                    txtTotal.setText(String.format("Rp. %,d", mTotal).replace(',', '.'));
                    purchaseDetail.setVisibility(View.VISIBLE);
                    btnBeli.setVisibility(View.VISIBLE);

                    toogleBtnBeli();

                    final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollview);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });

                } else {
                    Toast.makeText(TelkomTagihanActivity.this, "Inquiry gagal.\n" + result_desc, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseInquiryTelkomPSTN> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() == null) {
                    Toast.makeText(getApplicationContext(), "API cek tagihan gagal, TIMEOUT" , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cek tagihan gagal, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @OnClick(R.id.btn_beli)
    public void onBtnBeliClicked() {
        Intent intent = new Intent(TelkomTagihanActivity.this, EnterPinBillPaymentActivity.class);
        intent.putExtra("idpel", mIdPel);
        intent.putExtra("namapelanggan", mNamaPelanggan);
        intent.putExtra("periode", mStrPeriode);
        intent.putExtra("amount", mStrTagihan);
        intent.putExtra("adminca", mAdminCA);
        intent.putExtra("inquiryid", mInquiryId);
        intent.putExtra("systrace", mSystrace);
        intent.putExtra("billid", mBillid);
        intent.putExtra("debitsaldo", mTotal);
        intent.putExtra("tipe","BILLPAYMENT");
        intent.putExtra("provider",mProvider);
        intent.putExtra("kodeproduk", mKodeproduk);
        String myData = mIdPel+" " +mNamaPelanggan+" "+mStrPeriode+" "+mTagihan+" "+mAdminCA+" "+mInquiryId+" "+mSystrace+" "+mBillid+" "+mTotal;
        Log.d("TelkomTagihan" , myData);

        startActivity(intent);
        //Toast.makeText(TelkomTagihanActivity.this, "Under development", Toast.LENGTH_SHORT).show();
    }

    private boolean isEnoughBalance() {
        Log.d("TelkomBill", sessionManager.getSaldo() + " " + mAdminCA + " " + mTagihan);
        return (Integer.parseInt(sessionManager.getSaldo()) >= (mAdminCA + mTagihan));
    }

    private void toogleBtnBeli() {
        if (!isEnoughBalance()) {
            btnBeli.setEnabled(false);
            btnBeli.setBackgroundResource(R.color.colorGrid);
            txtSaldoTidakCukup.setVisibility(View.VISIBLE);
            Snackbar.make(myTopLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                    .setAction("Top Up", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(TelkomTagihanActivity.this, DepositRequestActivity.class));
                        }
                    }).show();
        } else {
            txtSaldoTidakCukup.setVisibility(View.GONE);
            btnBeli.setEnabled(true);
            btnBeli.setBackgroundResource(R.drawable.button_bg);
        }
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    sessionManager.setPoin(result_poin);
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else if (result_code.equals("0103")) {
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                if (t.getMessage()!=null) {
                    Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Fail, TIMEOUT." , Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
