package id.co.popay.suwunapps.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;

import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.activity.LoginActivity;

/**
 * Created by ara on 02/02/18.
 */

public class SessionManager extends MyCustomActivity{
    @VisibleForTesting

    /*variable sharepreference*/
            SharedPreferences pref;

    public SharedPreferences.Editor editor;
    public SessionManager sessionManager;

    /*mode share preference. Ini apa artinya??? */
    int mode = 0;

    /*nama dari share preference*/
    private static final String pref_name = "popaypref";

    /*kunci share preference*/
    private static final String loginkah = "islogin";
    public static final String kunci_id = "keyid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_main);
        sessionManager = new SessionManager(getApplicationContext());
        //setTheme(R.style.MyAppTheme);

    }
    public SessionManager(){

    }

    /*construktor*/
    public SessionManager(Context context) {
        /*mengakses class ini*/
        c = context;
        /*share preference dari class ini*/ /*(nama, mode)*/
        pref = context.getSharedPreferences(pref_name, mode);
        editor = pref.edit();
    }

    /*methode membuat session*/
    public void createSession(String email){
        /*login value menjadi true*/
        editor.putBoolean(loginkah, true);
        /*memasukkan email ke dalam variable kunci email*/
        editor.putString(kunci_id, email);
        editor.commit();
    }

    public void setIdUser(String iduser) {
        editor.putBoolean(loginkah, true);
        editor.putString("iduser", iduser);
        editor.commit();
    }

    public void setSaldo(String saldo) {
        editor.putBoolean(loginkah, true);
        editor.putString("saldo", saldo);
        editor.commit();
    }

    public void setPoin(String poin) {
        editor.putBoolean(loginkah, true);
        editor.putString("poin", poin);
        editor.commit();
    }

    public void setUID(String uid) {
        editor.putString("uid", uid);
        editor.commit();
    }

    public void setDateJoin(String dateJoin) {
        editor.putString("datejoin", dateJoin);
        editor.commit();
    }

    public void setMsisdn(String msisdn) {
        editor.putString("msisdn", msisdn);
        editor.commit();
    }

    public void setEmail(String email) {
        editor.putString("email", email);
        editor.commit();
    }

    public void setId(String Id) {
        editor.putString("id", Id);
        editor.commit();
    }

    public void checkLogin(){
        /*jika is_login = false*/
        if (!this.islogin()){
            /*pergi ke loginactivity*/
            Intent i = new Intent(c, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            c.startActivity(i);
        }else {
            /*jika true, pergi ke mainactivity*/
            Intent i = new Intent(c, MainTabActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            c.startActivity(i);
        }
    }



    /*set is_login menjadi false*/
    public boolean islogin() {
        return pref.getBoolean(loginkah, false);
    }

    public String getSaldo() {
        return pref.getString("saldo","");
    }
    public String getPoin() {
        return pref.getString("poin","");
    }
    public String getKunci_id() { return pref.getString(kunci_id,"");}
    public String getIdUser() {
        return pref.getString("iduser", "");
    }
    public String getUID() {return pref.getString("uid","");}
    public String getDateJoin() {return pref.getString("datejoin","");}
    public String getMsisdn() {return pref.getString("msisdn","");}
    public String getEmail() {return pref.getString("email","");}
    public String getId() {return pref.getString("id","");}


    public void logout(){

        /*hapus semua data dan kunci*/
        editor.clear();
        editor.commit();

        //gmail logout

        /*pergi ke loginactivity*/
        Intent i = new Intent(c, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(i);
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }



//    public void logoutWithAlert(){
//        AlertDialog.Builder dialog = new AlertDialog.Builder(c);
//        dialog.setTitle("Informasi");
//        dialog.setMessage("Apakah anda yakin ingin logout ? ");
//        dialog.setPositiveButton("YA", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                //moveTaskToBack(true);
//                //System.exit(0);
//                //android.os.Process.killProcess(android.os.Process.myPid());
//
//                /*hapus semua data dan kunci*/
//                editor.clear();
//                editor.commit();
//                /*pergi ke loginactivity*/
//                Intent i = new Intent(c, LoginActivity.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                c.startActivity(i);
//            }
//        });
//
//        dialog.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        });
//        dialog.show();
//
//    }

}
