package id.co.popay.suwunapps.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInfo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class EnterPinActivity extends SessionManager {

    @BindView(R.id.edt_pin)
    EditText edtPin;
    @BindView(R.id.mainCard)
    CardView mainCard;
    @BindView(R.id.btn_belipulsa)
    Button btnBelipulsa;
    @BindView(R.id.btn_batalpulsa)
    Button btnBatalpulsa;

    private String mDestinationNumber;
    private String mKodeproduk;
    private String mHarga;
    private String mUid;
    private String mTipe;
    private String mProvider;
    private String mAmount;

    private String mTimestamp;
    private String mSignature;
    Timestamp timestamp;

    TextView txtSaldo;
    TextView txtInfo, txtStatus, txtHeaderStatus;
    ImageView imgStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_pin);
        ButterKnife.bind(this);


//        intent.putExtra("msisdn",mMSISDN);
//        intent.putExtra("kodeproduk", mKodeProduk);
//        intent.putExtra("harga", mHarga);
//        intent.putExtra("uid", sessionManager.getUID());
//        intent.putExtra("tipe","DATA");
//        intent.putExtra("provider",strTelcoSelected);
//        intent.putExtra("amount", mNominal);

        Intent intent = getIntent();
        mDestinationNumber = intent.getStringExtra("msisdn");
        mKodeproduk = intent.getStringExtra("kodeproduk");
        mHarga = intent.getStringExtra("harga");
        mUid = intent.getStringExtra("uid");
        mTipe = intent.getStringExtra("tipe");
        mProvider = intent.getStringExtra("provider");
        mAmount = intent.getStringExtra("amount");
        Log.d("EnterPinActivity", mDestinationNumber + " " + mKodeproduk + " " + mHarga +
           " " + mUid + " " + mTipe + " " + mProvider + " " + mAmount) ;
    }

    @OnClick(R.id.btn_belipulsa)
    public void onBtnBelipulsaClicked() {
        String sPin = edtPin.getText().toString();
        if (sPin.equals("")) {
            Toast.makeText(this, "PIN masih kosong", Toast.LENGTH_SHORT).show();
        } else {
            showProgressDialog("");

//            timestamp = new Timestamp(System.currentTimeMillis());
//            mTimestamp = String.valueOf(timestamp.getTime());
//
//            try {
//                MessageDigest mdMD5 = MessageDigest.getInstance("MD5");
//                mdMD5.update(mTimestamp.getBytes());
//                byte[] output = mdMD5.digest();
//                StringBuffer hexString1 = new StringBuffer();
//                for (int i=0; i<output.length; i++) {
//                    hexString1.append(Integer.toHexString(0xFF & output[i]));
//                }
//                mSignature = hexString1.toString();
//                Log.d("EnterPinActivity", "MD5(" + mTimestamp + ") : " + mSignature);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//            }

            mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
            String strPin = Suwunapps.getEncPin(sPin, mTimestamp);
            mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mUid +
                                            strPin  +
                                           mTipe + mProvider + mAmount );

            APIServices myAPI = InitNetLibrary.getInstanceWc(this);
            String strParameter="uid:" + mUid + "|";
            strParameter = strParameter + "pin:" + strPin + "|";
            strParameter = strParameter + "tipe:" + mTipe+ "|";
            strParameter = strParameter + "provider:" + mProvider+ "|";
            strParameter = strParameter + "amount:" + mAmount+ "|";
            strParameter = strParameter + "nohp:" + mDestinationNumber+ "|";
            strParameter = strParameter + "timestamp:" + mTimestamp+ "|";
            strParameter = strParameter + "signature:" + mSignature+ "|";
            strParameter = strParameter + "kodeproduk: " + mKodeproduk;

            Log.d("EnterPinActivity",strParameter);
            Call<ResponseInfo> trxPulsa = myAPI.purchaseItem1(mTimestamp, mKodeproduk, mUid,
                                                strPin,
                                                mTipe, mProvider, mAmount, mDestinationNumber,  mSignature);
            trxPulsa.enqueue(new Callback<ResponseInfo>() {
                @Override
                public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                    hideProgressDialog();
                    String result_code = response.body().getResultCode();
                    String result_desc = response.body().getResultDesc();
                    //String result_info = response.body().getInfo();
                    Log.d("EnterPinActivity", "Result: " + result_code + " " + result_desc + " " + response.body().getBalance());

                    AlertDialog.Builder alert = new AlertDialog.Builder(EnterPinActivity.this).setCancelable(false);
                    LayoutInflater inflater = getLayoutInflater();
                    View v = inflater.inflate(R.layout.status_transaksi_pulsa, null);

                    txtStatus = (TextView) v.findViewById(R.id.txt_status);
                    txtInfo = (TextView) v.findViewById(R.id.txt_info);
                    imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                    txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);

                    //txtHeaderStatus.setText("Status pembelian " + mTipe);
                    Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                    btnOk.setVisibility(View.VISIBLE);

                    if (result_code.equals("0000")) {
                        txtStatus.setText(result_desc);
                        txtInfo.setText(response.body().getInfo());
                        //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                        imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                        //update saldo di sessionManager
                        sessionManager.setSaldo(response.body().getBalance());

                    } else if (result_code.equals("0168") || result_code.equals("2068")) {
                        if (result_desc.equals("PENDING")) {
                            txtStatus.setText("SEDANG DIPROSES");
                        } else {
                            txtStatus.setText(result_desc);
                        }
                        imgStatus.setImageResource(R.mipmap.ic_status_pending);
                        sessionManager.setSaldo(response.body().getBalance());

                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            //txtInfo.setText(result_desc);
                            if (result_desc.equals("PENDING")) {
                                txtStatus.setText("SEDANG DIPROSES");
                            } else {
                                txtStatus.setText(result_desc);
                            }

                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }

                    } else {
                        imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                        if (result_code.equals("1003")) {
                            //pin salah.
                            txtStatus.setText("GAGAL");
                            txtInfo.setText(result_desc );
                            btnOk.setVisibility(View.GONE);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //do nothing
                                }
                            });
                        } else if (result_code.equals("3777")) {
                            txtStatus.setText("GAGAL");
                            txtInfo.setText(result_desc );
                            btnOk.setVisibility(View.GONE);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(market_uri));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                        .FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                }
                            });

                        } else {
                            txtStatus.setText("GAGAL");
                            if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                                txtInfo.setText(result_desc );
                            } else {
                                txtInfo.setText(response.body().getInfo());
                            }
                        }
                        //txtInfo.setBackgroundResource(R.drawable.button_gagal);
                    }
                    alert.setView(v);
                    alert.show();
                }

                @Override
                public void onFailure(Call<ResponseInfo> call, Throwable t) {
                    hideProgressDialog();
//                    String pesan = t.getMessage();
//                    Toast.makeText(EnterPinActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
//                    Log.d("EnterPinActivity", pesan, t);
                    if (t.getMessage() != null ) {
                        String msg = t.getMessage().replace(BASE_IP, "API_HOST");
                        msg = "Cek Daftar Transaksi (E-501)";
                        Toast.makeText(getApplicationContext(),  msg , Toast.LENGTH_LONG).show();
                        Log.d("EnterPinActivity",  t.toString() + t.getMessage()   );
                        startActivity(new Intent(EnterPinActivity.this, MainTabActivity.class));
                    } else {
                        Toast.makeText(getApplicationContext(), "TIMEOUT. Cek daftar transaksi.", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(EnterPinActivity.this, MainTabActivity.class));
                    }
                }
            });
        }
    }

    @OnClick(R.id.btn_batalpulsa)
    public void onBtnBatalpulsaClicked() {
        startActivity(new Intent(EnterPinActivity.this, MainTabActivity.class));
    }

    public void displayHome(View view) {
        startActivity(new Intent(EnterPinActivity.this, MainTabActivity.class));
    }

    public void goToLupaPin(View view) {
        startActivity(new Intent(EnterPinActivity.this, LupaPinActivity.class));
    }
}
