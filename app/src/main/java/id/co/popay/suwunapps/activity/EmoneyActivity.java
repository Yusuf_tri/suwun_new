package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class EmoneyActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;
    @BindView(R.id.saldoBar)
    android.widget.LinearLayout saldoBar;
    @BindView(R.id.edtNomerHP)
    EditText edtNomerHP;
    @BindView(R.id.edtDenom)
    EditText edtDenom;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;
    @BindView(R.id.txtProduk)
    TextView txtProduk;
    @BindView(R.id.txtHarga)
    TextView txtHarga;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.LinearLayout)
    android.widget.LinearLayout mLinearLayout;


    private TextWatcher txtWatcherCardNumber= null;
    private String mDenomDescription;
    private String mKodeProduk;
    private String mMSISDN;
    private String mNominal;
    private String mHarga;
    private String mTimestamp, mSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emoney);
        ButterKnife.bind(this);

        purchaseDetail.setVisibility(View.GONE);
        txtWatcherCardNumber = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <16) {
                        btnBeli.setEnabled(false);
                        btnBeli.setBackgroundResource(R.color.colorGrid);
                }
            }
        };

        edtNomerHP.addTextChangedListener(txtWatcherCardNumber);
        getSaldoFromServer();

        Intent intent = getIntent();
        mDenomDescription = intent.getStringExtra("keterangan");
        mKodeProduk = intent.getStringExtra("kodeproduk");
        mMSISDN = intent.getStringExtra("msisdn");
        mNominal = intent.getStringExtra("nominal");
        mHarga = intent.getStringExtra("harga");

        if (intent.hasExtra("harga")) {
            purchaseDetail.setVisibility(View.VISIBLE);
            txtProduk.setText(mDenomDescription);
            txtHarga.setText(String.format("Harga Rp %,d", Integer.parseInt(mHarga)).replace(',', '.'));

            txtHarga.requestFocus();
        }

        if (mDenomDescription != null && mMSISDN != null) {
            if (Integer.parseInt(sessionManager.getSaldo()) < Integer.parseInt(mHarga)) {
                Snackbar.make(mLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                        .setAction("Top Up", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(EmoneyActivity.this, DepositRequestActivity.class));
                            }
                        }).show();
            } else {
                btnBeli.setEnabled(true);
                btnBeli.setBackgroundResource(R.drawable.button_bg);
            }
            edtNomerHP.setText(mMSISDN);
            //getTelco(mMSISDN);
        } else {
            mDenomDescription = "Pilih Denom Saldo";
        }

        edtDenom.setText(mDenomDescription);
        btnBeli.requestFocus();

    }

    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(EmoneyActivity.this, DepositRequestActivity.class));
    }

    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        //Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.edtDenom)
    public void onEdtDenomClicked() {
        if ((edtNomerHP.getText().toString()).length() != 16) {
            Toast.makeText(EmoneyActivity.this, "Nomor kartu e-money harus 16 digit.\nCek kembali nomer kartu Anda.", Toast.LENGTH_SHORT).show();
        } else {
            String nomerHP = edtNomerHP.getText().toString();
            Intent intent = new Intent(EmoneyActivity.this, ListDenomPulsaActivity.class);
            intent.putExtra("operator", "EMONEY");
            intent.putExtra("tipe", "EMONEY");
            intent.putExtra("msisdn", nomerHP);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_beli)
    public void onBtnBeliClicked() {
        if ((edtNomerHP.getText().toString()).length() != 16) {
            Toast.makeText(EmoneyActivity.this, "Nomor kartu e-money harus 16 digit.\nCek kembali nomer kartu Anda.", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(EmoneyActivity.this, EnterPinActivity.class);
            intent.putExtra("msisdn",mMSISDN);
            intent.putExtra("kodeproduk", mKodeProduk);
            intent.putExtra("harga", mHarga);
            intent.putExtra("uid", sessionManager.getUID());
            intent.putExtra("tipe","EMONEY");
            intent.putExtra("provider","EMONEY");
            intent.putExtra("amount", mNominal);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(EmoneyActivity.this, MainTabActivity.class));
        return;
    }


    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());
        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    sessionManager.setPoin(result_poin);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                    //txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(sessionManager.getSaldo())).replace(',', '.'));
                } else if (result_code.equals("0103")){
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                if (t.getMessage() == null) {
                    Toast.makeText(getApplicationContext(), "API cs Fail, TIMEOUT.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP,"API_HOST") , Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
}
