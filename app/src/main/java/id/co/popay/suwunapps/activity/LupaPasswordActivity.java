package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.MyCustomActivity;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class LupaPasswordActivity extends MyCustomActivity {

    @BindView(R.id.edt_emailterdaftar)
    TextInputEditText edtEmailterdaftar;
    @BindView(R.id.btn_kirimkode)
    Button btnKirimkode;
    @BindView(R.id.edt_kodereset)
    TextInputEditText edtKodereset;
    @BindView(R.id.edt_passwordbaru)
    TextInputEditText edtPasswordbaru;
    @BindView(R.id.btn_resetpassword)
    Button btnResetpassword;

    String mTimestamp, mSignature;
    String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_kirimkode)
    public void onBtnKirimkodeClicked() {
        if (TextUtils.isEmpty(edtEmailterdaftar.getText().toString())) {
            Toast.makeText(this, "Email terdaftar tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        } else {
            // call API, kirim kode
            kirimKodeResetPassword();
        }
    }


    @OnClick(R.id.btn_resetpassword)
    public void onBtnResetpasswordClicked() {
        if (TextUtils.isEmpty(edtKodereset.getText().toString())) {
            Toast.makeText(this, "Kode reset tidak boleh kosong. Cek email.", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(edtPasswordbaru.getText().toString())) {
            Toast.makeText(this, "Password tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        } else {
            resetPassword();
        }
    }

    private void kirimKodeResetPassword() {
        showProgressDialog("");
        APIServices myApi = InitNetLibrary.getInstances();
        mEmail = edtEmailterdaftar.getText().toString();

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + mEmail );

        Call<ResponseO> kirimKode = myApi.sendCode(mTimestamp, mEmail, mSignature );
        kirimKode.enqueue(new Callback<ResponseO>() {
            @Override
            public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    Toast.makeText(LupaPasswordActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LupaPasswordActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseO> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(LupaPasswordActivity.this, "API failure kirimKode\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_LONG).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API kirimKode Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API kirimKode Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void resetPassword() {
        showProgressDialog("");
        APIServices myApi = InitNetLibrary.getInstances();
        String strPassword = edtPasswordbaru.getText().toString();
        String strKodeReset = edtKodereset.getText().toString();
        mEmail = edtEmailterdaftar.getText().toString();

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + strKodeReset + strPassword + mEmail );

        Call<ResponseO> resetPass = myApi.updatePassword(mTimestamp, strKodeReset, strPassword, mEmail, mSignature);
        resetPass.enqueue(new Callback<ResponseO>() {
            @Override
            public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    Toast.makeText(LupaPasswordActivity.this, "Password baru telah diset.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LupaPasswordActivity.this, LoginActivity.class));
                } else {
                    Toast.makeText(LupaPasswordActivity.this, result_desc, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseO> call, Throwable t) {
                hideProgressDialog();
                //Toast.makeText(LupaPasswordActivity.this, "API failure updatePass\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_LONG).show();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API resetPass Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API resetPass Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }
}
