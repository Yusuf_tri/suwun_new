package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;


public class DenomPulsaItem{

	@SerializedName("kodeproduk")
	private String kodeproduk;

	@SerializedName("ispromo")
	private String ispromo;

	@SerializedName("info")
	private String info;

	@SerializedName("keterangan")
	private String keterangan;

	@SerializedName("nominal")
	private String nominal;

	@SerializedName("harga")
	private String harga;

	@SerializedName("provider")
	private String provider;

	@SerializedName("vendor")
	private String vendor;

	@SerializedName("tipe")
	private String tipe;

	@SerializedName("rowid")
	private String rowid;

	public void setIspromo(String ispromo){
		this.ispromo = ispromo;
	}

	public String getIspromo(){
		return ispromo;
	}

	public void setInfo(String info){
		this.info = info;
	}

	public String getInfo(){
		return info;
	}

	public void setKodeproduk(String kodeproduk){
		this.kodeproduk = kodeproduk;
	}

	public String getKodeproduk(){
		return kodeproduk;
	}

	public void setKeterangan(String keterangan){
		this.keterangan = keterangan;
	}

	public String getKeterangan(){
		return keterangan;
	}

	public void setNominal(String nominal){
		this.nominal = nominal;
	}

	public String getNominal(){
		return nominal;
	}

	public void setHarga(String harga){
		this.harga = harga;
	}

	public String getHarga(){
		return harga;
	}

	public void setProvider(String provider){
		this.provider = provider;
	}

	public String getProvider(){
		return provider;
	}

	public void setVendor(String vendor){
		this.vendor = vendor;
	}

	public String getVendor(){
		return vendor;
	}

	public void setTipe(String tipe){
		this.tipe = tipe;
	}

	public String getTipe(){
		return tipe;
	}

	public void setRowid(String rowid){
		this.rowid = rowid;
	}

	public String getRowid(){
		return rowid;
	}

	@Override
 	public String toString(){
		return 
			"DenomPulsaItem{" + 
			"kodeproduk = '" + kodeproduk + '\'' + 
			",keterangan = '" + keterangan + '\'' + 
			",nominal = '" + nominal + '\'' + 
			",harga = '" + harga + '\'' + 
			",provider = '" + provider + '\'' + 
			",vendor = '" + vendor + '\'' + 
			",tipe = '" + tipe + '\'' + 
			",rowid = '" + rowid + '\'' + 
			"}";
		}
}