package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResultDepositItem{

	@SerializedName("endingbalance")
	private String endingbalance;

	@SerializedName("uid")
	private String uid;

	@SerializedName("namabank")
	private String namabank;

	@SerializedName("jumlahtransfer")
	private String jumlahtransfer;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("expired_at")
	private String expiredAt;

	@SerializedName("rowid")
	private String rowid;

	@SerializedName("status")
	private String status;

	public void setEndingbalance(String endingbalance){
		this.endingbalance = endingbalance;
	}

	public String getEndingbalance(){
		return endingbalance;
	}

	public void setUid(String uid){
		this.uid = uid;
	}

	public String getUid(){
		return uid;
	}

	public void setNamabank(String namabank){
		this.namabank = namabank;
	}

	public String getNamabank(){
		return namabank;
	}

	public void setJumlahtransfer(String jumlahtransfer){
		this.jumlahtransfer = jumlahtransfer;
	}

	public String getJumlahtransfer(){
		return jumlahtransfer;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setExpiredAt(String expiredAt){
		this.expiredAt = expiredAt;
	}

	public String getExpiredAt(){
		return expiredAt;
	}

	public void setRowid(String rowid){
		this.rowid = rowid;
	}

	public String getRowid(){
		return rowid;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResultDepositItem{" + 
			"endingbalance = '" + endingbalance + '\'' + 
			",uid = '" + uid + '\'' + 
			",namabank = '" + namabank + '\'' + 
			",jumlahtransfer = '" + jumlahtransfer + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",expired_at = '" + expiredAt + '\'' + 
			",rowid = '" + rowid + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}