package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class VoucherGameActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;
    @BindView(R.id.saldoBar)
    LinearLayout saldoBar;
    @BindView(R.id.edtGameVoucher)
    EditText edtGameVoucher;
    @BindView(R.id.edtDenom)
    EditText edtDenom;
    @BindView(R.id.edtNomerHP)
    EditText edtNomerHP;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;
    @BindView(R.id.imgOperator)
    ImageView imgOperator;
    @BindView(R.id.txtOperator)
    TextView txtOperator;
    @BindView(R.id.logoBar)
    CardView logoBar;
    @BindView(R.id.txtProduk)
    TextView txtProduk;
    @BindView(R.id.txtHarga)
    TextView txtHarga;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.btn_beli)
    Button btnBeli;

    private String mMSISDN;
    private String mKodeProduk;
    private String mHarga;
    private String mProvider;
    private String mNominal;
    private String mDenomDescription;
    private LinearLayout mLinearLayout;
    private String mTimestamp, mSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_game);
        ButterKnife.bind(this);

        edtNomerHP.setText(sessionManager.getMsisdn());

        getSaldoFromServer();
        purchaseDetail.setVisibility(View.GONE);

        Intent intent = getIntent();
        mProvider = intent.getStringExtra("operator");
        mDenomDescription = intent.getStringExtra("keterangan");
        mKodeProduk = intent.getStringExtra("kodeproduk");
        mHarga = intent.getStringExtra("harga");
        mNominal = intent.getStringExtra("nominal");

        edtGameVoucher.setText(mProvider);
        edtDenom.setText(mDenomDescription);

        if (intent.hasExtra("harga")) {
            purchaseDetail.setVisibility(View.VISIBLE);
            txtProduk.setText(mDenomDescription);
            txtHarga.setText(String.format("Harga Rp %,d", Integer.parseInt(mHarga)).replace(',', '.'));

//            final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollview);
//            scrollView.post(new Runnable() {
//                @Override
//                public void run() {
//                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);
//                }
//            });
            txtHarga.requestFocus();
        }

        if (mDenomDescription != null ) {
            Log.d("VoucherGameActivity","Saldo " + sessionManager.getSaldo() + " harga " + mHarga  );
            if (Integer.parseInt(sessionManager.getSaldo()) < Integer.parseInt(mHarga)) {
                //Toast.makeText(this, "Saldo Anda tidak cukup", Toast.LENGTH_LONG).show();
                mLinearLayout = (LinearLayout) findViewById(R.id.myTopLinearLayout);
                Snackbar.make(mLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                        .setAction("Top Up", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(VoucherGameActivity.this, DepositRequestActivity.class));
                            }
                        }).show();
            } else {
                btnBeli.setEnabled(true);
                btnBeli.setBackgroundResource(R.drawable.button_bg);
            }
            //edtNomerHP.setText(mMSISDN);
        } else {
            mDenomDescription = "Denom voucher";
        }


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        return;
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    sessionManager.setPoin(result_poin);
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else if (result_code.equals("0103")) {
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"GAGAL ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                if (t.getMessage() != null) {
                    Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(VoucherGameActivity.this, DepositRequestActivity.class));
    }

    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        //Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.edtGameVoucher)
    public void onEdtGameVoucherClicked() {
        startActivity(new Intent(VoucherGameActivity.this, ListGameActivity.class));
    }

    @OnClick(R.id.edtDenom)
    public void onEdtDenomClicked() {
        Log.d("VoucherGameActivity", "Game Provider: " + mProvider);
        if (mProvider !=null) {
            Intent intent = new Intent(VoucherGameActivity.this, ListDenomPulsaActivity.class);
            intent.putExtra("operator", mProvider);
            intent.putExtra("tipe", "GAME");
            intent.putExtra("msisdn", mMSISDN);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Pilih provider game terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_beli)
    public void onBtnBeliClicked() {
        if ((edtNomerHP.getText().toString()).length() < 10) {
            Toast.makeText(VoucherGameActivity.this, "Nomer HP minimal 10 digit.", Toast.LENGTH_SHORT).show();
        } else {
            mMSISDN = edtNomerHP.getText().toString();
            Intent intent = new Intent(VoucherGameActivity.this, EnterPinActivity.class);
            intent.putExtra("msisdn",mMSISDN);
            intent.putExtra("kodeproduk", mKodeProduk);
            intent.putExtra("harga", mHarga);
            intent.putExtra("uid", sessionManager.getId());
            intent.putExtra("tipe","GAME");
            intent.putExtra("provider", mProvider);
            intent.putExtra("amount", mNominal);
            startActivity(intent);
        }
    }
}
