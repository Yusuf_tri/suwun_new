package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.adapters.PdamCityAdapter;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.CitiesItem;
import id.co.popay.suwunapps.model.ResponseCityPdam;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class ListPdamCityActivity extends SessionManager implements  PdamCityAdapter.OnItemClickListener {
    private String mOperator,mTipe, mMSISDN;
    List<CitiesItem> mCityList;
    PdamCityAdapter mAdapter;
    RecyclerView recyclerView;
    private String mTimestamp, mSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pdam_city);

        recyclerView = (RecyclerView)findViewById(R.id.list_pdamcity_container);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        loadListPdamCity();
    }

    private void loadListPdamCity() {
        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstances();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET);

        Call<ResponseCityPdam> pdamCity = myAPI.getCityPdam(mTimestamp,sessionManager.getUID(), mSignature);
        pdamCity.enqueue(new Callback<ResponseCityPdam>() {
            @Override
            public void onResponse(Call<ResponseCityPdam> call, Response<ResponseCityPdam> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                if(result_code.equals("0000")) {
                    List<CitiesItem> items = response.body().getCities();
                    Log.d("kota-kota",items.toString());
                    //mCityList.addAll(items);
                    mAdapter = new PdamCityAdapter(ListPdamCityActivity.this, items);
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.setListener(ListPdamCityActivity.this);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseCityPdam> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void OnItemClickLister(String cityId, String cityName) {
        Intent intent = new Intent(ListPdamCityActivity.this, PDAMActivity.class);
        intent.putExtra("cityId", cityId);
        intent.putExtra("cityName", cityName);
        startActivity(intent);
    }
}
