package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultInfoPstn{

	@SerializedName("provider")
	private String provider;

	@SerializedName("kodeproduk")
	private String kodeproduk;

	@SerializedName("adminsuwun")
	private int adminsuwun;

	@SerializedName("nama")
	private String nama;

	@SerializedName("biaya")
	private int biaya;

	@SerializedName("totalamount")
	private String totalamount;

	@SerializedName("jumbulan")
	private String jumbulan;

	@SerializedName("inquiryid")
	private String inquiryid;

	@SerializedName("rincian")
	private List<String> rincian;

	@SerializedName("systrace")
	private String systrace;

	@SerializedName("billid")
	private String billid;

	@SerializedName("idpel")
	private String idpel;

	public void setProvider(String provider){
		this.provider= provider;
	}

	public String getProvider(){
		return provider;
	}

	public void setKodeproduk(String kodeproduk){ this.kodeproduk= kodeproduk; }

	public String getKodeproduk(){
		return kodeproduk;
	}

	public void setAdminsuwun(int adminsuwun){
		this.adminsuwun = adminsuwun;
	}

	public int getAdminsuwun(){
		return adminsuwun;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setBiaya(int biaya){
		this.biaya = biaya;
	}

	public int getBiaya(){
		return biaya;
	}

	public void setTotalamount(String totalamount){
		this.totalamount = totalamount;
	}

	public String getTotalamount(){
		return totalamount;
	}

	public void setJumbulan(String jumbulan){
		this.jumbulan = jumbulan;
	}

	public String getJumbulan(){
		return jumbulan;
	}

	public void setInquiryid(String inquiryid){
		this.inquiryid = inquiryid;
	}

	public String getInquiryid(){
		return inquiryid;
	}

	public void setRincian(List<String> rincian){
		this.rincian = rincian;
	}

	public List<String> getRincian(){
		return rincian;
	}

	public void setSystrace(String systrace){
		this.systrace = systrace;
	}

	public String getSystrace(){
		return systrace;
	}

	public void setBillid(String billid){
		this.billid = billid;
	}

	public String getBillid(){
		return billid;
	}

	public void setIdpel(String idpel){
		this.idpel = idpel;
	}

	public String getIdpel(){
		return idpel;
	}

	@Override
 	public String toString(){
		return 
			"ResultInfoPstn{" + 
			"adminsuwun = '" + adminsuwun + '\'' + 
			",nama = '" + nama + '\'' + 
			",biaya = '" + biaya + '\'' + 
			",totalamount = '" + totalamount + '\'' + 
			",jumbulan = '" + jumbulan + '\'' + 
			",inquiryid = '" + inquiryid + '\'' + 
			",rincian = '" + rincian + '\'' + 
			",systrace = '" + systrace + '\'' + 
			",billid = '" + billid + '\'' + 
			",idpel = '" + idpel + '\'' + 
			"}";
		}
}