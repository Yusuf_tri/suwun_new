package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponseTransaksi {

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_trx")
	private List<ResultTrxItem> resultTrx;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultTrx(List<ResultTrxItem> resultTrx){
		this.resultTrx = resultTrx;
	}

	public List<ResultTrxItem> getResultTrx(){
		return resultTrx;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseTransaksi{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_trx = '" + resultTrx + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}