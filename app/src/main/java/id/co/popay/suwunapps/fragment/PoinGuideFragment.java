package id.co.popay.suwunapps.fragment;


import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import id.co.popay.suwunapps.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PoinGuideFragment extends Fragment {


    public PoinGuideFragment() {
        // Required empty public constructor
    }

    WebView wv_skbpoin;
    String url = "http://suwun.id/skbsuwun.php";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_poin_guide, container, false);
        wv_skbpoin = (WebView)v.findViewById(R.id.wv_skbpoin);
        wv_skbpoin.loadUrl(url);
        WebSettings settings = wv_skbpoin.getSettings();
        settings.setJavaScriptEnabled(true);
        wv_skbpoin.setWebViewClient(new MyWebViewClient());
        return v;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            final Uri uri = Uri.parse(url);
            //return super.shouldOverrideUrlLoading(view, url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            //return super.shouldOverrideUrlLoading(view, request);
            view.loadUrl(request.getUrl().toString());
            return true;
        }
    }
}
