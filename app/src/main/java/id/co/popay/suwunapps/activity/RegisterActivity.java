package id.co.popay.suwunapps.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Timestamp;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class RegisterActivity extends AppCompatActivity {
    private EditText edt_regemail;
    private EditText edt_regnama;
    private EditText edt_regnohp;
    private EditText edt_regpassword, edt_regconfpassword;
    private EditText edt_regpin;

    String strEmail, strNama, strNoHP, strPassword, strConfPassword, strPIN;
    Timestamp timestamp;
    String mTimestamp, mSignature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initUI();
    }

    private void initUI() {
        edt_regemail = (EditText)findViewById(R.id.edt_regemail);
        edt_regnama  = (EditText)findViewById(R.id.edt_regnama);
        edt_regnohp  = (EditText)findViewById(R.id.edt_regnohp);
        edt_regpassword = (EditText)findViewById(R.id.edt_regpassword);
        edt_regconfpassword = (EditText)findViewById(R.id.edt_regconfpassword);
        //edt_regpin = (EditText)findViewById(R.id.edt_regPIN);
    }

    public void displayHome(View v) {
        finish();
    }

    public void prosesDaftar(View v) {
        final ProgressDialog dialog = ProgressDialog.show(RegisterActivity.this, "Proses Register", "Loading...");

        strEmail = edt_regemail.getText().toString();
        strNama = edt_regnama.getText().toString();
        strNoHP = edt_regnohp.getText().toString();
        strPassword = edt_regpassword.getText().toString();
        strConfPassword = edt_regconfpassword.getText().toString();
        //strPIN = edt_regpin.getText().toString();

        //cek dulu semua input, sudah keiisi atau masih ada yg kosoTextUtils.isEmpty(edt_regnama.getText().toString())g.
        if (TextUtils.isEmpty(strEmail)) {
            edt_regemail.setError("email tidak boleh kosong");
            dialog.dismiss();
        } else if (TextUtils.isEmpty(strNama)) {
            edt_regnama.setError("nama tidak boleh kosong");
            dialog.dismiss();
        } else if (TextUtils.isEmpty(strNoHP)) {
            edt_regnohp.setError("Nomer HP tidak boleh kosong");
            dialog.dismiss();
        } else if (TextUtils.isEmpty(strPassword)) {
            edt_regpassword.setError("Password tidak boleh kosong");
            dialog.dismiss();
        } else if (TextUtils.isEmpty(strConfPassword)) {
            edt_regconfpassword.setError("Konfirmasi password tidak boleh kosong");
            dialog.dismiss();
        } else if (!strPassword.equals(strConfPassword)) {
            edt_regconfpassword.setError("Konfirmasi password tidak sama");
            dialog.dismiss();
        } else {
        /*
        @Header("timestamp") String strTimestamp,
            @Field("username") String strUsername,
            @Field("email") String strEmail,
            @Field("msisdn") String strMsisdn,
            @Field("password") String strPassword,
            @Field("signature") String strSignature         */

            mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
            mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET);

            APIServices myAPI = InitNetLibrary.getInstances();
            Call<ResponseO> myResponse = myAPI.registerUser(mTimestamp, strNama,strEmail, strNoHP,strPassword,mSignature);

            myResponse.enqueue(new Callback<ResponseO>() {
                @Override
                public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                    dialog.dismiss();
                    String result_code = response.body().getResultCode();
                    String result_desc = response.body().getResultDesc();

                    if (result_code.equals("0000")) {
                        Toast.makeText(RegisterActivity.this, "Proses register berhasil.\n\nAnda akan menerima SMS berisi PIN transaksi.", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                    } else {
                        Toast.makeText(RegisterActivity.this, "Proses register gagal.\n"+result_desc, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseO> call, Throwable t) {
                    dialog.dismiss();
                    //Toast.makeText(RegisterActivity.this, "Failure call API registerUser\n\n" + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                    if (t.getMessage() == null) {
                        Toast.makeText(getApplicationContext(), "API regU Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "API regU Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }
}
