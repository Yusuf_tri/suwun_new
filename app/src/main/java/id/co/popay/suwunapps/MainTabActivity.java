package id.co.popay.suwunapps;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import id.co.popay.suwunapps.activity.BantuanActivity;
import id.co.popay.suwunapps.activity.DepositRequestActivity;
import id.co.popay.suwunapps.activity.GantiPasswordActivity;
import id.co.popay.suwunapps.activity.GantiPinActivity;
import id.co.popay.suwunapps.activity.LoginActivity;
import id.co.popay.suwunapps.activity.SuwunPoinActivity;
import id.co.popay.suwunapps.fragment.HomeFragment;
import id.co.popay.suwunapps.fragment.TrxHistoryFragment;
import id.co.popay.suwunapps.fragment.UserProfileFragment;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseO;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class MainTabActivity extends SessionManager {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private HomeFragment mHomeFragment;
    private TrxHistoryFragment mTrxHistoryFragment;
    private UserProfileFragment mUserProfileFragment;
    private String mTimestamp, mSignature;
    private ImageView ivTambahSaldo;
    private TextView tvSaldo, tvPoin;
    private LinearLayout layoutSaldo, layoutPoin;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //tambahan aink
        tvPoin  = (TextView) findViewById(R.id.tvPoin);
        tvSaldo = (TextView) findViewById(R.id.tvSaldo);
        ImageView ivTambahSaldo = (ImageView) findViewById(R.id.ivTambahSaldo);

        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        //ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        //ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)
        //toolbar.setBackgroundColor(Color.WHITE);
        //toolbar.setTitleTextColor(Color.BLACK);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        //untuk swipeRefresh trx History
//        if (savedInstanceState == null) {
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            TrxHistoryFragment fragment = new TrxHistoryFragment();
//            transaction.replace(R.id.sample_content_fragment, fragment);
//            transaction.commit();
//        }

        //untuk klik icon ImageView tambah saldo
        ivTambahSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainTabActivity.this, DepositRequestActivity.class));
            }
        });

        //untuk klik area text Saldo
        layoutSaldo = (LinearLayout) findViewById(R.id.layoutSaldo);
        layoutSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainTabActivity.this, DepositRequestActivity.class));
            }
        });

        //untuk klik area text Poin

        layoutPoin = (LinearLayout) findViewById(R.id.layoutPoin);
        layoutPoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SuwunPoinActivity.class));
                //Toast.makeText(MainTabActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });

        //ambil data saldo uid ybs dari server.
        getSaldoFromServer();


    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mHomeFragment = new HomeFragment();
            mTrxHistoryFragment = new TrxHistoryFragment();
            mUserProfileFragment = new UserProfileFragment();
            //getSaldoFromServer();

            //send some values to HomeFragment
            Bundle argument = new Bundle();
            argument.putString("saldo",sessionManager.getSaldo());
            argument.putString("poin", sessionManager.getPoin());
            argument.putString("userId", sessionManager.getUID());
            argument.putString("dateJoin", sessionManager.getDateJoin());
            argument.putString("msisdn", sessionManager.getMsisdn());
            argument.putString("username", sessionManager.getIdUser());
            argument.putString("email", sessionManager.getEmail());
            argument.putString("id", sessionManager.getId());

            mHomeFragment.setArguments(argument);
            mUserProfileFragment.setArguments(argument);
            mTrxHistoryFragment.setArguments(argument);

        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position) {
                case 0 :
                    return mHomeFragment;
                case 1 :
                    return mTrxHistoryFragment;
                case 2 :
                    return mUserProfileFragment;
            }
            //return PlaceholderFragment.newInstance(position + 1);
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "HOME";
                case 1:
                    return "HISTORY";
                case 2:
                    return "PROFILE";
            }
            return null;
        }
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        //mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getUID());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());
        Log.d("MainTabActivity", "APPKEY:" + MyConstants.APP_KEY + "-" + MyConstants.APP_SECRET + sessionManager.getId());

       APIServices myAPI = InitNetLibrary.getInstanceWithCookie(MainTabActivity.this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    sessionManager.setPoin(result_poin);
                    Log.d("MainTab", result_desc + " " + result_poin);
                    tvSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc )).replace(',', '.'));
                    tvPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else if (result_code.equals("0103")){
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    //sessionManager.logout();
                    startActivity(new Intent(MainTabActivity.this, LoginActivity.class));
//                    sessionManager.setSaldo("0");
//                    sessionManager.setPoin("0");
//                    Log.d("MainTab", result_desc + " " + result_poin);
//                    tvSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
//                    tvPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() != null) {
                    //Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP,"API_HOST") , Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Gagal ambil profile user, " + t.getMessage().replace(BASE_IP,"API_HOST"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal ambil profile user, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void doLogout(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainTabActivity.this);
        alert.setMessage("Ingin logout?");
        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                diLogout();
            }
        });
        alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        return;
    }

    public void diLogout() {
        String mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        String mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());
        APIServices myAPI = InitNetLibrary.getInstanceWc(this);
        Call<ResponseO> logout = myAPI.logout(mTimestamp, sessionManager.getId(), mSignature);
        logout.enqueue(new Callback<ResponseO>() {
            @Override
            public void onResponse(Call<ResponseO> call, Response<ResponseO> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    Toast.makeText(MainTabActivity.this, "Logout Success", Toast.LENGTH_SHORT).show();
                    sessionManager.logout();
                } else {
                    Toast.makeText(MainTabActivity.this, "Logout Failed (E-" + result_code+")", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseO> call, Throwable t) {
                Toast.makeText(MainTabActivity.this, "FAIL Logout. " + t.getMessage().replace(MyConstants.BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void goToGantiPin(View view) {
        startActivity(new Intent(this, GantiPinActivity.class));
    }

    public void goToGantiPassword(View view) {
        startActivity(new Intent(this, GantiPasswordActivity.class));
    }

//    public void displaySaldo(String mSaldo) {
//        tvSaldo.setText(String.format("Rp. %,d", Integer.parseInt(sessionManager.getSaldo())).replace(',', '.'));
//        //tvPoin.setText(String.format("%,d", Integer.parseInt("100000")).replace(',', '.'));
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_bantuan) {
            //return true;
            startActivity(new Intent(MainTabActivity.this, BantuanActivity.class));
        }

/*
        else if (id == R.id.action_logout) {


            AlertDialog.Builder alert = new AlertDialog.Builder(MainTabActivity.this);
            alert.setMessage("Ingin logout?");
            alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    sessionManager.logout();
                }
            });
            alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alert.show();
        } else if (id == R.id.action_daftarharga) {
            Toast.makeText(MainTabActivity.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
        }
*/

        return super.onOptionsItemSelected(item);
    }

//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//        /**
//         * The fragment argument representing the section number for this
//         * fragment.
//         */
//        private static final String ARG_SECTION_NUMBER = "section_number";
//
//        public PlaceholderFragment() {
//        }
//
//        /**
//         * Returns a new instance of this fragment for the given section
//         * number.
//         */
//        public static PlaceholderFragment newInstance(int sectionNumber) {
//            PlaceholderFragment fragment = new PlaceholderFragment();
//            Bundle args = new Bundle();
//            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            fragment.setArguments(args);
//            return fragment;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main_tab, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            return rootView;
//        }
//    }

}
