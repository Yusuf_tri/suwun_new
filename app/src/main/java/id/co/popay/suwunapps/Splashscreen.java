package id.co.popay.suwunapps;

import android.os.Bundle;
import android.os.Handler;

import java.sql.Timestamp;

import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;

public class Splashscreen extends SessionManager {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        final String _versionName = id.co.popay.suwunapps.BuildConfig.VERSION_NAME;

        String mTimestamp, mSignature;

        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET );

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                sessionManager.checkLogin();
                finish();
            }
        },2000);

//        APIServices myAPI = InitNetLibrary.getInstances();
//        Call<ResponseKonfigurasi> cekConfig = myAPI.getKonfig(mTimestamp,  mSignature);
//        cekConfig.enqueue(new Callback<ResponseKonfigurasi>() {
//            @Override
//            public void onResponse(Call<ResponseKonfigurasi> call, Response<ResponseKonfigurasi> response) {
//                String result_code = response.body().getResultCode();
//                String result_desc = response.body().getResultDesc();
//                if (result_code.equals("0000")) {
//                    String vn = id.co.popay.suwunapps.BuildConfig.VERSION_NAME;
//                    List<String> listKonfig = response.body().getResultInfokonfig();
//                    Log.d("splashscreen", result_desc);
//                    if (! vn.equals(result_desc) ) {
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(Splashscreen.this);
//                        dialog.setTitle("Upgrade Aplikasi");
//                        dialog.setCancelable(false);
//                        dialog.setMessage("Current version: " + vn + "\n" +
//                                "Versi database: " + result_desc + "\n\n" +
//                                "Versi terbaru sudah tersedia di PLAYSTORE.\nSilakan lakukan upgrade aplikasi.");
//                        dialog.setPositiveButton("UPGRADE", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
//                                Intent intent = new Intent(Intent.ACTION_VIEW);
//                                intent.setData(Uri.parse(market_uri));
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
//                                        .FLAG_ACTIVITY_NO_ANIMATION);
//                                startActivity(intent);
//                            }
//                        });
//
//                        dialog.setNegativeButton("NANTI", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                sessionManager.checkLogin();
//                                finish();
//                            }
//                        });
//
//                        dialog.show();
//
//                    } else {
//                        Handler h = new Handler();
//                        h.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                sessionManager.checkLogin();
//                                finish();
//                            }
//                        },2000);
//
//                    }
//                } else {
//                    Handler h = new Handler();
//                    h.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            sessionManager.checkLogin();
//                            finish();
//                        }
//                    },2000);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseKonfigurasi> call, Throwable t) {
//            }
//        });


//        if (! _versionName.equals(dbVersion)) {
//            AlertDialog.Builder dialog = new AlertDialog.Builder(Splashscreen.this);
//            dialog.setTitle("Upgrade Aplikasi");
//            dialog.setMessage("Current version: " + _versionName + "\n" +
//                            "Versi database: " + dbVersion + "\n\n" +
//                            "Versi terbaru sudah tersedia di PLAYSTORE.\nSilakan lakukan upgrade aplikasi.");
//            dialog.setPositiveButton("UPGRADE", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    String market_uri = "https://play.google.com/store/apps/details?id=id.co.popay.suwunapps";
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse(market_uri));
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
//                            .FLAG_ACTIVITY_NO_ANIMATION);
//                    startActivity(intent);
//                }
//            });
//
//            dialog.setNegativeButton("NANTI", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    dialogInterface.dismiss();
//                    sessionManager.checkLogin();
//                    finish();
//                }
//            });
//
//            dialog.show();
//
//        } else {
            //menampilkan logo dengan jeda waktu
//            Handler h = new Handler();
//            h.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    sessionManager.checkLogin();
//                    finish();
//                }
//            },2000);

//        }


    }

}
