package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResponseInquiryBillPLN{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_info_pln")
	private ResultInfoPln resultInfoPln;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultInfoPln(ResultInfoPln resultInfoPln){
		this.resultInfoPln = resultInfoPln;
	}

	public ResultInfoPln getResultInfoPln(){
		return resultInfoPln;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseInquiryBillPLN{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_info_pln = '" + resultInfoPln + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}