package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResponseGameUser{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_gameuser")
	private ResultGameuser resultGameuser;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultGameuser(ResultGameuser resultGameuser){
		this.resultGameuser = resultGameuser;
	}

	public ResultGameuser getResultGameuser(){
		return resultGameuser;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseGameUser{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_gameuser = '" + resultGameuser + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}