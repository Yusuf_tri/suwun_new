package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class GameProviderItem{

	@SerializedName("provider")
	private String provider;

	@SerializedName("playstore_link")
	private String playstoreLink;

	@SerializedName("gambar")
	private String gambar;

	@SerializedName("rowid")
	private String rowid;

	public void setProvider(String provider){
		this.provider = provider;
	}

	public String getProvider(){
		return provider;
	}

	public void setPlaystoreLink(String playstoreLink){
		this.playstoreLink = playstoreLink;
	}

	public String getPlaystoreLink(){
		return playstoreLink;
	}

	public void setGambar(String gambar){
		this.gambar = gambar;
	}

	public String getGambar(){
		return gambar;
	}

	public void setRowid(String rowid){
		this.rowid = rowid;
	}

	public String getRowid(){
		return rowid;
	}

	@Override
 	public String toString(){
		return 
			"GameProviderItem{" + 
			"provider = '" + provider + '\'' + 
			",playstore_link = '" + playstoreLink + '\'' + 
			",gambar = '" + gambar + '\'' + 
			",rowid = '" + rowid + '\'' + 
			"}";
		}
}