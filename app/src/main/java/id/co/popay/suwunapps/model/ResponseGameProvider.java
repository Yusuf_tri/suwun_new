package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponseGameProvider{

	@SerializedName("game_provider")
	private List<String> gameProvider;

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_code")
	private String resultCode;

	public void setGameProvider(List<String> gameProvider){
		this.gameProvider = gameProvider;
	}

	public List<String> getGameProvider(){
		return gameProvider;
	}

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseGameProvider{" + 
			"game_provider = '" + gameProvider + '\'' + 
			",result_desc = '" + resultDesc + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}