package id.co.popay.suwunapps.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInfo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class RedeemGopayActivity extends SessionManager {

    @BindView(R.id.ivGambarRedeem)
    ImageView ivGambarRedeem;
    @BindView(R.id.tvJudulRedeem)
    TextView tvJudulRedeem;
    @BindView(R.id.edtNomerHP)
    EditText edtNomerHP;
    @BindView(R.id.msisdnCardView)
    CardView msisdnCardView;
    @BindView(R.id.btn_beli)
    Button btnBeli;

    private String mMSISDN;
    private String mItemGambar, mItemId, mItemTipe, mItemJudul, mItemDeskripsi, mItemPoin, mItemKodeproduk;
    private String mTimestamp, mSignature;
    private TextWatcher txtWatcherMSISDN = null;

    TextView txtSaldo;
    TextView txtInfo, txtStatus, txtHeaderStatus;
    ImageView imgStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_gopay);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mItemGambar = intent.getStringExtra("itemGambar");
        mItemId = intent.getStringExtra("itemId");
        mItemTipe = intent.getStringExtra("itemTipe");
        mItemJudul = intent.getStringExtra("itemJudul");
        mItemDeskripsi = intent.getStringExtra("itemDeskripsi");
        mItemPoin = intent.getStringExtra("itemPoin");
        mItemKodeproduk  = intent.getStringExtra("itemKodeproduk");

        Picasso.with(this).load(mItemGambar).error(R.drawable.noimage).into(ivGambarRedeem);
        tvJudulRedeem.setText(mItemJudul + " " + "(" + mItemPoin + " poin)");
        txtWatcherMSISDN = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 10 ) {
                    btnBeli.setEnabled(false);
                    btnBeli.setBackgroundResource(R.color.colorGrid);
                } else {
                    btnBeli.setEnabled(true);
                    btnBeli.setBackgroundResource(R.drawable.button_bg);
                }

            }
        };
        edtNomerHP.addTextChangedListener(txtWatcherMSISDN);
    }

    @OnClick(R.id.btn_beli)
    public void onViewClicked() {
        mMSISDN = edtNomerHP.getText().toString();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getUID() +
                mItemTipe + mItemPoin + mItemKodeproduk + mMSISDN);

        doRedeemGopay();

    }

    private void doRedeemGopay() {
        String data =mTimestamp+ "-" + mItemTipe + "-" + mItemPoin + "-" + mItemId + "-" +  mItemJudul + "-" + mItemKodeproduk + "-" + mMSISDN;
        Log.d("RedeemGopay", data);

        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstances();
        Call<ResponseInfo> redeemGopay = myAPI.redeemGopay(mTimestamp,mItemTipe,mItemPoin, mItemId, mItemJudul, mItemKodeproduk, mMSISDN, sessionManager.getUID(), mSignature);
        redeemGopay.enqueue(new Callback<ResponseInfo>() {
            @Override
            public void onResponse(Call<ResponseInfo> call, Response<ResponseInfo> response) {
                hideProgressDialog();
                String result_desc= response.body().getResultDesc();
                String result_code= response.body().getResultCode();
                AlertDialog.Builder alert = new AlertDialog.Builder(RedeemGopayActivity.this).setCancelable(false);

                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.status_transaksi_pulsa, null);

                txtStatus = (TextView) v.findViewById(R.id.txt_status);
                txtInfo = (TextView) v.findViewById(R.id.txt_info);
                imgStatus = (ImageView) v.findViewById(R.id.imgStatus);
                txtHeaderStatus = (TextView) v.findViewById(R.id.txtHeaderStatus);

                txtHeaderStatus.setText("Status redeem");
                Button btnOk = (Button) v.findViewById(R.id.btn_OK);
                btnOk.setVisibility(View.VISIBLE);

                if (result_code.equals("0000")) {
                    txtStatus.setText(result_desc);
                    txtInfo.setText(response.body().getInfo() );
                    //txtInfo.setBackgroundResource(R.drawable.button_sukses);
                    imgStatus.setImageResource(R.mipmap.ic_status_sukses);
                    //update saldo di sessionManager
                    sessionManager.setPoin(response.body().getBalance());

                } else if (result_code.equals("0168") || result_code.equals("2068")) {
                    if (result_desc.equals("PENDING")) {
                        txtStatus.setText("SEDANG DIPROSES");
                    } else {
                        txtStatus.setText(result_desc);
                    }
                    imgStatus.setImageResource(R.mipmap.ic_status_pending);
                    sessionManager.setPoin(response.body().getBalance());

                    if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                        //txtInfo.setText(result_desc);
                        if (result_desc.equals("PENDING")) {
                            txtStatus.setText("SEDANG DIPROSES");
                        } else {
                            txtStatus.setText(result_desc);
                        }

                    } else {
                        txtInfo.setText(response.body().getInfo() );
                    }

                } else {
                    imgStatus.setImageResource(R.mipmap.ic_status_gagal);
                    if (result_code.equals("1003")) {
                        //pin salah.
                        txtStatus.setText("GAGAL");
                        txtInfo.setText(result_desc );
                        btnOk.setVisibility(View.GONE);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        });
                    } else {
                        txtStatus.setText("GAGAL");
                        if (response.body().getInfo() == null || response.body().getInfo().equals("")) {
                            txtInfo.setText(result_desc );
                        } else {
                            txtInfo.setText(response.body().getInfo());
                        }
                    }
                    //txtInfo.setBackgroundResource(R.drawable.button_gagal);
                }
                alert.setView(v);
                alert.show();

            }

            @Override
            public void onFailure(Call<ResponseInfo> call, Throwable t) {
                hideProgressDialog();
                String pesan = t.getMessage();
//                Toast.makeText(RedeemGameActivity.this, "Call API failed.\n"+ t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                Log.d("RedeemGopay", pesan, t);
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API redeemGp Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API redeemGp Failed, TIMEOUT. Cek daftar riwayat redeem.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void displayHome(View view) {
        startActivity(new Intent(RedeemGopayActivity.this, SuwunPoinActivity.class));
    }

}
