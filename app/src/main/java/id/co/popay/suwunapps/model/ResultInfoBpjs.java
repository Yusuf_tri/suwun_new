package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResultInfoBpjs{

	@SerializedName("provider")
	private String provider;

	@SerializedName("kodeproduk")
	private String kodeproduk;

	@SerializedName("branchid")
	private String branchid;

	@SerializedName("idpel")
	private String idpel;

	@SerializedName("branch")
	private String branch;

	@SerializedName("periode")
	private String periode;

	@SerializedName("jumpeserta")
	private String jumpeserta;

	@SerializedName("totalbayar")
	private int totalbayar;

	@SerializedName("nokapst")
	private String nokapst;

	@SerializedName("tagihan")
	private int tagihan;

	@SerializedName("nama")
	private String nama;

	@SerializedName("balance")
	private int balance;

	@SerializedName("biaya")
	private int biaya;

	@SerializedName("totalamount")
	private String totalamount;

	@SerializedName("inquiryid")
	private String inquiryid;

	@SerializedName("systrace")
	private String systrace;

	@SerializedName("billid")
	private String billid;

	@SerializedName("nova")
	private String nova;

	@SerializedName("adminsuwun")
	private String adminsuwun;

	public void setProvider(String provider){
		this.provider= provider;
	}

	public String getProvider(){
		return provider;
	}

	public void setKodeproduk(String kodeproduk){ this.kodeproduk= kodeproduk; }

	public String getKodeproduk(){
		return kodeproduk;
	}

	public void setAdminsuwun(String adminsuwun) {this.adminsuwun = adminsuwun; }

	public String getAdminsuwun() {return adminsuwun; }

	public void setBranchid(String branchid){
		this.branchid = branchid;
	}

	public String getBranchid(){
		return branchid;
	}

	public void setIdpel(String idpel){
		this.idpel = idpel;
	}

	public String getIdpel(){
		return idpel;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setPeriode(String periode){
		this.periode = periode;
	}

	public String getPeriode(){
		return periode;
	}

	public void setJumpeserta(String jumpeserta){
		this.jumpeserta = jumpeserta;
	}

	public String getJumpeserta(){
		return jumpeserta;
	}

	public void setTotalbayar(int totalbayar){
		this.totalbayar = totalbayar;
	}

	public int getTotalbayar(){
		return totalbayar;
	}

	public void setNokapst(String nokapst){
		this.nokapst = nokapst;
	}

	public String getNokapst(){
		return nokapst;
	}

	public void setTagihan(int tagihan){
		this.tagihan = tagihan;
	}

	public int getTagihan(){
		return tagihan;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setBalance(int balance){
		this.balance = balance;
	}

	public int getBalance(){
		return balance;
	}

	public void setBiaya(int biaya){
		this.biaya = biaya;
	}

	public int getBiaya(){
		return biaya;
	}

	public void setTotalamount(String totalamount){
		this.totalamount = totalamount;
	}

	public String getTotalamount(){
		return totalamount;
	}

	public void setInquiryid(String inquiryid){
		this.inquiryid = inquiryid;
	}

	public String getInquiryid(){
		return inquiryid;
	}

	public void setSystrace(String systrace){
		this.systrace = systrace;
	}

	public String getSystrace(){
		return systrace;
	}

	public void setBillid(String billid){
		this.billid = billid;
	}

	public String getBillid(){
		return billid;
	}

	public void setNova(String nova){
		this.nova = nova;
	}

	public String getNova(){
		return nova;
	}

	@Override
 	public String toString(){
		return 
			"ResultInfoBpjs{" + 
			"branchid = '" + branchid + '\'' + 
			",idpel = '" + idpel + '\'' + 
			",branch = '" + branch + '\'' + 
			",periode = '" + periode + '\'' + 
			",jumpeserta = '" + jumpeserta + '\'' + 
			",totalbayar = '" + totalbayar + '\'' + 
			",nokapst = '" + nokapst + '\'' + 
			",tagihan = '" + tagihan + '\'' + 
			",nama = '" + nama + '\'' + 
			",balance = '" + balance + '\'' + 
			",biaya = '" + biaya + '\'' + 
			",totalamount = '" + totalamount + '\'' + 
			",inquiryid = '" + inquiryid + '\'' + 
			",systrace = '" + systrace + '\'' + 
			",billid = '" + billid + '\'' + 
			",nova = '" + nova + '\'' + 
			"}";
		}
}