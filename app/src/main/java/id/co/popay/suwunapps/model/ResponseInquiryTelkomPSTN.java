package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;


public class ResponseInquiryTelkomPSTN{

	@SerializedName("result_desc")
	private String resultDesc;

	@SerializedName("result_info_pstn")
	private ResultInfoPstn resultInfoPstn;

	@SerializedName("result_code")
	private String resultCode;

	public void setResultDesc(String resultDesc){
		this.resultDesc = resultDesc;
	}

	public String getResultDesc(){
		return resultDesc;
	}

	public void setResultInfoPstn(ResultInfoPstn resultInfoPstn){
		this.resultInfoPstn = resultInfoPstn;
	}

	public ResultInfoPstn getResultInfoPstn(){
		return resultInfoPstn;
	}

	public void setResultCode(String resultCode){
		this.resultCode = resultCode;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"ResponseInquiryTelkomPSTN{" + 
			"result_desc = '" + resultDesc + '\'' + 
			",result_info_pstn = '" + resultInfoPstn + '\'' + 
			",result_code = '" + resultCode + '\'' + 
			"}";
		}
}