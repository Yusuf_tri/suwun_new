package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import id.co.popay.suwunapps.MainTabActivity;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.fragment.PoinGuideFragment;
import id.co.popay.suwunapps.fragment.PoinHistoryFragment;
import id.co.popay.suwunapps.fragment.PoinRedeemFragment;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class SuwunPoinActivity extends SessionManager {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private TextView txtSaldo;
    private TextView txtPoin;
    private ImageView ivTambahSaldo;
    private LinearLayout layoutTambahSaldo;

    private PoinGuideFragment mPoinGuideFragment;
    private PoinHistoryFragment mPoinHistoryFragment;
    private PoinRedeemFragment mPoinRedeemFragment;

    private String mTimestamp;
    private String mSignature;

    private WebView wv_skbpoin;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suwun_poin);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowTitleEnabled(true);
        //ab.setDisplayShowHomeEnabled(true);
        //ab.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitleTextColor(Color.BLACK);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        //toolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SuwunPoinActivity.this, MainTabActivity.class));
                //finish();
            }
        });

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        txtSaldo = (TextView)findViewById(R.id.txtSaldo);
        txtPoin  = (TextView)findViewById(R.id.txtPoin);
        ivTambahSaldo = (ImageView)findViewById(R.id.ivTambahSaldo);
        layoutTambahSaldo = (LinearLayout)findViewById((R.id.layoutSaldo));

        ivTambahSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goTambahSaldo();
            }
        });

        layoutTambahSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goTambahSaldo();
            }
        });

        //tambahan skb poin suwun, loading web page.
//        wv_skbpoin =(WebView) findViewById(R.id.wv_skbpoin);
//        wv_skbpoin.getSettings().setJavaScriptEnabled(true);
//        wv_skbpoin.loadUrl("http://suwun.id/skbsuwun.php");

        getSaldoFromServer();
    }

    public void goTambahSaldo() {
        startActivity(new Intent(SuwunPoinActivity.this, DepositRequestActivity.class));
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_suwun_poin, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mPoinGuideFragment = new PoinGuideFragment();
            mPoinHistoryFragment = new PoinHistoryFragment();
            mPoinRedeemFragment = new PoinRedeemFragment();

            Bundle argument = new Bundle();
            argument.putString("saldo",sessionManager.getSaldo());
            argument.putString("poin", sessionManager.getPoin());
            argument.putString("userId", sessionManager.getUID());
            argument.putString("dateJoin", sessionManager.getDateJoin());
            argument.putString("msisdn", sessionManager.getMsisdn());
            argument.putString("username", sessionManager.getIdUser());
            argument.putString("email", sessionManager.getEmail());

            mPoinGuideFragment.setArguments(argument);
            mPoinHistoryFragment.setArguments(argument);
            mPoinGuideFragment.setArguments(argument);


        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0 :
                    return mPoinRedeemFragment;
                case 1 :
                    return mPoinGuideFragment;
                case 2 :
                    return mPoinHistoryFragment;
            }
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Tukar Poin";
                case 1:
                    return "Panduan";
                case 2:
                    return "Riwayat";
            }
            return null;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        //return super.onSupportNavigateUp();
        onBackPressed();
        return true;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_suwun_poin, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    //Toast.makeText(MainActivity.this, "berhasil cek saldo: " + result_desc, Toast.LENGTH_SHORT).show();
                    sessionManager.setSaldo(result_desc);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    sessionManager.setPoin(result_poin);
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));

                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage()!=null) {
                    Toast.makeText(getApplicationContext(), "Gagal Cek Saldo, " + t.getMessage().replace(BASE_IP,"API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Gagal Cek Saldo, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
