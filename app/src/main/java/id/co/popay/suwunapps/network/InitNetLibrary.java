package id.co.popay.suwunapps.network;

import android.content.Context;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import id.co.popay.suwunapps.BuildConfig;
import id.co.popay.suwunapps.helper.MyConstants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ara on 01/02/18.
 */

public class InitNetLibrary {

    static String _versionName = BuildConfig.VERSION_NAME;
    static int _versionCode = BuildConfig.VERSION_CODE;

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder()
                            .addHeader("appkey", MyConstants.APP_KEY)
                            .addHeader("appid", MyConstants.APP_ID)
                            .addHeader("nonce", MyConstants.APP_SALT)
                            .addHeader("version_name", _versionName)
                            .addHeader("version_code", String.valueOf(_versionCode))
                            .build();
                    return chain.proceed(request);
                }
            })
            //.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();

    public static Retrofit setInit() {
        //tambahan buat handle Use JsonReader.setLenient(true) to accept malformed JSON at line 1 column 1 path $

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(MyConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }


    public static APIServices getInstances() {
        return setInit().create(APIServices.class);
    }


    //tambahan kalo pake cookie di okHttpClient-nya

    public static OkHttpClient getHeader(Context context) {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("appkey", MyConstants.APP_KEY)
                        .addHeader("appid", MyConstants.APP_ID)
                        .addHeader("nonce", MyConstants.APP_SALT)
                        .addHeader("version_name", _versionName)
                        .addHeader("version_code", String.valueOf(_versionCode))
                        .build();
                return chain.proceed(request);
            }
        };

        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));

        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .cookieJar(cookieJar)
                .build();
        return okClient;
    }

    public static Retrofit setInitWithCookie(Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(getHeader(context))
                .baseUrl(MyConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }

    public static APIServices getInstanceWithCookie(Context context) {
        return setInitWithCookie(context).create(APIServices.class);
    }

    public static APIServices getInstanceWc(Context context) {
        return setInitWithCookie(context).create(APIServices.class);
    }
}
