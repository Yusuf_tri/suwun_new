package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.adapters.GameProviderAdapter;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseGameProvider;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class ListGameActivity extends SessionManager implements GameProviderAdapter.OnItemClickListener{
    private String mOperator,mTipe, mMSISDN;
    List<String> mProviderList;
    GameProviderAdapter mAdapter;
    RecyclerView recyclerView;
    private String mTimestamp, mSignature;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_game);

        recyclerView = (RecyclerView) findViewById(R.id.list_game_container);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        mProviderList = new ArrayList<>();
        mAdapter= new GameProviderAdapter(this, mProviderList);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setListener(this);
        loadListGameProvider();

    }

    private void loadListGameProvider() {
        showProgressDialog("");
        APIServices myAPI = InitNetLibrary.getInstances();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET);

        final Call<ResponseGameProvider> gameProvider = myAPI.getGameProvider( mTimestamp, mSignature);

        gameProvider.enqueue(new Callback<ResponseGameProvider>() {
            @Override
            public void onResponse(Call<ResponseGameProvider> call, Response<ResponseGameProvider> response) {
                hideProgressDialog();
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();

                if(result_code.equals("0000")) {
                    List<String> items = response.body().getGameProvider();
                    mProviderList.addAll(items);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseGameProvider> call, Throwable t) {
                hideProgressDialog();
                if (t.getMessage() != null ) {
                    Toast.makeText(getApplicationContext(), "API Failed, " + t.getMessage().replace(BASE_IP, "API_HOST") , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void OnItemClickListener(String itemId) {
        Intent intent = new Intent(ListGameActivity.this, VoucherGameActivity.class);
        intent.putExtra("operator", itemId);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        return;
    }
}
