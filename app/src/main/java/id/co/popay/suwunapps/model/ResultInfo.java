package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;

public class ResultInfo{

	@SerializedName("nama")
	private String nama;

	@SerializedName("inquiryid")
	private String inquiryid;

	@SerializedName("meter")
	private String meter;

	@SerializedName("systrace")
	private String systrace;

	@SerializedName("billid")
	private int billid;

	@SerializedName("idpel")
	private String idpel;

	@SerializedName("daya")
	private String daya;

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setInquiryid(String inquiryid){
		this.inquiryid = inquiryid;
	}

	public String getInquiryid(){
		return inquiryid;
	}

	public void setMeter(String meter){
		this.meter = meter;
	}

	public String getMeter(){
		return meter;
	}

	public void setSystrace(String systrace){
		this.systrace = systrace;
	}

	public String getSystrace(){
		return systrace;
	}

	public void setBillid(int billid){
		this.billid = billid;
	}

	public int getBillid(){
		return billid;
	}

	public void setIdpel(String idpel){
		this.idpel = idpel;
	}

	public String getIdpel(){
		return idpel;
	}

	public void setDaya(String daya){
		this.daya = daya;
	}

	public String getDaya(){
		return daya;
	}

	@Override
 	public String toString(){
		return 
			"ResultInfo{" + 
			"nama = '" + nama + '\'' + 
			",inquiryid = '" + inquiryid + '\'' + 
			",meter = '" + meter + '\'' + 
			",systrace = '" + systrace + '\'' + 
			",billid = '" + billid + '\'' + 
			",idpel = '" + idpel + '\'' + 
			",daya = '" + daya + '\'' + 
			"}";
		}
}