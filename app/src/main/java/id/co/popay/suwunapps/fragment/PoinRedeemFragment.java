package id.co.popay.suwunapps.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.activity.RedeemItemDetailActivity;
import id.co.popay.suwunapps.adapters.RedeemPoinAdapter;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.RedeemItemItem;
import id.co.popay.suwunapps.model.ResponseRedeem;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PoinRedeemFragment extends Fragment implements RedeemPoinAdapter.OnItemClickListener {
    List<RedeemItemItem> listRedeemItem;
    RedeemPoinAdapter mAdapter;
    RecyclerView recyclerView;
    private String mTimestamp, mSignature;

    String mUserId;
    ProgressDialog mProgressDialog;

    public PoinRedeemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_poin_redeem, container, false);
        View view = inflater.inflate(R.layout.fragment_poin_redeem, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.itemredeem_container);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        listRedeemItem = new ArrayList<>();
        mAdapter = new RedeemPoinAdapter(getActivity(), listRedeemItem);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
        mAdapter.setListener(this);

        loadItemRedeem();

        return view;
    }

    private void loadItemRedeem() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            //mProgressDialog.setTitle("");
            mProgressDialog.setMessage("loading redeem item...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();

        APIServices myAPI = InitNetLibrary.getInstances();
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET );

        Call<ResponseRedeem> dataItemRedeem = myAPI.getredeemitem(mTimestamp, mSignature);
        dataItemRedeem.enqueue(new Callback<ResponseRedeem>() {
            @Override
            public void onResponse(Call<ResponseRedeem> call, Response<ResponseRedeem> response) {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                if (result_code.equals("0000")) {
                    List<RedeemItemItem> itemRedeem = response.body().getRedeemItem();
                    listRedeemItem.addAll(itemRedeem);
                    mAdapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(getContext(), result_desc, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseRedeem> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(getContext(), t.getMessage().replace(MyConstants.BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void OnItemClickListener(String itemId, String itemTipe, String itemJudul, String itemDeskripsi, String itemPoin, String itemGambar, String itemNominal, String itemKodeproduk) {
        Intent intent =null;

//        if (itemTipe.equals("PULSA")) {
//            intent = new Intent(getActivity(), RedeemPulsaActivity.class);
//        } else if (itemTipe.equals("VOUCHER")) {
//            intent = new Intent(getActivity(), RedeemVoucherActivity.class);
//        } else if (itemTipe.equals("BARANG")) {
//            intent = new Intent(getActivity(), RedeemBarangActivity.class);
//        } else if (itemTipe.equals("GAME")) {
//            intent = new Intent(getActivity(), RedeemGameActivity.class);
//        } else if (itemTipe.equals("GOPAY")) {
//            intent = new Intent(getActivity(), RedeemPulsaActivity.class);
//        } else if (itemTipe.equals("TOKEN")) {
//            intent = new Intent(getActivity(), RedeemPulsaActivity.class);
//        }
//        else {
//            Toast.makeText(getContext(), "Tipe Item belum terdaftar", Toast.LENGTH_SHORT).show();
//        }

        intent = new Intent(getActivity(), RedeemItemDetailActivity.class);
        intent.putExtra("itemId", itemId);
        intent.putExtra("itemTipe", itemTipe);
        intent.putExtra("itemJudul", itemJudul);
        intent.putExtra("itemDeskripsi", itemDeskripsi);
        intent.putExtra("itemPoin",itemPoin);
        intent.putExtra("itemGambar", itemGambar);
        intent.putExtra("itemNominal", itemNominal);
        intent.putExtra("itemKodeproduk", itemKodeproduk);
        startActivity(intent);
    }
}
