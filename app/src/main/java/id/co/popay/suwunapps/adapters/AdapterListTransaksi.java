package id.co.popay.suwunapps.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.model.ResultTrxItem;

/**
 * Created by ara on 10/11/17.
 */



public class AdapterListTransaksi extends BaseAdapter {
    Context con;
    List<ResultTrxItem> dataTrx;

    public AdapterListTransaksi(Context c, List<ResultTrxItem> resultTrx) {
        con =  c;
        dataTrx = resultTrx;
    }

    @Override
    public int getCount() {
        return dataTrx.size() ;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)con.getSystemService(con.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.layout_list_transaksi,null);

        //setting semua widget di dalem view layout_list_transaksi
        TextView txtItem = (TextView) view.findViewById(R.id.txtItem);
        TextView txtHarga= (TextView) view.findViewById(R.id.txtHarga);
        TextView txtStatus= (TextView) view.findViewById(R.id.txtStatus);
        TextView txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);
        ImageView imgOperator = (ImageView) view.findViewById(R.id.imgOperator);

        String strOperator = dataTrx.get(position).getOperator();
        String strItem = strOperator + " " + dataTrx.get(position).getNominal();
        String strHarga= "Harga " + String.format("Rp. %,d", Integer.parseInt(dataTrx.get(position).getPrice())).replace(',', '.') + ", ke " + dataTrx.get(position).getTargetNumber();
        String strStatus = dataTrx.get(position).getStatus() ;

        if (strStatus.equals("SUKSES")) {
            strStatus = dataTrx.get(position).getStatus() + ", " + dataTrx.get(position).getInfo();
        }

        txtItem.setText(strItem);
        txtHarga.setText(strHarga);
        txtStatus.setText(strStatus);
        txtStatus.setTextIsSelectable(true);
        txtTanggal.setText(dataTrx.get(position).getCreatedAt());
        switch (strOperator) {
            case "TELKOMSEL" : imgOperator.setImageResource(R.drawable.logo_telkomsel); break;
            case "INDOSAT"   : imgOperator.setImageResource(R.drawable.logo_indosat); break;
            case "XL"        : imgOperator.setImageResource(R.drawable.logo_xl); break;
            case "SMARTFREN" : imgOperator.setImageResource(R.drawable.logo_smartfren); break;
            case "AXIS"      : imgOperator.setImageResource(R.drawable.logo_axis); break;
            case "TRI"       : imgOperator.setImageResource(R.drawable.logo_three); break;
            case "GOJEK"     : imgOperator.setImageResource(R.drawable.logo_gojek);break;
            case "PLN"       : imgOperator.setImageResource(R.drawable.ic_logo_pln);break;
            case "PDAM"      : imgOperator.setImageResource(R.drawable.ic_pdam); break;
            case "EMONEY"    : imgOperator.setImageResource(R.drawable.logo_emoney); break;
            case "OVO"       : imgOperator.setImageResource(R.drawable.logo_ovo); break;
            case "TCASH"     : imgOperator.setImageResource(R.drawable.logo_tcash); break;
            case "DANA"      : imgOperator.setImageResource(R.drawable.logo_dana); break;
            default : imgOperator.setImageResource(R.drawable.ic_nogame);
        }

        return view;
    }
}
