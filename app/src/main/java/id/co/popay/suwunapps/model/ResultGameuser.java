package id.co.popay.suwunapps.model;

import com.google.gson.annotations.SerializedName;


public class ResultGameuser{

	@SerializedName("target_name")
	private String targetName;

	@SerializedName("target_balance")
	private String targetBalance;

	@SerializedName("target_id")
	private String targetId;

	@SerializedName("target_level")
	private String targetLevel;

	public void setTargetName(String targetName){
		this.targetName = targetName;
	}

	public String getTargetName(){
		return targetName;
	}

	public void setTargetBalance(String targetBalance){
		this.targetBalance = targetBalance;
	}

	public String getTargetBalance(){
		return targetBalance;
	}

	public void setTargetId(String targetId){
		this.targetId = targetId;
	}

	public String getTargetId(){
		return targetId;
	}

	public void setTargetLevel(String targetLevel){
		this.targetLevel = targetLevel;
	}

	public String getTargetLevel(){
		return targetLevel;
	}

	@Override
 	public String toString(){
		return 
			"ResultGameuser{" + 
			"target_name = '" + targetName + '\'' + 
			",target_balance = '" + targetBalance + '\'' + 
			",target_id = '" + targetId + '\'' + 
			",target_level = '" + targetLevel + '\'' + 
			"}";
		}
}