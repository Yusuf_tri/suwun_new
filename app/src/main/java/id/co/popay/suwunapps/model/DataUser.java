package id.co.popay.suwunapps.model;


import com.google.gson.annotations.SerializedName;


public class DataUser{

	@SerializedName("uid")
	private String uid;

	@SerializedName("pin_trx")
	private String pinTrx;

	@SerializedName("password")
	private String password;

	@SerializedName("usermsisdn")
	private String usermsisdn;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("saldo")
	private String saldo;

	@SerializedName("verification_code")
	private String verificationCode;

	@SerializedName("email")
	private String email;

	@SerializedName("username")
	private String username;

	@SerializedName("poin")
	private String poin;

	@SerializedName("_id")
	private String _id;

	public void setId(String _id){
		this._id= _id;
	}

	public String getId(){
		return _id;
	}

	public void setPoin(String poin){
		this.poin= poin;
	}

	public String getPoin(){
		return poin;
	}

	public void setUid(String uid){
		this.uid = uid;
	}

	public String getUid(){
		return uid;
	}

	public void setPinTrx(String pinTrx){
		this.pinTrx = pinTrx;
	}

	public String getPinTrx(){
		return pinTrx;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setUsermsisdn(String usermsisdn){
		this.usermsisdn = usermsisdn;
	}

	public String getUsermsisdn(){
		return usermsisdn;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setSaldo(String saldo){
		this.saldo = saldo;
	}

	public String getSaldo(){
		return saldo;
	}

	public void setVerificationCode(String verificationCode){
		this.verificationCode = verificationCode;
	}

	public String getVerificationCode(){
		return verificationCode;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"DataUser{" + 
			"uid = '" + uid + '\'' + 
			",pin_trx = '" + pinTrx + '\'' + 
			",password = '" + password + '\'' + 
			",usermsisdn = '" + usermsisdn + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",saldo = '" + saldo + '\'' + 
			",verification_code = '" + verificationCode + '\'' + 
			",email = '" + email + '\'' + 
			",username = '" + username + '\'' +
			",_id = '" + _id + '\'' +
			"}";
		}
}