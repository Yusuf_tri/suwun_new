package id.co.popay.suwunapps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.popay.suwunapps.R;
import id.co.popay.suwunapps.helper.MyConstants;
import id.co.popay.suwunapps.helper.SessionManager;
import id.co.popay.suwunapps.helper.Suwunapps;
import id.co.popay.suwunapps.model.ResponseInquiryPDAM;
import id.co.popay.suwunapps.model.ResponseSaldo;
import id.co.popay.suwunapps.network.APIServices;
import id.co.popay.suwunapps.network.InitNetLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.popay.suwunapps.helper.MyConstants.BASE_IP;

public class PDAMActivity extends SessionManager {

    @BindView(R.id.txtSaldo)
    TextView txtSaldo;
    @BindView(R.id.txtPoin)
    TextView txtPoin;
    @BindView(R.id.saldoBar)
    LinearLayout saldoBar;
    @BindView(R.id.edtKota)
    EditText edtKota;
    @BindView(R.id.edtCustId)
    EditText edtCustId;
    @BindView(R.id.btnCekTagihan)
    Button btnCekTagihan;
    @BindView(R.id.nomervaCardView)
    CardView nomervaCardView;
    @BindView(R.id.txtLabelCustId)
    TextView txtLabelCustId;
    @BindView(R.id.txtCustId)
    TextView txtCustId;
    @BindView(R.id.txtLabelNama)
    TextView txtLabelNama;
    @BindView(R.id.txtNamaPelanggan)
    TextView txtNamaPelanggan;
    @BindView(R.id.txtLabelPeriode)
    TextView txtLabelPeriode;
    @BindView(R.id.txtPeriode)
    TextView txtPeriode;
    @BindView(R.id.txtLabelNominal)
    TextView txtLabelNominal;
    @BindView(R.id.txtTagihan)
    TextView txtTagihan;
    @BindView(R.id.txtLabelAdminCA)
    TextView txtLabelAdminCA;
    @BindView(R.id.txtAdminCA)
    TextView txtAdminCA;
    @BindView(R.id.txtLabelTotal)
    TextView txtLabelTotal;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtSaldoTidakCukup)
    TextView txtSaldoTidakCukup;
    @BindView(R.id.purchaseDetail)
    CardView purchaseDetail;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.btn_beli)
    Button btnBeli;
    @BindView(R.id.myTopLinearLayout)
    LinearLayout myTopLinearLayout;

    TextWatcher textWatcherIdPel = null;
    @BindView(R.id.txtLabelDenda)
    TextView txtLabelDenda;
    @BindView(R.id.txtDenda)
    TextView txtDenda;
    @BindView(R.id.txtLabelCubic)
    TextView txtLabelCubic;
    @BindView(R.id.txtCubic)
    TextView txtCubic;
    @BindView(R.id.txtLabelArea)
    TextView txtLabelArea;
    @BindView(R.id.txtArea)
    TextView txtArea;
    @BindView(R.id.txtTunggakan)
    TextView txtTunggakan;

    private String mTimestamp, mSignature, mCityId, mCityName, mCubic, mArea;
    private String mIdPel;
    private String mNamaPelanggan;
    private int mPeriode;
    private String mStrPeriode;
    private int mTagihan, mDenda;
    private int mTotalBayar;
    private String mInquiryId = "0";
    private String mSystrace;
    private String mBillid = "0";
    private int mAdminCA = 0;
    private int mTotal;
    private String mStrTagihan;
    private String mProvider, mKodeproduk;
    private String mStrTunggakan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdam);
        ButterKnife.bind(this);

        getSaldoFromServer();
        purchaseDetail.setVisibility(View.GONE);
        btnBeli.setVisibility(View.GONE);

        textWatcherIdPel = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 4) {
                    btnCekTagihan.setEnabled(true);
                    btnCekTagihan.setBackgroundResource(R.drawable.button_bg);
                } else {
                    btnCekTagihan.setEnabled(false);
                    btnCekTagihan.setBackgroundResource(R.color.colorGrid);
                    purchaseDetail.setVisibility(View.INVISIBLE);
                    btnBeli.setVisibility(View.INVISIBLE);
                    btnBeli.setEnabled(false);
                    btnBeli.setBackgroundResource(R.color.colorGrid);
                    txtSaldoTidakCukup.setVisibility(View.GONE);
                }
            }
        };

        edtCustId.addTextChangedListener(textWatcherIdPel);

        Intent intent = getIntent();

        if (intent.hasExtra("cityId")) {
            mCityId = intent.getStringExtra("cityId");
            mCityName = intent.getStringExtra("cityName");
            edtKota.setText(mCityName);
        }

    }

    @OnClick(R.id.txtSaldo)
    public void onTxtSaldoClicked() {
        startActivity(new Intent(PDAMActivity.this, DepositRequestActivity.class));
    }

    @OnClick(R.id.txtPoin)
    public void onTxtPoinClicked() {
        startActivity(new Intent(PDAMActivity.this, SuwunPoinActivity.class));
    }

    @OnClick(R.id.edtKota)
    public void onEdtKotaClicked() {
        startActivity(new Intent(this, ListPdamCityActivity.class));
    }

    @OnClick(R.id.btnCekTagihan)
    public void onBtnCekTagihanClicked() {

    }

    @OnClick(R.id.btn_beli)
    public void onBtnBeliClicked() {
        Intent intent = new Intent(PDAMActivity.this, EnterPinBillPaymentActivity.class);
        intent.putExtra("idpel", mIdPel);
        intent.putExtra("namapelanggan", mNamaPelanggan);
        intent.putExtra("periode", mStrPeriode);
        intent.putExtra("amount", mStrTagihan);
        intent.putExtra("adminca", mAdminCA);
        intent.putExtra("inquiryid", mInquiryId);
        intent.putExtra("systrace", mSystrace);
        intent.putExtra("billid", mBillid);
        intent.putExtra("debitsaldo", mTotal);
        intent.putExtra("tipe", "BILLPAYMENT");
        intent.putExtra("provider", mProvider);
        intent.putExtra("kodeproduk", mKodeproduk);
        String myData = mIdPel + " " + mNamaPelanggan + " " + mStrPeriode + " " + mTagihan + " " + mAdminCA + " " + mInquiryId + " " + mSystrace + " " + mBillid + " " + mTotal;
        Log.d("PDAMTagihan", myData);

        startActivity(intent);

    }

    private boolean isEnoughBalance() {
        Log.d("PDAMBill", sessionManager.getSaldo() + " " + mAdminCA + " " + mTagihan);
        return (Integer.parseInt(sessionManager.getSaldo()) >= (mAdminCA + mTagihan));
    }

    private void toogleBtnBeli() {
        if (!isEnoughBalance()) {
            btnBeli.setEnabled(false);
            btnBeli.setBackgroundResource(R.color.colorGrid);
            txtSaldoTidakCukup.setVisibility(View.VISIBLE);
            txtTunggakan.setVisibility(View.GONE);
            Snackbar.make(myTopLinearLayout, "Saldo Anda tidak cukup", Snackbar.LENGTH_LONG)
                    .setAction("Top Up", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(PDAMActivity.this, DepositRequestActivity.class));
                        }
                    }).show();
        } else {
            txtSaldoTidakCukup.setVisibility(View.GONE);
            btnBeli.setEnabled(true);
            btnBeli.setBackgroundResource(R.drawable.button_bg);
        }
    }

    private void getSaldoFromServer() {
        mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
        mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET + sessionManager.getId());

        APIServices myAPI = InitNetLibrary.getInstanceWithCookie(this);
        Call<ResponseSaldo> cekSaldoDb = myAPI.getSaldo(mTimestamp, sessionManager.getId(), mSignature);
        cekSaldoDb.enqueue(new Callback<ResponseSaldo>() {
            @Override
            public void onResponse(Call<ResponseSaldo> call, Response<ResponseSaldo> response) {
                String result_code = response.body().getResultCode();
                String result_desc = response.body().getResultDesc();
                String result_poin = response.body().getResultPoin();
                if (result_code.equals("0000")) {
                    sessionManager.setSaldo(result_desc);
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt(result_desc)).replace(',', '.'));
                    sessionManager.setPoin(result_poin);
                    txtPoin.setText(String.format("%,d", Integer.parseInt(result_poin)).replace(',', '.'));
                } else if (result_code.equals("0103")){
                    Toast.makeText(getApplicationContext(),"Session Anda habis. Silakan login ulang. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                    sessionManager.setSaldo("0");
                    sessionManager.setPoin("0");
                    txtSaldo.setText(String.format("Rp. %,d", Integer.parseInt("0" )).replace(',', '.'));
                    txtPoin.setText(String.format("%,d", Integer.parseInt("0")).replace(',', '.'));
                } else {
                    Toast.makeText(getApplicationContext(),"Gagal ambil info saldo. (E"+ result_code +"). ",Toast.LENGTH_LONG ).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSaldo> call, Throwable t) {
                if (t.getMessage() != null) {
                    Toast.makeText(getApplicationContext(), "API cs Fail, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "API cs Failed, TIMEOUT.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @OnClick(R.id.btnCekTagihan)
    public void onViewClicked() {
        mCityName = edtKota.getText().toString();
        Log.d("PDAMActivity", "Data mCityName: " + mCityName);
        if (mCityName.equals("")) {
            Toast.makeText(PDAMActivity.this, "Pilih Kota terlebih dulu.", Toast.LENGTH_LONG).show();
        } else {
            showProgressDialog("");
            btnBeli.setEnabled(false);
            purchaseDetail.setVisibility(View.INVISIBLE);
            btnBeli.setVisibility(View.INVISIBLE);
            btnBeli.setBackgroundResource(R.color.colorGrid);

            mIdPel = edtCustId.getText().toString();
            mTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
            mSignature = Suwunapps.getSHA1(mTimestamp + MyConstants.APP_KEY + MyConstants.APP_SECRET +
                    "BILL" + "PDAM" +
                    mCityId + mIdPel + sessionManager.getId());

            APIServices myAPI = InitNetLibrary.getInstanceWc(this);
            Call<ResponseInquiryPDAM> inquiryPDAMCall = myAPI.inquiryBillPDAM(mTimestamp, "BILL", "PDAM", mIdPel, mCityId, sessionManager.getId(), mSignature);
            inquiryPDAMCall.enqueue(new Callback<ResponseInquiryPDAM>() {
                @Override
                public void onResponse(Call<ResponseInquiryPDAM> call, Response<ResponseInquiryPDAM> response) {
                    hideProgressDialog();
                    String result_code = response.body().getResultCode();
                    String result_desc = response.body().getResultDesc();
                    Log.d("PDAMTagihanActivity", response.body().toString());
                    if (result_code.equals("0000")) {
                        mNamaPelanggan = response.body().getResultInfoPdam().getNama();
                        mStrPeriode = response.body().getResultInfoPdam().getPeriode();
                        mTagihan = response.body().getResultInfoPdam().getTagihan();
                        mStrTagihan = String.valueOf(response.body().getResultInfoPdam().getTagihan() + response.body().getResultInfoPdam().getDenda());
                        mAdminCA = Integer.parseInt(response.body().getResultInfoPdam().getAdminsuwun());
                        mDenda = response.body().getResultInfoPdam().getDenda();
                        mTotalBayar = response.body().getResultInfoPdam().getTotalbayar();

                        mSystrace = String.valueOf(response.body().getResultInfoPdam().getSystrace());
                        mProvider = response.body().getResultInfoPdam().getProvider();
                        mKodeproduk = response.body().getResultInfoPdam().getKodeproduk();
                        mCubic = response.body().getResultInfoPdam().getCubic();
                        mArea = response.body().getResultInfoPdam().getArea();
                        //String rincian = TextUtils.join("\n", response.body().getResultInfoPstn().getRincian());

                        Log.d("PDAMActivity", mArea + " " + mKodeproduk + " " + mCubic + " " + mCityId + " " + mStrPeriode.indexOf('-'));
                        txtCustId.setText(edtCustId.getText().toString());
                        txtNamaPelanggan.setText(mNamaPelanggan);
                        txtArea.setText(mArea);
                        //txtPeriode.setText(mStrPeriode + " bulan\n" + rincian);
                        txtPeriode.setText(mStrPeriode);

//                        if (mCityId.equals("1006")) {
//                            //PDAM Palembang, pembayaran hanya 1 bulan aja
//                            txtTagihan.setText(String.format("Rp. %,d", mTagihan).replace(',', '.'));
//                            txtDenda.setText(String.format("Rp. %,d", mDenda).replace(',', '.'));
//                            if (mStrPeriode.indexOf('-') != -1) {
//                                //ada karakter '-', berarti ada tunggakan
//                                txtTunggakan.setVisibility(View.VISIBLE);
//                                txtTunggakan.setText("*Hanya untuk 1 bulan, periode " + mStrPeriode.substring(7, 13));
//                            } else {
//                                txtTunggakan.setVisibility(View.GONE);
//                                //txtSaldoTidakCukup.setText("Pembayaran untuk 1 bulan, periode " + mStrPeriode.substring(5,9));
//                            }
//
//                        } else {
//                            txtTagihan.setText(String.format("Rp. %,d", mTagihan).replace(',', '.'));
//                            txtDenda.setText(String.format("Rp. %,d", mDenda).replace(',', '.'));
//
//                        }

                        txtTagihan.setText(String.format("Rp. %,d", mTagihan).replace(',', '.'));
                        txtDenda.setText(String.format("Rp. %,d", mDenda).replace(',', '.'));
                        txtAdminCA.setText(String.format("Rp. %,d", mAdminCA).replace(',', '.'));
                        txtCubic.setText(mCubic);
                        mTotal = (mTagihan + mAdminCA + mDenda);
                        Log.d("TotalBayar", mTagihan + " " + mAdminCA + " " + mDenda);
                        txtTotal.setText(String.format("Rp. %,d", mTotal).replace(',', '.'));
                        purchaseDetail.setVisibility(View.VISIBLE);
                        btnBeli.setVisibility(View.VISIBLE);

                        toogleBtnBeli();

                        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollview);
                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });


                    } else {
                        Toast.makeText(PDAMActivity.this, result_desc, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseInquiryPDAM> call, Throwable t) {
                    hideProgressDialog();
                    if (t.getMessage() == null) {
                        Toast.makeText(getApplicationContext(), "API cek tagihan gagal, TIMEOUT", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "API cek tagihan gagal, " + t.getMessage().replace(BASE_IP, "API_HOST"), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

    }
}
